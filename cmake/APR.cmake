
set (APR_installation     "/group/online/dataflow/cmtuser/libraries/apr")
set (APR_includes         "${APR_installation}/include")
set (APR_LIBS             "-L${APR_installation}/.libs -lapr-1")

macro (add_APR_definitions lib)
  target_compile_definitions(${lib} PRIVATE -DAPR_DECLARE_EXPORT)
endmacro()
