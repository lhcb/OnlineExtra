# - Locate BitTorrent library
# Defines:
#
#  BitTorrent_FOUND
#  BitTorrent_INCLUDE_DIR
#  BitTorrent_INCLUDE_DIRS (not cached)
#  BitTorrent_LIBRARY
#  BitTorrent_LIBRARIES (not cached)
#  BitTorrent_LIBRARY_DIRS (not cached)
#  BitTorrent_EXECUTABLE

find_path(BitTorrent_INCLUDE_DIR libtorrent/torrent.hpp)
find_library(BitTorrent_LIBRARY NAMES torrent-rasterbar )

# handle the QUIETLY and REQUIRED arguments and set BitTorrent_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(BitTorrent DEFAULT_MSG BitTorrent_INCLUDE_DIR BitTorrent_LIBRARY)

mark_as_advanced(BitTorrent_FOUND BitTorrent_INCLUDE_DIR BitTorrent_LIBRARY)

set(BitTorrent_INCLUDE_DIRS ${BitTorrent_INCLUDE_DIR})
get_filename_component(BitTorrent_LIBRARY_DIRS ${BitTorrent_LIBRARY} PATH)

set(BitTorrent_LIBRARIES ${BitTorrent_LIBRARY})

macro (add_BitTorrent_definitions lib)
  target_compile_definitions(${lib} PRIVATE -DBitTorrent_DECLARE_EXPORT)
  target_compile_options(${lib} PRIVATE -Wno-suggest-override)
  target_compile_options(${lib} PRIVATE -Wno-deprecated-declarations)
endmacro()
