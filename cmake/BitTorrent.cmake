#
#  Cmake fragment to compile/link against the libtorrent library
#
set (BitTorrent_installation     "/group/online/dataflow/cmtuser/libraries/BitTorrent/1.0.11/$ENV{CMTCONFIG}")
set (BitTorrent_includes         "${BitTorrent_installation}/include")
set (BitTorrent_LIBS             "${BitTorrent_installation}/lib/libtorrent-rasterbar.so")

macro (add_BitTorrent_definitions lib)
  target_compile_definitions(${lib} PRIVATE -DBitTorrent_DECLARE_EXPORT)
  target_compile_options(${lib} PRIVATE -Wno-suggest-override)
  target_compile_options(${lib} PRIVATE -Wno-deprecated-declarations)
endmacro()
