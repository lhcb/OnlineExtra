
set (ACTIVEMQCPP_installation     "/group/online/dataflow/cmtuser/libraries/activemq-cpp-library-3.8.2/src/main")
set (ACTIVEMQCPP_includes         "${ACTIVEMQCPP_installation}")
set (ACTIVEMQCPP_LIBS             "-L${ACTIVEMQCPP_installation}/.libs -lactivemq-cpp" )

LINK_DIRECTORIES( ${ACTIVEMQCPP_installation}/.libs )

macro (add_ACTIVEMQCPP_definitions lib)
  target_compile_definitions(${lib} PRIVATE -DHAVE_ACTIVEMQ)
  target_compile_options(${lib} PRIVATE -Wno-overloaded-virtual)
endmacro()
