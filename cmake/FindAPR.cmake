# - Locate APR library
# Defines:
#
#  APR_FOUND
#  APR_INCLUDE_DIR
#  APR_INCLUDE_DIRS (not cached)
#  APR_LIBRARY
#  APR_LIBRARIES (not cached)
#  APR_LIBRARY_DIRS (not cached)
#  APR_EXECUTABLE

find_path(APR_INCLUDE_DIR apr-1/apr.h)
find_library(APR_LIBRARY NAMES apr-1 )

# handle the QUIETLY and REQUIRED arguments and set APR_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(APR DEFAULT_MSG APR_INCLUDE_DIR APR_LIBRARY)

mark_as_advanced(APR_FOUND APR_INCLUDE_DIR APR_LIBRARY)

set(APR_INCLUDE_DIRS ${APR_INCLUDE_DIR}/apr-1)
get_filename_component(APR_LIBRARY_DIRS ${APR_LIBRARY} PATH)

set(APR_LIBRARIES ${APR_LIBRARY})

macro (add_APR_definitions lib)
  target_compile_definitions(${lib} PRIVATE -DAPR_DECLARE_EXPORT)
endmacro()
