# - Locate APR library
# Defines:
#
#  ActiveMQCPP_FOUND
#  ActiveMQCPP_INCLUDE_DIR
#  ActiveMQCPP_INCLUDE_DIRS (not cached)
#  ActiveMQCPP_LIBRARY
#  ActiveMQCPP_LIBRARIES (not cached)
#  ActiveMQCPP_LIBRARY_DIRS (not cached)
#  ActiveMQCPP_EXECUTABLE

find_path(ActiveMQCPP_INCLUDE_DIR activemq-cpp-3.9.3/activemq/core/ActiveMQSession.h)
find_library(ActiveMQCPP_LIBRARY NAMES activemq-cpp )

# handle the QUIETLY and REQUIRED arguments and set ActiveMQCPP_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(ActiveMQCPP DEFAULT_MSG ActiveMQCPP_INCLUDE_DIR ActiveMQCPP_LIBRARY)

mark_as_advanced(ActiveMQCPP_FOUND ActiveMQCPP_INCLUDE_DIR ActiveMQCPP_LIBRARY)

set(ActiveMQCPP_INCLUDE_DIRS ${ActiveMQCPP_INCLUDE_DIR}/activemq-cpp-3.9.3)
get_filename_component(ActiveMQCPP_LIBRARY_DIRS ${ActiveMQCPP_LIBRARY} PATH)

set(ActiveMQCPP_LIBRARIES ${ActiveMQCPP_LIBRARY})

macro (add_ACTIVEMQCPP_definitions lib)
  target_compile_definitions(${lib} PRIVATE -DHAVE_ACTIVEMQ)
  target_compile_options(${lib} PRIVATE -Wno-overloaded-virtual)
  target_compile_options(ActiveMQ PRIVATE -Wno-suggest-override)
endmacro()
