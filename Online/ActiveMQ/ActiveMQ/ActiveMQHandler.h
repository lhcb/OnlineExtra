//====================================================================
//  Comet
//--------------------------------------------------------------------
//
//  Package    : ActiveMQ
//
//  Description: DIM enabled ActiveMQ
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//====================================================================
#ifndef ACTIVEMQ_ActiveMQHANDLER_H
#define ACTIVEMQ_ActiveMQHANDLER_H 1

// Framework include files
#include "CPP/Interactor.h"

/*
 *   ActiveMQ namespace declaration
 */
namespace ActiveMQ  {

  // Forward declarations
  class ActiveMQSensor;

  /**@class ActiveMQHandler ActiveMQHandler.h ActiveMQ/ActiveMQHandler.h
   *
   * @author M.Frank
   */
  class ActiveMQHandler : public CPP::Interactor {
  public:
    enum { PROTO_SIMPLE=0, PROTO_XML=1 };
  protected:
    /// Service data handler
    ActiveMQSensor* m_sensor;
    /// Protocol specifier
    int m_protocol;
  public:
    /// Default constructor
    ActiveMQHandler(ActiveMQSensor* sensor, int prot) : m_sensor(sensor), m_protocol(prot)  {}
    /// Standard destructor
    virtual ~ActiveMQHandler()  {}
    /// Event handler: Subscribes to new connections
    virtual void handle(const CPP::Event& ev)  override;
  };
}      // End namespace ActiveMQ
#endif // ACTIVEMQ_AMQPHANDLER_H
