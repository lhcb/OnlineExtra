//====================================================================
//  Stomp
//--------------------------------------------------------------------
//
//  Package    : Stomp
//
//  Description: DIM enabled Stomp plugin 
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//====================================================================
#ifndef STOMP_LOG_H
#define STOMP_LOG_H 1

// C++ include files
#include <iostream>

/*
 *   Stomp namespace declaration
 */
namespace ActiveMQ {
  void install_printer(long level);
  std::ostream& log();
}      // End namespace Stomp
#endif /* STOMP_LOG_H */

