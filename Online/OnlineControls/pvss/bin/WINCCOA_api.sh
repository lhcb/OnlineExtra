#!/bin/bash
#. /group/online/dataflow/scripts/pvss_preamble.sh $*
export RELEASEDIR=/group/online/dataflow/cmtuser/WINCC;
cd ${RELEASEDIR}/Online/OnlineControls/cmt;
#
ORACLE=/cvmfs/sft.cern.ch/lcg/releases/LCG_87/oracle/11.2.0.3.0/x86_64-centos7-gcc62-opt;
CX_ORA=/cvmfs/sft.cern.ch/lcg/releases/LCG_87/cx_oracle/5.1.1/x86_64-centos7-gcc62-opt/lib/python2.7/site-packages;
# Clean runtime environment
#
PVSSMGR_Num=0
eval `python <<EOF
args = '$*'.split(' ')
num='0'
for i in xrange(len(args)):
  if args[i].upper()[:5]=='-PROJ':
    print 'PVSS_system='+str(args[i+1])+';'
  elif args[i].upper()[:4]=='-NUM':
    print 'PVSSMGR_Num='+str(args[i+1])+';'
EOF`
# export UTGID=${2}${PVSSMGR_Num}
export PVSS_II=/localdisk/wincc/${PVSS_system}/config/config
. ./WCCOA.setup.${PVSS_system}
export PYTHONPATH=${CX_ORA}:${ORACLE}/lib:${PYTHONPATH};
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${CX_ORA}:${ORACLE}/lib;
#
echo $PVSS_II -- exec -a ${2} PVSS00api.exe $*
exec `which PVSS00api.exe` $*
