#uses "ctrlUtils.cpp"

int propertyEditor_doExit = 0;
int propertyEditor_Slots  = 64;

string PropertyEditor_sysName()  {
  return PropertyEditorSystem;
}

/// Access system ID of the job options system
int PropertyEditor_sysID()  {
  return getSystemId(PropertyEditor_sysName()+":");
}

string PropertyEditor_slot_dp(string slot, string sub_dp)   {
  int islot = slot;
  string nam;
  sprintf(nam,"%s:PropertyEditorSlot_%02X%s",PropertyEditor_sysName(),islot,sub_dp);
  return nam;
}

int PropertyEditor_request(string request)   {
  int rc = dpSet(PropertyEditor_sysName()+":PropertyEditor.Request", request);
  return rc;
}

/// Regularly update the "InUse" datapoint. to indicate to the server that the slot is still in use.
void PropertyEditor_panel_thread(string slot)    {
  while (1)  {
    dpSetWait(PropertyEditor_slot_dp(slot,".InUse"),1);
    delay(5);
  }
}

/// Handle server response to the panel
void PropertyEditor_panelResponse(string dp, string value)   {
  if ( strlen(value) != 0 )  {
    mapping args;
    DebugTN("DP:"+dp+" Value:"+value);
    ctrlUtils_parse_args(value,"/",args);
    if ( mappingHasKey(args,"status") && args["status"] == "SUCCESS" )   {
      if ( mappingHasKey(args,"id") && args["id"] == m_id.text )   {
	if ( mappingHasKey(args,"allocate") )   {
	  m_slot.text = args["slot"];
	  DebugTN("panelResponse: allocate "+m_slot.text);
	  startThread("PropertyEditor_panel_thread",m_slot.text);
	  PropertyEditor_panel_connect_slot(m_slot.text);
	  PropertyEditor_panel_load_domains();
	  return;
	}
      }
      if ( mappingHasKey(args,"id") && args["id"] == m_id.text )   {
        if ( mappingHasKey(args,"set_domain") )   {
          string domain = args["domain"];
          PropertyEditor_panel_load_nodes(domain);
          return;
        }
      }
      if ( mappingHasKey(args,"id") && args["id"] == m_id.text )   {
        if ( mappingHasKey(args,"set_task") )   {
          string task = args["task"];
	  string dp   = PropertyEditor_slot_dp(m_slot.text,".Options");
	  dyn_string value;
	  int rc = dpGet(dp, value);
	  if ( dynlen(value) > 0 )  {
	    PropertyEditor_optionResponse(dp, value);
	  }
          return;
        }
      }
    }
    string nam;
    ctrlUtils_trace("PropertyEditor: Request failed:"+value);
  }
}

/// Load options into the table. Set coloring if they were edited before....
void PropertyEditor_optionResponse(string dp, dyn_string value)   {
  int        pos, row, num;
  string     tmp, val;
  dyn_string nam, opts;

  for(int i=1; i<dynlen(value); ++i)   {
    row  = i-1;
    pos  = strpos(value[i],' ');
    tmp  = substr(value[i],0, pos);
    nam  = strsplit(tmp,'/');
    val  = substr(value[i],pos+1);
    num  = m_optTable.lineCount();
    opts = mappingHasKey(propertyEditor_changes,tmp)
      ? makeDynString("Changed","green","black") : makeDynString("","white","white");
    if ( row >= num )  {
      m_optTable.appendLine("ID",i, "Client", nam[1], "Name", nam[2], "Value", val, "Changed", opts);
    }
    else   {
      m_optTable.cellValueRC(row,"Client")  = nam[1];
      m_optTable.cellValueRC(row,"Name")    = nam[2];
      m_optTable.cellValueRC(row,"Value")   = val;
      m_optTable.cellValueRC(row,"Changed") = opts;
    }
  }
  for(int i=dynlen(value), n=m_optTable.lineCount(); i<n; ++i)
    m_optTable.deleteLineN(i);

  if ( m_loadOptions.enabled && 0 == dynlen(value) )    {
    ChildPanelOn("visionUtils/ErrorBox","Editor Error",
		 makeDynString("$1:No options present for task\n"+m_task.selectedText),50,50);
  }
}

/// Connect datapoint listeners to receive a callback when the task options changed
void PropertyEditor_panel_connect_slot(string slot)    {
  int slot_no = slot;
  dpConnect("PropertyEditor_panelResponse",  PropertyEditor_slot_dp(slot,".Response"));
  dpConnect("PropertyEditor_optionResponse", PropertyEditor_slot_dp(slot,".Options"));
}

/// Initialize editor application
void PropertyEditor_init()   {
  PropertyEditor_panel_init();
  dpConnect("PropertyEditor_panelResponse",PropertyEditor_sysName()+":PropertyEditor.Response");
  PropertyEditor_request("allocate/id="+m_id.text);
}

/// Basic panel initialization
void PropertyEditor_panel_init()   {
  m_id.text                  = getCurrentTime();
  m_id.toolTipText           = "Client identifier within server";
  m_idLabel.text             = "Client ID:";
  m_idLabel.toolTipText      = m_id.toolTipText;

  m_logo.text                = "LHCb";
  m_logo2.text               = "Online";

  m_slot.text                = "";
  m_slot.toolTipText         = "Slot identifier within server";
  m_slotLabel.text           = "Slot:";
  m_slotLabel.toolTipText    = m_slot.toolTipText;

  m_explorer.text            = "Options Explorer";
  m_explorer.toolTipText     = "Select here the process to\naccess the job properties.";
  m_editor.text              = "Option Editor";
  m_editor.toolTipText       = "In this frame single options are edited.\nDouble click in the table to load an option.";

  m_optID.text               = "";
  m_optID.toolTipText        = "Option identifier";  
  m_optID.visible            = false;
  m_optClient.text           = "";
  m_optClient.toolTipText    = "Option Client\nnormally a service, algorithm or a tool";
  m_optClient.visible        = false;
  m_optName.text             = "";
  m_optName.toolTipText      = "Option name local to client";
  m_optName.visible          = false;

  m_optTag.text              = "";
  m_optTag.visible           = true;

  m_optValue.text            = "";
  m_optValue.toolTipText     = "String representation of the option value";
  m_optReload.text           = "Reload";
  m_optReload.toolTipText    = "Reload the original option value from the table below";
  m_optSave.text             = "Save";
  m_optSave.toolTipText      = "Save the edited option value back to the table\n"+
    "Note: This does NOT yet update the client process";
  m_domain.enabled           = true;
  m_domain.toolTipText       = "Select here the top DIM DNS node\nyou want to investigate.";
  m_domainLabel.text         = "Select DNS";
  m_domainLabel.toolTipText  = m_domain.toolTipText;

  m_node.enabled             = false;
  m_node.toolTipText         = "Select here the node within\nthe top DIM DNS domain\nyou want to investigate.";
  m_node.deleteAllItems();

  m_nodeLabel.text           = "Select Node";
  m_nodeLabel.toolTipText    = m_node.toolTipText;

  m_task.enabled             = false;
  m_task.deleteAllItems();
  m_task.toolTipText         = "Select here the task by utgid to see the options.";

  m_taskLabel.text           = "Select Task";
  m_taskLabel.toolTipText    = m_task.toolTipText;

  m_loadOptions.enabled      = false;
  m_loadOptions.text         = "Load Options";
  m_loadOptions.toolTipText  = "Click here to load the options\nof the selected process into the panel.";

  m_loadOptions2.enabled     = false;
  m_loadOptions2.toolTipText = "Click here to load the options\nof the selected process into the panel.";

  m_sendChanges.enabled      = false;
  m_sendChanges.text         = "Send Changed Options to Server";
  m_sendChanges.toolTipText  = "Send all changed options right to the target process.";
  m_sendChanges2.enabled     = false;
  m_sendChanges2.toolTipText = m_sendChanges.toolTipText;

  m_close.text               = "Close Panel - Discard Changes";
  m_close.toolTipText        = "Close the Panel.\nAny pending changes,\nwhich are not yet sent to the server\nshall be discarded.";
  m_close2.toolTipText       = m_close.toolTipText;

  m_optTable.toolTipText     = "This table holds all options of the selected process.";
  LayerOn(1);
  LayerOff(2);
}

/// Load the DNS domains into the editor panel
void PropertyEditor_panel_load_domains()   {
  string value;
  int rc = dpGet(PropertyEditor_sysName()+":PropertyEditor.Domains",value);
  dyn_string domains = strsplit(value,'|');
  m_domain.deleteAllItems();
  m_domain.items = domains;
  if ( dynlen(domains) > 0 )
    m_domain.selectedPos = 1;
  // Clean dependent fields
  m_node.deleteAllItems();
  m_task.deleteAllItems();
  m_node.enabled         = false;
  m_task.enabled         = false;
  m_loadOptions.enabled  = false;
  m_loadOptions2.enabled = false;
}

/// Explorer callback: a DNS domain is selected
void PropertyEditor_panel_select_domain(string domain)  {
  PropertyEditor_request("set_domain/id="+m_id.text+"/slot="+m_slot.text+"/domain="+domain);        
  m_node.deleteAllItems();
  m_task.deleteAllItems();
  m_optTable.deleteAllLines();
  mappingClear(propertyEditor_changes);
  m_node.enabled         = false;
  m_task.enabled         = false;
  m_loadOptions.enabled  = false;
  m_loadOptions2.enabled = false;
  m_sendChanges.enabled  = false;
  m_sendChanges2.enabled = false;
  LayerOff(2);
}

/// Explorer callback: load the nodes of a DNS domain
void PropertyEditor_panel_load_nodes(string domain)   {
  string value;
  int    rc = dpGet(PropertyEditor_sysName()+":PropertyEditorDomain_"+domain+".Nodes", value);
  dyn_string nodes = strsplit(value,'|');
  m_node.deleteAllItems();
  m_optTable.deleteAllLines();
  mappingClear(propertyEditor_changes);
  m_node.items           = nodes;
  m_node.enabled         = true;
  if ( dynlen(nodes) > 0 )   {
    m_node.selectedPos = 1;
    PropertyEditor_panel_select_node(nodes[1]);
    return;
  }
  m_loadOptions.enabled  = false;
  m_loadOptions2.enabled = false;
  m_sendChanges.enabled  = false;
  m_sendChanges2.enabled = false;
}

/// Explorer callback: a single node in a DNS domain is selected
void PropertyEditor_panel_select_node(string node)  {
  string value;
  string domain  = m_domain.selectedText;
  string pattern1 = "_"+strtoupper(node)+"_";
  string pattern2 = "_"+strtolower(node)+"_";
  dyn_string display;
  int rc = dpGet(PropertyEditor_sysName()+":PropertyEditorDomain_"+domain+".Tasks", value);

  LayerOff(2);
  m_task.deleteAllItems();
  m_optTable.deleteAllLines();
  mappingClear(propertyEditor_changes);
  dyn_string tasks = strsplit(value,"|");
  for(int i=1; i<=dynlen(tasks); ++i)    {
    if ( strpos(tasks[i],pattern1,0) >= 0 )
      dynAppend(display,tasks[i]);
    else if ( strpos(tasks[i],pattern2,0) >= 0 )
      dynAppend(display,tasks[i]);
  }
  m_task.items           = display;
  m_task.enabled         = true;
  if ( dynlen(display) > 0 )   {
    m_task.selectedPos = 1;
    PropertyEditor_panel_select_task(display[1]);
    return;
  }
  m_loadOptions.enabled  = false;
  m_loadOptions2.enabled = false;  
  m_sendChanges.enabled  = false;
  m_sendChanges2.enabled = false;
}

/// Explorer callback: Select the task, where the options should be explored
void PropertyEditor_panel_select_task(string task)  {
  DebugTN("Selected task: "+task);
  LayerOff(2);
  m_optTable.deleteAllLines();
  mappingClear(propertyEditor_changes);
  m_loadOptions.enabled  = true;
  m_loadOptions2.enabled = true;
  m_sendChanges.enabled  = false;
  m_sendChanges2.enabled = false;
  /// Possibly call directly the loading procedure.
  /// PropertyEditor_panel_loadOptions();
}

/// Explorer callback: Load the options of a selected task into the table 
void PropertyEditor_panel_load_options()   {
  string id     = m_id.text;
  string slot   = m_slot.text;
  string domain = m_domain.text;
  LayerOff(2);
  m_optTable.deleteAllLines();
  mappingClear(propertyEditor_changes);
  if ( strlen(id) && strlen(slot) )   {
    PropertyEditor_request("set_task/id="+id+"/slot="+slot+"/domain="+domain+"/task="+m_task.selectedText);
  }
}

/// Editor callback: Callback on Double-click in option table to start editing
void PropertyEditor_panel_start_edit(int row, string column)   {
  string val_ID = m_optTable.cellValueRC(row,"ID");
  if ( 0 == strlen(val_ID) )    {
    ChildPanelOn("visionUtils/ErrorBox","Editor Error",
		 makeDynString("$1:The option with the ID "+row+"\ndoes not exist"),50,50);
    return;
  }
  m_optID.text = val_ID;
}

/// Editor callback: (Re-)load option from table to editor (Option ID is row!)
void PropertyEditor_panel_edit_option()   {
  string ids = m_optID.text;
  int    row = ids;
  row = row - 1;
  if ( 0 == strlen(ids) || row < 0 )   {
    ChildPanelOn("visionUtils/ErrorBox","Editor Error",
		 makeDynString("$1:The option with the ID "+ids+"\ndoes not exist"),50,50);
    return;
  }
  m_optClient.text = m_optTable.cellValueRC(row,"Client");
  m_optName.text   = m_optTable.cellValueRC(row,"Name");
  m_optValue.text  = m_optTable.cellValueRC(row,"Value");
  m_optTag.text    = "No. "+m_optID.text+"  "+m_optClient.text+"."+m_optName.text;
  LayerOn(2);
}

/// Editor callback: Save the edited changes back to the table and mark the row as changed.
void PropertyEditor_panel_save_option()   {
  string ids = m_optID.text;
  int    row = ids;
  row = row - 1;
  if ( 0 == strlen(ids) || row < 0 )   {
    ChildPanelOn("visionUtils/ErrorBox","Editor Error",
		 makeDynString("$1:The option with the ID "+ids+"\ndoes not exist"),50,50);
    return;
  }
  string val = m_optTable.cellValueRC(row,"Value");
  if ( val == m_optValue.text )   {
    ChildPanelOn("visionUtils/ErrorBox","Editor Warning",
		 makeDynString("$1:The option with the ID "+ids+
			       "\ndid not change:\n"+
			       "\nValue:"+m_optValue.text+
			       "\nOriginal:"+val),50,50);
    return;
  }
  m_optTable.cellValueRC(row,"Client")  = m_optClient.text;
  m_optTable.cellValueRC(row,"Name")    = m_optName.text;
  m_optTable.cellValueRC(row,"Value")   = m_optValue.text;
  m_optTable.cellValueRC(row,"Changed") = makeDynString("Edited","red","white");
  propertyEditor_changes[m_optClient.text+"/"+m_optName.text] = m_optValue.text;
  m_optClient.text = "";
  m_optName.text   = "";
  m_optValue.text  = "";
  m_optTag.text    = "";

  m_sendChanges.enabled  = true;
  m_sendChanges2.enabled = true;
  LayerOff(2);
}

/// Main panel callback: Send all changes back to the server process
void PropertyEditor_panel_send_changes()   {
  string changed, opt, client, slot = m_slot.text;
  dyn_string changes = makeDynString("setProperties");
  int rc;
  for(int row=0, n=m_optTable.lineCount(); row<n; ++row)   {
    changed = m_optTable.cellValueRC(row,"Changed");
    client  = m_optTable.cellValueRC(row,"Client")+"/"+m_optTable.cellValueRC(row,"Name");
    if ( strlen(changed) && changed == "Edited" )   {
      opt = m_optTable.cellValueRC(row,"Value");
      propertyEditor_changes[client] = opt;
      dynAppend(changes,client+" "+opt);
      DebugTN("Option "+client+" Changed value! Sending new value to task...");
    }
    else   {
      //DebugTN("Option "+client+" IDENTICAL!");
    }
  }
  rc = dpSetWait(PropertyEditor_slot_dp(slot,".SetOptions"),changes);
  if ( rc != 0 )    {
    dyn_string err = makeDynString("$1:Failed to properly send the changes "+
				   "\nto the target process:\n"+m_task.selectedText);
    for(int i=1, n=dynlen(changes); i<n; ++i)
      dynAppend(err, changes[i]);
    ChildPanelOn("visionUtils/ErrorBox","Editor Error", err, 50, 50);
    return;
  }
  for(int row=0, n=m_optTable.lineCount(); row<n; ++row)   {
    changed = m_optTable.cellValueRC(row,"Changed");
    if ( strlen(changed) )   {
      m_optTable.cellValueRC(row,"Changed") = makeDynString("Submitted","yellow","black");
    }
  }
  m_sendChanges.enabled  = false;
  m_sendChanges2.enabled = false;
}

/// Main panel callback: Close panel
void PropertyEditor_panel_close()    {
  string id = m_id.text;
  string slot = m_slot.text;
  
  ctrlUtils_trace("PropertyEditor: Disconnecting datapoints and closing panel. ID:"+id+"Slot:"+slot);
  // Clara says: No need to disconnect if the panel closes.
  //dpDisconnect("PropertyEditor_panelResponse",PropertyEditor_sysName()+":PropertyEditor.Response");
  if ( strlen(id) && strlen(slot) )    {
    //dpDisconnect("PropertyEditor_panelResponse",  PropertyEditor_slot_dp(slot,".Response"));
    //dpDisconnect("PropertyEditor_optionResponse", PropertyEditor_slot_dp(slot,".Options"));
    PropertyEditor_request("deallocate/id="+id+"/slot="+slot);
  }
  dyn_anytype daa;
  dyn_anytype pars;
  daa[1] = 0.0;
  daa[2] = "FALSE";
  pars[1] = myModuleName();
  pars[2] = myPanelName();
  pars[3] = daa;
  panelOff(pars);
}

/// PropertyEditor Server: Install property editor package
int PropertySrv_install()   {
  /* */
  dyn_dyn_string names;
  dyn_dyn_int types;
  string nam;

  // Create partition editor data type
  names[1] = makeDynString ("PropertyEditorControl","","","");
  names[2] = makeDynString ("","Request", "","");
  names[3] = makeDynString ("","Response","","");
  names[4] = makeDynString ("","Domains", "","");
  types[1] = makeDynInt (DPEL_STRUCT);
  types[2] = makeDynInt (0,DPEL_STRING);
  types[3] = makeDynInt (0,DPEL_STRING);
  types[4] = makeDynInt (0,DPEL_STRING);
  ctrlUtils_installDataType(names,types);

  // Create partition editor data type
  names[1]  = makeDynString ("PropertyEditorSlot","","","");
  names[2]  = makeDynString ("","InUse","","");
  names[3]  = makeDynString ("","User","","");
  names[4]  = makeDynString ("","Domain","","");
  names[5]  = makeDynString ("","Response","","");
  names[6]  = makeDynString ("","Options","","");
  names[7]  = makeDynString ("","SetOptions","","");
  types[1]  = makeDynInt (DPEL_STRUCT);
  types[2]  = makeDynInt (0,DPEL_INT);
  types[3]  = makeDynInt (0,DPEL_STRING);
  types[4]  = makeDynInt (0,DPEL_STRING);
  types[5]  = makeDynInt (0,DPEL_STRING);
  types[6]  = makeDynInt (0,DPEL_DYN_STRING);
  types[7]  = makeDynInt (0,DPEL_DYN_STRING);
  ctrlUtils_installDataType(names,types);

  // Create partition editor data type
  names[1] = makeDynString ("PropertyEditorDomain","","","");
  names[2] = makeDynString ("","Nodes", "","");
  names[3] = makeDynString ("","Tasks", "","");
  types[1] = makeDynInt (DPEL_STRUCT);
  types[2] = makeDynInt (0,DPEL_STRING);
  types[3] = makeDynInt (0,DPEL_STRING);
  ctrlUtils_installDataType(names,types);

  nam = "PropertyEditor";
  ctrlUtils_trace("PropertyEditor: Create datapoint: "+nam);
  ctrlUtils_printErrors(dpCreate(nam, "PropertyEditorControl", PropertyEditor_sysID()),
                        "PropertyEditor: FAILED to create datapoint: "+nam);
  for(int i=1; i<=propertyEditor_Slots; ++i)  {
    sprintf(nam,"PropertyEditorSlot_%02X",i);
    if ( !dpExists(nam) )  {
      ctrlUtils_trace("PropertyEditor: Create datapoint: "+nam);
      ctrlUtils_printErrors(dpCreate(nam, "PropertyEditorSlot", PropertyEditor_sysID()),
                            "PropertyEditor: FAILED to create datapoint: "+nam);
    }
  }

  if ( !dpExists("DimMapPropertyEditor") )   {
    int mgr_num = 100;
    nam = "DimMapPropertyEditor";
    ctrlUtils_trace("PropertyEditor: Create RPCDomainInfo config: "+nam);
    fwDim_createConfig(nam);
    fwDim_setManNum(nam, mgr_num);
  }
  fwDim_subscribeService("DimMapPropertyEditor","/RPCDomainInfo/WinCCOA/Domains","PropertyEditor.Domains","");
  /*  */
  ctrlUtils_trace("PropertyEditor: Installation finished successfully.");
  return 0;
}

/// PropertyEditor Server: Uninstall property editor package
int PropertySrv_uninstall()   {
  string config_nam = "DimMapPropertyEditor";
  fwDim_unSubscribeServices(config_nam,"*");
  fwDim_unSubscribeCommands(config_nam,"*");
  fwDim_unPublishServices(config_nam,"*");
  fwDim_unPublishCommands(config_nam,"*");
  fwDim_kill(config_nam);
  fwDim_deleteConfig(config_nam);
  ctrlUtils_uninstallDataType("PropertyEditorControl",true);
  ctrlUtils_uninstallDataType("PropertyEditorSlot",true);
  ctrlUtils_uninstallDataType("PropertyEditorDomain",true);
  return 0;
}

/// PropertyEditor Server: Clear editor slot before/after usage
int PropertySrv_slot_clear(string s)   {
  if ( dpExists(s) )    {
    dyn_string empty;
    int rc = dpSetWait(s+".InUse",0,
                       s+".User","",
                       s+".Domain","",
                       s+".Options",empty,
                       s+".SetOptions",empty,
                       s+".Response","");
    return rc;
  }
  return 0;
}

/// PropertyEditor Server: Send a slot specific reply
int PropertySrv_slot_reply(string slot, string reply)   {
  int rc = dpSetWait(PropertyEditor_slot_dp(slot,".Response"),reply);
  DebugTN("Slot-reply: "+slot+" Reply:"+reply+" rc="+rc);
  return rc;
}

/// PropertyEditor Server: Slot specific DIM config
string PropertySrv_config(int editor_id)   {
  string config_nam;
  sprintf(config_nam,"PropertyEditor_Dim_%02X",editor_id);
  return config_nam;
}


/// PropertyEditor Server: Setup DNS domain for a given slot
int PropertySrv_set_domain(string id, string slot_nam, string domain)   {
  string     reply;
  string     dp = PropertyEditor_slot_dp(slot_nam,"");
  int        rc, num_mgr = -1, slot = slot_nam;
  dyn_string empty;

  string config_nam = PropertySrv_config(slot);
  string nam = "PropertyEditorDomain_"+domain;
  if ( strlen(domain)>0 && !dpExists(nam) )   {
    ctrlUtils_printErrors(dpCreate(nam, "PropertyEditorDomain", PropertyEditor_sysID()),
                          "PropertySrv: FAILED to create datapoint: "+nam);
  }
  if ( strlen(domain)>0 )   {
    ctrlUtils_trace("PropertySrv: Subscribe SERVIVE /RPCDomainInfo/WinCCOA/"+domain+"/Nodes --> "+nam+".Nodes");
    ctrlUtils_trace("PropertySrv: Subscribe SERVICE /RPCDomainInfo/WinCCOA/"+domain+"/Tasks --> "+nam+".Tasks");
    fwDim_subscribeService("DimMapPropertyEditor",
                           "/RPCDomainInfo/WinCCOA/"+domain+"/Nodes",
                           nam+".Nodes","");
    fwDim_subscribeService("DimMapPropertyEditor",
                           "/RPCDomainInfo/WinCCOA/"+domain+"/Tasks",
                           nam+".Tasks","");
  }
  // All old stuff is now undone. Start new subscriptions.
  rc = dpSetWait(dp+".Domain", domain, dp+".Options",empty, dp+".SetOptions",empty);
  DebugTN("dpSetWait("+dp+".Domain"+", "+domain+", "+dp+".Options"+", [], "+dp+".SetOptions, []) = "+rc);
  if ( rc == 0 )  {
    if ( dpExists(config_nam) )   {
      dpSetWait(config_nam+".ApiParams.exit",1);
      fwDim_unSubscribeServices(config_nam,"*");
      fwDim_unSubscribeCommands(config_nam,"*");
      fwDim_unPublishServices(config_nam,"*");
      fwDim_unPublishCommands(config_nam,"*");
      fwDim_kill(config_nam);
      delay(1);
    }
    else   {
      fwDim_createConfig(config_nam);
      fwDim_setManNum(config_nam, 100+slot);
    }
    if ( strlen(domain)>0 )   {
      fwDim_getManNum(config_nam, num_mgr);
      fwDim_start(config_nam, num_mgr, domain);
    }
    reply = strlen(id) == 0 
      ? "set_domain/slot="+slot+"/domain="+domain+"/status=SUCCESS"
      : "set_domain/id="+id+"/slot="+slot+"/domain="+domain+"/status=SUCCESS";
    rc = PropertySrv_slot_reply(slot, reply);
    if ( rc == 0 )   {
      return rc;
    }
  }
  reply = strlen(id) == 0 
    ? "set_domain/slot="+slot+"/domain="+domain+"/status=FAILED"
    : "set_domain/id="+id+"/slot="+slot+"/domain="+domain+"/status=FAILED";
  ctrlUtils_printErrors(rc,"PropertySrv.set_domain("+slot+","+domain+") request failed.");
  PropertySrv_slot_reply(slot, reply);
  return rc;
}

/// PropertyEditor Server: Select a dask
int PropertySrv_set_task(string id, string slot_nam, string domain, string task)   {
  string     reply;
  string     dp = PropertyEditor_slot_dp(slot_nam,"");
  int        rc, num_mgr = -1, slot = slot_nam;
  dyn_string empty;

  if ( strlen(domain) > 0 )   {
    string dp_nam;
    string config_nam = PropertySrv_config(slot);

    sprintf(dp_nam,"PropertyEditorSlot_%02X",slot);
    if ( !dpExists(dp_nam) )   {
      ctrlUtils_printErrors(dpCreate(dp_nam, "PropertyEditorDomain", PropertyEditor_sysID()),
                            "PropertySrv: FAILED to create datapoint: "+dp_nam);
    }
    fwDim_unSubscribeServices(config_nam,"*");
    fwDim_unSubscribeCommands(config_nam,"*");
    ctrlUtils_trace("PropertySrv: Subscribe SERVICE "+task+"/properties/publish --> "+dp_nam+".Options");
    ctrlUtils_trace("PropertySrv: Subscribe COMMAND "+task+"/properties/set     --> "+dp_nam+".SetOptions");
    fwDim_subscribeService(config_nam, task+"/properties/publish", dp_nam+".Options","");
    fwDim_subscribeCommand(config_nam, task+"/properties/set",     dp_nam+".SetOptions","");
    reply = strlen(id) == 0 
      ? "set_task/domain="+domain+"/task="+task+"/status=SUCCESS"
      : "set_task/id="+id+"/domain="+domain+"/task="+task+"/status=SUCCESS";
    rc = PropertySrv_slot_reply(slot, reply);
    if ( rc == 0 )   {
      return rc;
    }
  }
  ctrlUtils_printErrors(rc,"PropertySrv.set_task("+slot+","+domain+","+task+") request failed.");
  reply = strlen(id) == 0 
    ? "set_task/slot="+slot+"/domain="+domain+"/task="+task+"/status=FAILED"
    : "set_task/id="+id+"/slot="+slot+"/domain="+domain+"/task="+task+"/status=FAILED";
  PropertySrv_slot_reply(slot, reply);
  return rc;
}

/// PropertyEditor Server: Send replay to the main instance
int PropertySrv_gen_reply(string reply)   {
  string nam = PropertyEditor_sysName()+":PropertyEditor.Response";
  int rc = dpSetWait(nam,reply);
  return rc;
}

/// PropertyEditor Server: Allocate editor slot
int PropertySrv_allocate_slot(string id)   {
  int        i, rc;
  dyn_int    inUse;
  dyn_string inuse_ids, slots = dpNames(PropertyEditor_sysName()+":PropertyEditorSlot_*");

  for(int i=1; i <= dynlen(slots); ++i)
    dynAppend(inuse_ids, slots[i]+".InUse");
  rc = dpGet(inuse_ids, inUse);
  if ( 0 == rc )  {
    for ( i=1; i <= dynlen(slots); ++i )  {
      if ( inUse[i] == 0 )    {
        string s = slots[i];
        PropertySrv_slot_clear(s);
        rc = dpSetWait(inuse_ids[i],1,s+".User",id);
        if ( rc == 0 )   {
          string config_nam = PropertySrv_config(i);
          if ( !dpExists(config_nam) )   {
            ctrlUtils_trace("PropertySrv: Create Editor config: "+config_nam);
            fwDim_createConfig(config_nam);
            fwDim_setManNum(config_nam, 100+i);
          }
          else   {
            dpSetWait(config_nam+".ApiParams.exit",1);
            fwDim_kill(config_nam);
            delay(1);
          }
          fwDim_unSubscribeServices(config_nam,"*");
          fwDim_unSubscribeCommands(config_nam,"*");
          fwDim_unPublishServices(config_nam,"*");
          fwDim_unPublishCommands(config_nam,"*");
          rc = PropertySrv_gen_reply("allocate/status=SUCCESS/id="+id+"/slot="+i);
          if ( rc == 0 )   {
            ctrlUtils_trace("PropertySrv: allocated slot:"+i+" For client:"+id);
            return rc;
          }
        }
      }
    }
  }
  ctrlUtils_printErrors(rc,"PropertySrv: Allocation request failed for client:"+id);
  PropertySrv_gen_reply("allocate/status=FAILED/id="+id);
  return rc;
}

/// PropertyEditor Server: Clean a slot: unsubscribe services and delete config
int PropertySrv_release_slot(int slot)    {
  string config_nam = PropertySrv_config(slot);
  string slot_nam  = PropertyEditor_slot_dp(slot, "");
  ctrlUtils_trace("PropertySrv: Cleaning slot: "+slot+" DP: "+slot_nam+" Config: "+config_nam);
  if ( dpExists(config_nam) )    {
    fwDim_unSubscribeCommands(config_nam,"*");
    fwDim_unSubscribeServices(config_nam,"*");
    fwDim_unPublishServices(config_nam,"*");
    fwDim_unPublishCommands(config_nam,"*");
    fwDim_kill(config_nam);
    fwDim_deleteConfig(config_nam);
  }
  return PropertySrv_slot_clear(slot_nam);
}

/// PropertyEditor Server: Deallocate editor slot
int PropertySrv_deallocate_slot(string id)   {
  int        i, rc;
  dyn_string clients, client_id;
  dyn_string slots = dpNames(PropertyEditor_sysName()+":PropertyEditorSlot_*");

  for(int i=1; i <= dynlen(slots); ++i)
    dynAppend(clients, slots[i]+".User");
  rc = dpGet(clients, client_id);
  if ( 0 == rc )  {
    for(i=1; i <= dynlen(client_id); ++i)  {
      if ( client_id[i] == id )    {
	rc = PropertySrv_release_slot(i);
        if ( rc == 0 )   {
          rc = PropertySrv_gen_reply("deallocate/status=SUCCESS/id="+id);
          if ( rc == 0 )   {
            ctrlUtils_trace("PropertySrv: deallocated slot:"+i+" For client:"+id);
            return rc;
          }
        }
      }
    }
  }
  ctrlUtils_printErrors(rc,"PropertySrv: Deallocation request failed for client:"+id);
  PropertySrv_gen_reply("deallocate/status=FAILED/id="+id);
  return rc;
}

/// PropertyEditor Server: Handle allocation request
void PropertySrv_handleAlloc(string dp, string value)   {
  if ( strlen(value) != 0 )  {
    string unique_id;
    mapping args;
    ctrlUtils_parse_args(value,"/",args);
    if ( mappingHasKey(args,"id") )   {
      unique_id = args["id"];
      if ( mappingHasKey(args,"allocate") )   {
        PropertySrv_allocate_slot(unique_id);
        return;
      }
      else if ( mappingHasKey(args,"deallocate") )   {
        PropertySrv_deallocate_slot(unique_id);
        return;
      }
    }
    if ( mappingHasKey(args,"slot") && mappingHasKey(args,"domain") )   {
      string slot = args["slot"];
      string domain = args["domain"];
      if ( mappingHasKey(args,"set_domain") )   {
        PropertySrv_set_domain(unique_id, slot, domain);
        return;
      }
      else if ( mappingHasKey(args,"set_task") )   {
        string task = args["task"];
        PropertySrv_set_task(unique_id, slot, domain, task);
        return;
      }
    }
    string nam;
    ctrlUtils_trace("PropertySrv: Request failed:"+value);
    PropertySrv_gen_reply("FAILED");
  }
}


/// PropertyEditor Server: Configure property editor startup
int PropertySrv_configure()   {
  string nam = PropertyEditor_sysName()+":PropertyEditor";
  dpSetWait(nam+".Response","",nam+".Request","");
  if ( !ctrlUtils_printErrors(dpConnect("PropertySrv_handleAlloc",nam+".Request"),
                              "PropertySrv: FAILED to subscribe to PropertySrv datapoint.") )  {
    ctrlUtils_trace("PropertySrv: Configuration FAILED.");
    return 1;
  }
  dyn_string slots = dpNames(nam+"Slot_*");
  for(int i=1; i<=dynlen(slots); ++i)
    PropertySrv_slot_clear(slots[i]);

  int num_mgr = -1;
  ctrlUtils_trace("PropertySrv: Create RPCDomainInfo config: DimMapPropertyEditor");
  fwDim_getManNum("DimMapPropertyEditor", num_mgr);
  fwDim_start("DimMapPropertyEditor", num_mgr, "ECS03");
  ctrlUtils_trace("PropertySrv: Configuration finished successfully.");
  return 0;
}

/// PropertyEditor Server: Listen to panel requests
int PropertySrv_listen()   {
  //propertyEditor_doExit = 1;
  return 0;
}

/// PropertyEditor Server: Main entry point to run the object
int PropertySrv_run()   {
  propertyEditor_doExit = 0;
  addGlobal("PropertyEditorSystem",STRING_VAR);
  PropertyEditorSystem = "STORAGE";
  if ( 0 == PropertySrv_configure() )  {
    if ( 0 == PropertySrv_listen() )  {
      int        rc;
      time       now;
      int        diff;
      dyn_int    inUse;
      dyn_time   slot_times;
      dyn_string slot_names = dpNames(PropertyEditor_sysName()+":PropertyEditorSlot_*");
      dyn_string used_names;
      for(int i=1; i <= dynlen(slot_names); ++i)  {
	dynAppend(used_names,slot_names[i]+".InUse");
	slot_names[i] = slot_names[i]+".InUse:_original.._stime";
      }
      ctrlUtils_trace("PropertySrv active and listening to commands.");
      while( 0 == propertyEditor_doExit )  {
        delay(5);
	rc = dpGet(slot_names, slot_times, used_names, inUse);
	if ( rc == 0 )    {
	  now = getCurrentTime();
	  for(int i=1; i <= dynlen(slot_names); ++i)   {
	    if ( inUse[i] != 0 )  {
	      diff = now - slot_times[i];
	      if ( diff > 20 )   {
		ctrlUtils_trace("Need to clean slot: "+slot_names[i]+"  Diff:"+diff);
		PropertySrv_release_slot(i);
	      }
	    }
	  }
	}
      }
      ctrlUtils_trace("PropertySrv server stop:"+propertyEditor_doExit);
    }
  }
  ctrlUtils_trace("PropertySrv server finished ans shall exit.");
  return 0;
}
