#uses "PropertyEditor.cpp"

/** Main CTRL entry point to run the Property server
  *
  * @author M.Frank
  */
main()   {
  PropertySrv_run();
}
