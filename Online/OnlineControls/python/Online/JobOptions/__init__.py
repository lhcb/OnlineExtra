# ==========================================================================
#   LHCb Online software suite
# --------------------------------------------------------------------------
#   Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
#   All rights reserved.
# 
#   For the licensing terms see OnlineSys/LICENSE.
# 
# --------------------------------------------------------------------------
#   Author     : Markus Frank
# ==========================================================================

""" Online module: Storage submodule

    @author M.Frank
    @date   1/3/2007
"""
