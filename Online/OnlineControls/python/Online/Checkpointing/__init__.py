# ==========================================================================
#   LHCb Online software suite
# --------------------------------------------------------------------------
#   Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
#   All rights reserved.
# 
#   For the licensing terms see OnlineSys/LICENSE.
# 
# --------------------------------------------------------------------------
#   Author     : Markus Frank
# ==========================================================================

"""

Python classes managing the checkpoint file creation fro HLT jobs

    @author  M.Frank

"""

