import sqlalchemy
import dqdb_params

from sqlalchemy       import Table, Column
from sqlalchemy       import Integer, Float, String
from sqlalchemy       import Sequence
from sqlalchemy       import ForeignKey, UniqueConstraint
from sqlalchemy       import MetaData
from sqlalchemy       import exc, desc
from sqlalchemy       import or_
from sqlalchemy.orm   import relationship, backref
from sqlalchemy.orm   import Session, sessionmaker
from sqlalchemy.sql   import table, column, select, update, insert

from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

context_data_file = Table('context_data_file', Base.metadata,
                          Column('contextId',  Integer, ForeignKey('context.contextId'),        nullable = False),
                          Column('dataFileId', Integer, ForeignKey('data_file.dataFileId'),     nullable = False)
                      )

run_data_file     = Table('run_data_file',     Base.metadata,
                          Column('runNumber',  Integer, ForeignKey('run.runNumber'),            nullable = False),
                          Column('dataFileId', Integer, ForeignKey('data_file.dataFileId'),     nullable = False)
                          )
context_ref_file  = Table('context_ref_file',  Base.metadata,
                          Column('contextId',  Integer, ForeignKey('context.contextId'),        nullable = False),
                          Column('refFileId',  Integer, ForeignKey('reference_file.refFileId'), nullable = False)
                          )

run_ref_file      = Table('run_ref_file',      Base.metadata,
                          Column('runNumber',  Integer, ForeignKey('run.runNumber'),            nullable = False),
                          Column('refFileId',  Integer, ForeignKey('reference_file.refFileId'), nullable = False)
                          )
run_dq_flag       = Table('run_dq_flag',       Base.metadata,
                          Column('runNumber',  Integer, ForeignKey('run.runNumber'),            nullable = False, unique = True),
                          Column('dqFlagId',   Integer, ForeignKey('dq_flags.dqFlagId'),        nullable = False, default = 15)
                          )

fill_data_property = Table('fill_data_property', Base.metadata,
                           Column('fillId',            Integer,  ForeignKey('fill.id'),                      nullable = False),
                           Column('dataPropertyId',    Integer,  ForeignKey('data_property.dataPropertyId'), nullable = False),
                           Column('dataPropertyValue', Float)
                           )
run_data_property  = Table('run_data_property', Base.metadata,
                           Column('runNumber',         Integer, ForeignKey('run.runNumber'),                nullable = False),
                           Column('dataPropertyId',    Integer, ForeignKey('data_property.dataPropertyId'), nullable = False),
                           Column('dataPropertyValue', Float)
                           )
procPass_data_file = Table('processingPass_data_file', Base.metadata,
                          Column('processingPassId',  Integer, ForeignKey('processing_pass.processingPassId'), nullable = False),
                          Column('dataFileId',        Integer, ForeignKey('data_file.dataFileId'),            nullable = False)
                      )
procPass_ref_file  = Table('processingPass_ref_file',  Base.metadata,
                          Column('processingPassId',  Integer, ForeignKey('processing_pass.processingPassId'), nullable = False, unique=True),
                          Column('refFileId',         Integer, ForeignKey('reference_file.refFileId'),        nullable = False)
                          )

class Context(Base):
    __tablename__ = 'context'
    
    contextId   = Column(Integer, Sequence('contextId_seq'), primary_key=True)
    contextName = Column(String(40), unique=True, nullable=False)

    dataFiles = relationship('DataFile',
                             secondary=context_data_file,
                             backref=backref('contexts', uselist=True)
                             )

    refFiles = relationship('ReferenceFile',
                            secondary=context_ref_file,
                            backref=backref('contexts', uselist=True),
                            uselist=True
                            )

    def __repr__(self):
        return "<Context(contextName='%s)'>" %(self.contextName)

class Run(Base):
    __tablename__ = 'run'

    runNumber = Column(Integer, primary_key=True)
    fillId    = Column(Integer, ForeignKey('fill.id'), nullable=True)

    dataFile = relationship('DataFile',
                            secondary=run_data_file,
                            backref=backref('run', uselist=False)
                            )

    refFiles = relationship('ReferenceFile',
                            secondary=run_ref_file,
                            backref=backref('runs', uselist=True),
                            uselist=True
                            )
    dqFlag = relationship('DQFlags',
                          secondary = run_dq_flag,
                          uselist   = False
                          )

    def __repr__(self):
        return "<Run(runNumber='%s')>" %(str(self.runNumber))

class Fill(Base):
    __tablename__ = 'fill'

    id   = Column(Integer, primary_key=True)
    runs = relationship(Run, backref=backref('fill'), uselist=True)

    def __repr__(self):
        return "<Fill(number='%s')>" %(str(self.id))

 
class DataFile(Base):
    __tablename__ = 'data_file'

    dataFileId   = Column(Integer, Sequence('datafile_id_seq'), primary_key=True)
    dataFilePath = Column(String(255), unique=True, nullable=False)

    def __repr__(self):
        return "<DataFile(dataFilePath='%s')>" %(self.dataFilePath)
    
class ReferenceFile(Base):
     __tablename__ = 'reference_file'
 
     refFileId   = Column(Integer, Sequence('refFile_id_seq'), primary_key=True)
     refFilePath = Column(String(255), unique=True, nullable=False)
 
     def __repr__(self):
         return "<ReferenceFile(refFilePath='%s')>" %(self.refFilePath)

class DQFlags(Base):
    __tablename__ = 'dq_flags'

    dqFlagId = Column(Integer, Sequence('dqFlag_id_seq'), primary_key=True)
    dqFlag   = Column(String(32), unique=True, nullable=False)
    
    def __repr__(self):
        return "<DQFlags(flag='%s')>" %(self.dqFlag)

class DataProperty(Base):
    __tablename__ = 'data_property'

    dataPropertyId = Column(Integer,     Sequence('dataProperty_id_seq'), primary_key=True)
    dataProperty   = Column(String(256), unique=True, nullable=False)
    
    def __repr__(self):
        return "<DataProperty(Property='%s')>" %(self.dataProperty)

class ProcessingPass(Base):
    __tablename__ = 'processing_pass'

    processingPassId = Column(Integer,   Sequence('processingPass_id_seq'), primary_key=True)
    processingPass   = Column(String(256), unique=True, nullable=False)
    
    dataFiles = relationship(DataFile,
                             uselist=True,
                             secondary=procPass_data_file,
                             backref=backref('procPass')
                             )

    refFiles = relationship(ReferenceFile,
                            uselist=False,
                            secondary=procPass_ref_file,
                            backref=backref('procPass')
                            )

    def __repr__(self):
        return "<ProcessingPass(Property='%s')>" %(self.processingPass)

class DQ_DB:
    def __init__(self):
        self.address = 'oracle://' + dqdb_params.login + ':' \
                                   + dqdb_params.pwd + '@'   \
                                   + dqdb_params.tns

        self.engine       = sqlalchemy.create_engine(self.address)
        self.engine.echo  = False
        self.conn         = self.engine.connect()
        self.base         = Base()

        self.base.metadata.create_all(self.engine)

        Session = sessionmaker(bind=self.engine)
        self.session = Session()

        self.offlineDQContextName = 'OfflineDQ'
        self.onlineDQContextName  = 'OnlineDQ'

        return
#----------------------------------------------------------------------------------------
    def addOfflineDQFile(self, run, processingPass, path):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        insFile  = self.insertDataFile(path)
        dataFile = insFile['datafile']

        insProcPass = self.insertProcessingPass(processingPass)
        procPass    = insProcPass['processingPass']

        if insFile['Added'] or insProcPass['Added']:
            self.commit()

        #
        # Check the run, the context and the processing pass are known
        #

        needToAdd = False
        if dataFile.run is None:
            dataFile.run = run
            needToAdd    = True
            
        hasOfflineDQ = False
        for c in dataFile.contexts:
            if c.contextName == self.offlineDQContextName:
                hasOfflineDQ = True

        hasProcPass = False
        for p in dataFile.procPass:
            if p.processingPass == procPass:
                hasProcPass = True

        #
        # If the run, the context or the processing pass
        # were not known add them.
        #

        if not hasOfflineDQ:
            needToAdd = True
            context   = self.getContext(self.offlineDQContextName)
            dataFile.contexts.append(context)

        if not hasProcPass:
            needToAdd = True
            dataFile.procPass.append(procPass)
            
        if needToAdd:
            self.session.add(dataFile)
            self.commit()

        return dataFile
#----------------------------------------------------------------------------------------
    def addOfflineDQRefFile(self, run, processingPass, path):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        insFile = self.insertRefFile(path)
        refFile = insFile['refFile']

        insProcPass = self.insertProcessingPass(processingPass)
        procPass    = insProcPass['processingPass']

        if insFile['Added'] or insProcPass['Added']:
            self.commit()
        #
        # Check the run, the context and the processing pass are known
        #

        hasRun = False
        for r in refFile.runs:
            if r.runNumber == run.runNumber:
                hasRun = True
                break
                
        hasOfflineDQ = False
        for c in refFile.contexts:
            if c.contextName == self.offlineDQContextName:
                hasOfflineDQ = True
                break

        hasProcPass = False
        for p in refFile.procPass:
            if p.processingPass == procPass:
                hasProcPass = True

        #
        # If the run, the context or the processing pass
        # were not known add them.
        #

        needToAdd = False

        if not hasRun:
            needToAdd = True
            refFile.runs.append(run)

        if not hasOfflineDQ:
            needToAdd = True
            context   = self.getContext(self.offlineDQContextName)
            refFile.contexts.append(context)

        if not hasProcPass:
            needToAdd = True
            refFile.procPass.append(procPass)
            
        print hasRun, hasOfflineDQ, hasProcPass
#        return

        if needToAdd:
            self.session.add(refFile)
            self.commit()

        return refFile
#----------------------------------------------------------------------------------------
    def addOnlineDQFile(self, path):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        ins      = self.insertDataFile(path)
        dataFile = ins['datafile']

        if ins['Added']:
            self.commit()
            
        hasOnlineDQ = False
        for c in dataFile.contexts:
            if c.contextName == self.onlineDQContextName:
                hasOnlineDQ = True

        if not hasOnlineDQ:
            context   = self.getContext(self.onlineDQContextName)
            dataFile.contexts.append(context)
            self.session.add(dataFile)
            self.commit()

        return dataFile
#----------------------------------------------------------------------------------------
    def addOnlineDQFileForRun(self, run, path):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """

        dataFile = self.addOnlineDQFile(path)

        if dataFile.run is None:
            dataFile.run = run
            needToAdd    = True
            self.session.add(dataFile)
            self.commit()

        return dataFile
#----------------------------------------------------------------------------------------
    def addOnlineDQRefFile(self, run, path):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        ins     = self.insertRefFile(path)
        refFile = ins['refFile']

        if ins['Added']:
            self.commit()
        #
        # Check the run and context are known
        #

        hasRun = False
        for r in refFile.runs:
            if r.runNumber == run.runNumber:
                hasRun = True
                break
                
        
        hasOnlineDQ = False
        for c in refFile.contexts:
            if c.contextName == self.onlineDQContextName:
                hasOnlineDQ = True
                break
        #
        # If either the run or the context were not known add them
        #

        needToAdd = False

        if not hasRun:
            needToAdd = True
            refFile.runs.append(run)

        if not hasOnlineDQ:
            needToAdd = True
            context   = self.getContext(self.onlineDQContextName)
            refFile.contexts.append(context)

        if needToAdd:
            self.session.add(refFile)
            self.commit()

        return refFile
#----------------------------------------------------------------------------------------
    def commit(self):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        self.session.commit()
        return

#----------------------------------------------------------------------------------------
    def create_all(self):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        self.base.metadata.create_all(bind=self.engine)
        print self.base.metadata.tables.keys()
        return
#----------------------------------------------------------------------------------------
    def deleteRunDataFile(self, runNumber, path):
        run = self.getRun(runNumber)
        if run is None:
            return False

        dataFile = self.getDataFile(path)
        if dataFile is None:
            return False

        d = run_data_file.delete()
        d = d.where(run_data_file.c.runNumber==run.runNumber)
        d = d.where(run_data_file.c.dataFileId==dataFile.dataFileId)

        res = self.conn.execute(d)

        return True
#----------------------------------------------------------------------------------------
    def deleteRunPropertyValue(self, runNumber, propertyName):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """

        propertyId = self.getDataPropertyId(propertyName)
        if propertyId is None:
            return False

        d = run_data_property.delete()
        d = d.where(run_data_property.c.runNumber==runNumber)
        d = d.where(run_data_property.c.dataPropertyId==propertyId)

        res = self.conn.execute(d)

        return True
#----------------------------------------------------------------------------------------
    def deleteRunPropertyErrValue(self, runNumber, propertyName):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """

        errName = propertyName + '_err'

        res = self.deleteRunPropertyValue(runNumber, errName)
        if not res:
            return False

        res = self.deleteRunPropertyValue(runNumber, propertyName)
        if not res:
            return False

        return True
#----------------------------------------------------------------------------------------
    def drop_all(self):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        self.commit()
        self.base.metadata.drop_all(bind=self.engine)
        return
#----------------------------------------------------------------------------------------
    def drop_table(self, table):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        smt = "DROP TABLE \"%s\"" %(table)
        s = self.conn.execute(smt)
        return
#----------------------------------------------------------------------------------------
    def getContext(self, name):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        return self.session.query(Context).filter_by(contextName=name).first()

#----------------------------------------------------------------------------------------
    def getContextId(self, name):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        contextId = None

        s = self.session.query(Context).filter_by(contextName=name)
        for r in s:
            contextId = r.contextId
        return contextId

#----------------------------------------------------------------------------------------
    def getDataFile(self, path):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        dataFile = self.session.query(DataFile).filter_by(dataFilePath=path).first()

        if dataFile:
            return dataFile
        else:
            dataFile = DataFile(dataFilePath=path)
            return dataFile

#----------------------------------------------------------------------------------------
    def getDataFileId(self, path):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        dataFileId = None

        dataFile = self.session.query(DataFile).filter_by(dataFilePath=path).first()
        if dataFile:
            dataFileId =  dataFile.dataFileId

        return dataFileId
#----------------------------------------------------------------------------------------
    def getDataProperties(self):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        dp = []
        q = self.session.query(DataProperty)
        for r in q:
            dp.append(r.dataProperty)
        return dp
#----------------------------------------------------------------------------------------
    def getDataProperty(self, name):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        dp = self.session.query(DataProperty).filter_by(dataProperty=name).first()
        return dp
#----------------------------------------------------------------------------------------
    def getDataPropertyId(self, name):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        propertyId = None

        s = self.session.query(DataProperty).filter_by(dataProperty=name)
        for r in s:
            propertyId = r.dataPropertyId
        return propertyId
#----------------------------------------------------------------------------------------
    def getDQFlagId(self, flag):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        dqFlagId = None

        dqFlag = self.session.query(DQFlags).filter_by(dqFlag=flag).first()
        if dqFlag:
            dqFlagId =  dqFlag.dqFlagId

        return dqFlagId
    
#----------------------------------------------------------------------------------------
    def getFill(self, fillId):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        run = self.session.query(Fill).filter_by(id=fillId).first()
        return run

#----------------------------------------------------------------------------------------
    def getOfflineDQFile(self, runNumber):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        ref = self.getRunFileData(runNumber, self.offlineDQContextName)
        return ref
#----------------------------------------------------------------------------------------
    def getOfflineDQFileWithProcPass(self, runNumber, processingPass):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        ref = self.getRunFileDataWithProcPass(runNumber, self.offlineDQContextName, processingPass)
        return ref
#----------------------------------------------------------------------------------------
    def getOfflineDQFiles(self, runNumber):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        ref = self.getRunFilesData(runNumber, self.offlineDQContextName)
        return ref
#----------------------------------------------------------------------------------------
    def getOfflineDQFilesWithProcPass(self, runNumber, processingPass):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        ref = self.getRunFilesDataWithProcPass(runNumber, self.offlineDQContextName, processingPass)
        return ref
#----------------------------------------------------------------------------------------
    def getOfflineDQRef(self, runNumber):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        ref = self.getRunFileRef(runNumber, self.offlineDQContextName)
        return ref
#----------------------------------------------------------------------------------------
    def getOfflineDQRefWithProcPass(self, runNumber, processingPass):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        ref = self.getRunFileRefWithProcPass(runNumber, self.offlineDQContextName, processingPass)
        return ref
#----------------------------------------------------------------------------------------
    def getOfflineDQRefs(self, runNumber):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        ref = self.getRunFilesRef(runNumber, self.offlineDQContextName)
        return ref
#----------------------------------------------------------------------------------------
    def getOnlineDQFile(self, runNumber):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        ref = self.getRunFileData(runNumber, self.onlineDQContextName)
        return ref
#----------------------------------------------------------------------------------------
    def getOnlineDQFiles(self, runNumber):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        ref = self.getRunFilesData(runNumber, self.onlineDQContextName)
        return ref
#----------------------------------------------------------------------------------------
    def getOnlineDQRef(self, runNumber):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        ref = self.getRunFileRef(runNumber, self.onlineDQContextName)
        return ref
#----------------------------------------------------------------------------------------
    def getOnlineDQRefs(self, runNumber):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        ref = self.getRunFilesRef(runNumber, self.onlineDQContextName)
        return ref
#----------------------------------------------------------------------------------------
    def getProcessingPasses(self):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        procPass = []
        q = self.session.query(ProcessingPass)
        for r in q:
            procPass.append(r.processingPass)
        return procPass
#----------------------------------------------------------------------------------------
    def getRun(self, runNumber):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        run = self.session.query(Run).filter_by(runNumber=runNumber).first()
        return run

#----------------------------------------------------------------------------------------
    def getRunFileData(self, runNumber, contextName):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        dataFile = self.getRunFileDataObj(runNumber, contextName);

        dataFilePath = None
        if dataFile:
            dataFilePath = dataFile.dataFilePath

        return dataFilePath
#----------------------------------------------------------------------------------------
    def getRunFileDataObj(self, runNumber, contextName):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        dataFile = self.session.query(DataFile).\
            filter(DataFile.run.has(runNumber=runNumber)).\
            filter(DataFile.contexts.any(contextName=contextName)).first()

        return dataFile
#----------------------------------------------------------------------------------------
    def getRunFileDataWithProcPass(self, runNumber, contextName, processingPass):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        dataFile = self.getRunFileDataWithProcPassObj(runNumber, contextName, processingPass);

        dataFilePath = None
        if dataFile:
            dataFilePath = dataFile.dataFilePath

        return dataFilePath
#----------------------------------------------------------------------------------------
    def getRunFileDataWithProcPassObj(self, runNumber, contextName, processingPass):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        dataFile = self.session.query(DataFile).\
            filter(DataFile.run.has(runNumber=runNumber)).\
            filter(DataFile.contexts.any(contextName=contextName)).\
            filter(DataFile.procPass.any(processingPass=processingPass)).first()

        return dataFile
#----------------------------------------------------------------------------------------
    def getRunFileRef(self, runNumber, contextName):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        refFilePath = None

        refFile = self.session.query(ReferenceFile).\
            filter(ReferenceFile.runs.any(runNumber=runNumber)).\
            filter(ReferenceFile.contexts.any(contextName=contextName)).first()

        if refFile:
            refFilePath = refFile.refFilePath

        return refFilePath
#----------------------------------------------------------------------------------------
    def getRunFileRefWithProcPass(self, runNumber, contextName, processingPass):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        refFilePath = None

        refFile = self.session.query(ReferenceFile).\
            filter(ReferenceFile.runs.any(runNumber=runNumber)).\
            filter(ReferenceFile.contexts.any(contextName=contextName)).\
            filter(ReferenceFile.procPass.any(processingPass=processingPass)).first()

        if refFile:
            refFilePath = refFile.refFilePath

        return refFilePath
#----------------------------------------------------------------------------------------
    def getRunFilesData(self, runNumber, contextName):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        filelist = []
        for dataFile in self.session.query(DataFile).\
                filter(DataFile.run.has(runNumber=runNumber)).\
                filter(DataFile.contexts.any(contextName=contextName)):
            filelist.append(dataFile.dataFilePath)
            
        return filelist
#----------------------------------------------------------------------------------------
    def getRunFilesDataWithProcPass(self, runNumber, contextName, processingPass):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        filelist = []
        for dataFile in self.session.query(DataFile).\
                filter(DataFile.run.has(runNumber=runNumber)).\
                filter(DataFile.contexts.any(contextName=contextName)).\
                filter(DataFile.procPass.any(processingPass=processingPass)):
            filelist.append(dataFile.dataFilePath)
            
        return filelist

#----------------------------------------------------------------------------------------
    def getRunFilesRef(self, runNumber, contextName):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        filelist = []

        for refFile in self.session.query(ReferenceFile).\
                filter(ReferenceFile.runs.any(runNumber=runNumber)).\
                filter(ReferenceFile.contexts.any(contextName=contextName)):
            filelist.append(refFile.refFilePath)

        return filelist
#----------------------------------------------------------------------------------------
    def getRunPropertyValue(self, propertyName, runNumber):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        value = None

        propertyId = self.getDataPropertyId(propertyName)
        if propertyId is None:
            return value

        s = run_data_property.select()
        s = s.where(run_data_property.c.runNumber==runNumber)
        s = s.where(run_data_property.c.dataPropertyId==propertyId)

        res = self.conn.execute(s)
        for row in res:
            value = row[2]

        return value
#----------------------------------------------------------------------------------------
    def getRunsPropertyValue(self, propertyName, runLow=0, runHigh=99999999):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        values = []

        propertyId = self.getDataPropertyId(propertyName)
        if propertyId is None:
            return values

        s = run_data_property.select()
        s = s.where(run_data_property.c.runNumber>=runLow)
        s = s.where(run_data_property.c.runNumber<=runHigh)
        s = s.where(run_data_property.c.dataPropertyId==propertyId)
        s = s.order_by(run_data_property.c.runNumber)

        res = self.conn.execute(s)
        for row in res:
            value = [row[0],row[2]]
            values.append(value)
    
        res.close()
        return values
#----------------------------------------------------------------------------------------
    def getRunsPropertyValueWithErr(self, propertyName, runLow=0, runHigh=99999999, dqFlag='ANY'):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        retval = {'OK'     : 0,
                  'values' : {}}

        propertyId = self.getDataPropertyId(propertyName)
        if propertyId is None:
            retval['OK'] = 1
            return retval
        
        propertyErr   = propertyName + '_err'
        propertyErrId = self.getDataPropertyId(propertyErr)
        if propertyErr is None:
            retval['OK'] = 2
            return retval

        if dqFlag == 'ANY':
            s = run_data_property.select()
            s = s.where(run_data_property.c.runNumber>=runLow)
            s = s.where(run_data_property.c.runNumber<=runHigh)
            s = s.where(or_(run_data_property.c.dataPropertyId==propertyId,run_data_property.c.dataPropertyId==propertyErrId))
            s = s.order_by(run_data_property.c.runNumber)
            res = self.conn.execute(s)
        else:
             res = self.session.query(run_data_property).join(Run).join(Run.dqFlag).    \
                        filter(run_data_property.c.runNumber>=runLow).                  \
                        filter(run_data_property.c.runNumber<=runHigh).                 \
                        filter(DQFlags.dqFlag == dqFlag).                               \
                        filter(or_(run_data_property.c.dataPropertyId==propertyId,      \
                                   run_data_property.c.dataPropertyId==propertyErrId)). \
                        order_by(run_data_property.c.runNumber)

        for row in res:
            runNumber = str(row[0])
            print row
            if not retval['values'].has_key(runNumber):
                retval['values'][runNumber] = [0,0]

            if row[1] == propertyId:
                retval['values'][runNumber][0] = row[2]
            elif row[1] == propertyErrId:
                retval['values'][runNumber][1] = row[2]

        if dqFlag == 'ANY':
            res.close()

        return retval
#----------------------------------------------------------------------------------------
    def getRuns(self, runLow=0, runHigh=99999999):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        runs = []
        for row in self.session.query(Run).         \
                    filter(Run.runNumber>=runLow).  \
                    filter(Run.runNumber<=runHigh). \
                    order_by(Run.runNumber):
            nextRun = row.runNumber
            runs.append(row.runNumber)
        return runs
#----------------------------------------------------------------------------------------
    def getRunsWithDQFlag(self, dqFlag, runLow=0, runHigh=99999999):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        runs = []
        for row in self.session.query(Run).join(Run.dqFlag). \
                    filter(Run.runNumber>=runLow).           \
                    filter(Run.runNumber<=runHigh).          \
                    filter(DQFlags.dqFlag == dqFlag).        \
                    order_by(Run.runNumber):
            nextRun = row.runNumber
            runs.append(row.runNumber)
        return runs

#----------------------------------------------------------------------------------------
    def insertContext(self, name):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        context = self.session.query(Context).filter_by(contextName=name).first()
 
        if context:
            return context
        else:
            context = Context(contextName=name)
            self.session.add(context)
            return context

#----------------------------------------------------------------------------------------
    def insertDataFile(self, path):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        retval = {'Added'    : False,
                  'datafile' : None}
        dataFile = self.session.query(DataFile).filter_by(dataFilePath=path).first()

        if dataFile:
            retval['datafile'] = dataFile
            return retval
        else:
            dataFile = DataFile(dataFilePath=path)
            self.session.add(dataFile)

            retval['datafile'] = dataFile
            retval['Added']    = True

            return retval
#----------------------------------------------------------------------------------------
    def insertDataProperty(self, dataPropertyName):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        retval       = {'Added'        : False,
                        'dataProperty' : None}
        dataProperty = self.session.query(DataProperty).filter_by(dataProperty=dataPropertyName).first()

        if dataProperty:
            retval['dataProperty'] = dataProperty
            return retval
        else:
            dataProperty = DataProperty(dataProperty=dataPropertyName)
            self.session.add(dataProperty)

            retval['dataProperty'] = dataProperty
            retval['Added']        = True

            return retval
#----------------------------------------------------------------------------------------
    def insertProcessingPass(self, processingPassPath):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        retval       = {'Added'          : False,
                        'processingPass' : None}
        processingPass = self.session.query(ProcessingPass).filter_by(processingPass=processingPassPath).first()

        if processingPass:
            retval['processingPass'] = processingPass
            return retval
        else:
            processingPass = ProcessingPass(processingPass=processingPassPath)
            self.session.add(processingPass)

            retval['processingPass'] = processingPass
            retval['Added']          = True

            return retval
#----------------------------------------------------------------------------------------
    def insertDQFlag(self, flag):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        retval = {'Added'  : False,
                  'dqFlag' : None}

        dqFlag = self.session.query(DQFlags).filter_by(dqFlag=flag).first()

        if dqFlag:
            retval['dqFlag'] = dqFlag
            return retval
        else:
            dqFlag = DQFlags(dqFlag=flag)
            self.session.add(dqFlag)

            retval['dqFlag'] = dqFlag
            retval['Added']  = True

            return retval

#----------------------------------------------------------------------------------------
    def insertFill(self, fillId):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        fill = self.session.query(Fill).filter_by(id=fillId).first()

        if fill:
            return fill
        else:
            fill = Fill(id=fillId)
            self.session.add(fill)
            return fill

#----------------------------------------------------------------------------------------
    def insertRefFile(self, path):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        retval = {'Added'    : False,
                  'refFile' : None}

        refFile = self.session.query(ReferenceFile).filter_by(refFilePath=path).first()

        if refFile:
            retval['refFile'] = refFile
            return retval
        else:
            refFile = ReferenceFile(refFilePath=path)
            self.session.add(refFile)

            retval['refFile'] = refFile
            retval['added']   = True

            return retval

#----------------------------------------------------------------------------------------
    def insertRun(self, runNumber, fillId):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        run = self.getRun(runNumber)

        if run:
            return run
        else:
            fill = self.insertFill(fillId)
            run  = Run(runNumber=runNumber, fillId=fillId)
            self.session.add(run)
            return run
#----------------------------------------------------------------------------------------
    def listTables(self):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        tables = self.base.metadata.tables.keys()
        print tables
        return
#----------------------------------------------------------------------------------------
    def nextOnlineDQRun(self, runNumber):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        nextRun = None
        for row in self.session.query(Run).         \
                    filter(Run.runNumber>runNumber).\
                    order_by(Run.runNumber).limit(1):
            nextRun = row.runNumber
        return nextRun

#----------------------------------------------------------------------------------------
    def nextOnlineDQRunUnchecked(self, runNumber):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        nextRun = None
        for row in self.session.query(Run).join(Run.dqFlag). \
                    filter(Run.runNumber>runNumber).         \
                    filter(DQFlags.dqFlag == 'UNCHECKED').   \
                    order_by(Run.runNumber).limit(1):
            nextRun = row.runNumber
        return nextRun

#----------------------------------------------------------------------------------------
    def prevOnlineDQRun(self, runNumber):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        prevRun = None
        for row in self.session.query(Run).              \
                        filter(Run.runNumber<runNumber). \
                        order_by(desc(Run.runNumber)).limit(1):
            prevRun = row.runNumber
        return prevRun

#----------------------------------------------------------------------------------------
    def prevOnlineDQRunUnchecked(self, runNumber):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        nextRun = None
        for row in self.session.query(Run).join(Run.dqFlag). \
                    filter(Run.runNumber<runNumber).         \
                    filter(DQFlags.dqFlag == 'UNCHECKED').   \
                    order_by(desc(Run.runNumber)).limit(1):
            nextRun = row.runNumber
        return nextRun

#----------------------------------------------------------------------------------------
    def rollback(self):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        self.session.rollback()
        return
#----------------------------------------------------------------------------------------
    def setRunDQFlag(self, run, flag):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        retval = {'OK'       : True,
                  'Modified' : True}

        if type(run) is int:
            run = self.getRun(run)
        elif type(run) is str:
            run = self.getRun(int(run))

        if run is None:
            retval['OK'] = False
            return retval
        #
        # Get the DQ flag id number
        #

        dqFlagId = self.getDQFlagId(flag)

        if dqFlagId is None:
            retval['OK'] = False
            return retval

        stmt = 0
        if run.dqFlag is None:
            stmt = run_dq_flag.insert()
            stmt = stmt.values(runNumber=run.runNumber,dqFlagId=dqFlagId)
        else:
            if run.dqFlag.dqFlag == flag:
                retval['Modified'] = False
            else:
                stmt = run_dq_flag.update()
                stmt = stmt.values(dqFlagId=dqFlagId)
                stmt = stmt.where(run_dq_flag.c.runNumber==run.runNumber)

        if stmt != 0:
            self.conn.execute(stmt)

        return retval

#----------------------------------------------------------------------------------------
    def setRunPropertyValue(self, run, name, value):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        retval = {'OK'       : True,
                  'Modified' : True}

        if type(run) is int:
            run = self.getRun(run)
        elif type(run) is str:
            run = self.getRun(int(run))

        if run is None:
            retval['OK'] = False
            return retval

        propertyId = self.getDataPropertyId(name)
        if propertyId is None:
            retval['OK'] = False
            return retval

        stmt = 0
        oldValue = self.getRunPropertyValue(name, run.runNumber)
        if oldValue is None:
            stmt = run_data_property.insert()
            stmt = stmt.values(runNumber=run.runNumber,dataPropertyId=propertyId,dataPropertyValue=value)
        else:
            if value == oldValue:
                retval['Modified'] = False
            else:
                stmt = run_data_property.update()
                stmt = stmt.values(dataPropertyValue=value)
                stmt = stmt.where(run_data_property.c.runNumber==run.runNumber)
                stmt = stmt.where(run_data_property.c.dataPropertyId==propertyId)
        if stmt != 0:
            self.conn.execute(stmt)

        return retval
#----------------------------------------------------------------------------------------
    def updateOfflineDQDataFile(self, run, path):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """

        oldDataFile = self.getRunFileDataObj(run.runNumber, self.offlineDQContextName)

        ins         = self.insertDataFile(path)
        newDataFile = ins['datafile']

        if ins['Added']:
            self.commit()

        u = run_data_file.update()
        u = u.values(dataFileId=newDataFile.dataFileId)
        u = u.where(run_data_file.c.runNumber==run.runNumber)
        u = u.where(run_data_file.c.dataFileId==oldDataFile.dataFileId)

        self.conn.execute(u)
        return
#----------------------------------------------------------------------------------------
    def updateOnlineDQDataFile(self, run, path):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """

        oldDataFile = self.getRunFileDataObj(run.runNumber, self.onlineDQContextName)
        newDataFile = self.addOnlineDQFile(path)

        u = run_data_file.update()
        u = u.values(dataFileId=newDataFile.dataFileId)
        u = u.where(run_data_file.c.runNumber==run.runNumber)
        u = u.where(run_data_file.c.dataFileId==oldDataFile.dataFileId)

        self.conn.execute(u)
        return
#----------------------------------------------------------------------------------------
    def  updateOnlineDQRef(self, run, refPath):
        """ 
         \author  M. Adinolfi
         \version 1.0
        """
        refFile   = self.addOnlineDQRefFile(run, refPath)
        contextId = self.getContextId('OnlineDQ')

        u = run_ref_file.update()
        u = u.values(refFileId=refFile.refFileId)
        u = u.where(run_ref_file.c.runNumber==run.runNumber)

        self.conn.execute(u)
        return
