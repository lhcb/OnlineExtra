//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : PVSS
//
//  Author     : Markus Frank
//==========================================================================

#include "PVSSManager/SyncWaitForAnswer.h"
#include "PVSS/DevAnswer.h"

#include "Manager.hxx"
#include "AnswerGroup.hxx"

using namespace PVSS;


/// Start working on request
void SyncWaitForAnswer::setAnswerState(DevAnswer::State s) const {
  if ( m_answer ) m_answer->setState(s);
}

/// Add error to answer structure
void SyncWaitForAnswer::addError(const char* txt)  const  {
  if ( m_answer )
    m_answer->addError(txt);
  else 
    ::printf("PVSS> ----------Error:%s\n",txt);
}

/// Collect possible errors
bool SyncWaitForAnswer::collectErrors(DpMsgAnswer &answer)  const {
  bool err = false;
  for(AnswerGroup *g=answer.getFirstGroup(); g; g=answer.getNextGroup() )  {
    if ( g->getError() )  {
      err = true;
      addError(g->getError()->getErrorText());
    }
  }
  return err;
}

void SyncWaitForAnswer::hotLinkCallBack(DpMsgAnswer &answer)  {
  setAnswerState(DevAnswer::WORKING);
  call(answer);
  setAnswerState(collectErrors(answer) ? DevAnswer::ERROR : DevAnswer::FINISHED);
}

