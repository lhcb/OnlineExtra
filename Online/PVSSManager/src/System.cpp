//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : PVSS
//
//  Author     : Markus Frank
//==========================================================================

#include "PVSS/Internals.h"

// PVSS include files
#include "Resources.hxx"
#include "Manager.hxx"
#include "ErrHdl.hxx"

#include <cstdio>

int PVSS::systemID(const char* name)   {
  if ( name && name[0] != 0 ) {
    SystemNumType sysNum = 0;
    DpIdentificationResult res = Manager::getDpIdentificationPtr()->getSystemId(name,sysNum);
    if ( 0 == res ) {
      return sysNum;
    }
  }
  return 0;
}

int PVSS::defaultSystemID()   {
  return DpIdentification::getDefaultSystem();
}

const char* PVSS::defaultSystemName()   {
  static char s_sysName[256];
  PVSS::UString name("Unknown");
  s_sysName[0] = 0;
  if ( 0 == Manager::getDpIdentificationPtr()->getSystemName(DpIdentification::getDefaultSystem(),name.str) ) {
    ::strcpy(s_sysName,name.cstr);
    delete [] name.str;
  }
  return s_sysName;
}

const char* PVSS::eventHostName()   {
  return Resources::getEventHost().c_str();
}


const char* PVSS::dataHostName()   {
  return Resources::getDataHost().c_str();
}


#if 0
namespace PVSS {
  std::vector<std::string> PVSS::systemNames();
}
std::vector<std::string> PVSS::systemNames() {
  size_t count = 0;
  SystemNumType* ids = 0;
  CharString*    names = 0;
  std::vector<std::string> res;
  if ( 0 == Manager::getDpIdentificationPtr()->getAllSystems(ids,names,count) ) {
    for(size_t i=0; i<count; ++i) {
      res.push_back(names[i])
    }
    if ( count > 0 ) {
      delete [] ids;
      delete [] names;
    }
  }
  return res;
}
#endif
int PVSS::pvss_print(int severity, int type, const char* message)  {
  ErrHdl::error(ErrClass::ErrPrio(severity),
                ErrClass::ERR_CONTROL,
                type,
                message);
  return 1;
}
