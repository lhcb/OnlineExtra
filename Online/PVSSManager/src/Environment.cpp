//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : PVSS
//
//  Author     : Markus Frank
//==========================================================================

#include "PVSS/DevAnswer.h"
#include "PVSS/Internals.h"
#include "PVSS/Environment.h"

using namespace PVSS;

Environment::Environment()  {
}

Environment& PVSS::Environment::instance()  {
  static PVSS::Environment e;
  return e;
}

bool Environment::waitForAnswer(DevAnswer* answer)  {
  for(;;)  {
    DevAnswer::State s = answer->state();
    if ( s == DevAnswer::ERROR ) return false;
    if ( s == DevAnswer::FINISHED ) return true;
    pvss_sleep(1);
  }
  return false;
}

