//====================================================================
//  Comet
//--------------------------------------------------------------------
//
//  Package    : Stomp
//
//  Description: DIM enabled Stomp
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//====================================================================
#ifndef STOMP_SUBSCRIBEHANDLER_H
#define STOMP_SUBSCRIBEHANDLER_H 1

// Framework include files
#include "CPP/Interactor.h"
// C++ include files
#include <string>
#include <map>

/*
 *   Stomp namespace declaration
 */
namespace Stomp  {

  // Forward declarations
  class Connector;

  /**@class SubscribeHandler SubscribeHandler.h Stomp/SubscribeHandler.h
   *
   * @author M.Frank
   */
  class SubscribeHandler : public CPP::Interactor {
  public:
    /// Definition of connection points
    typedef std::map<std::string,Connector*>   Connections;
  protected:
    /// Service data handler
    CPP::Interactor* m_sender;
    /// Map with known connections
    Connections m_con;
  public:
    /// Default constructor
    SubscribeHandler(const std::string& ini, CPP::Interactor* sender);
    /// Standard destructor
    virtual ~SubscribeHandler();
    /// Load services from file
    void loadServices(const std::string& ini);

    /// Event handler: Subscribes to new connections
    virtual void handle(const CPP::Event& ev)  override;
    /// Ioc event handler
    virtual void handleIoc(const CPP::Event& ev);
    /// Network event handler
    void handleNet(const CPP::Event& ev);
  };
}      // End namespace Stomp
#endif // STOMP_SUBSCRIBEHANDLER_H
