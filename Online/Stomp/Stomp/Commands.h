//====================================================================
//  Stomp
//--------------------------------------------------------------------
//
//  Package    : Stomp
//
//  Description: DIM enabled Stomp plugin for Apache
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//====================================================================
#ifndef STOMP_COMMANDS_H
#define STOMP_COMMANDS_H 1

namespace Stomp {

  enum Commands {
    CMD_SUBSCRIBE,
    CMD_UNSUBSCRIBE,
    CMD_DATA,
    CMD_PUBLISH
  };
}

#endif /* STOMP_COMMANDS_H */

