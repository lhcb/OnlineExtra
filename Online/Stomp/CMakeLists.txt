################################################################################
# Package: Stomp
################################################################################
gaudi_subdir(Stomp v1r0)

gaudi_depends_on_subdirs(Online/OnlineBase)
gaudi_depends_on_subdirs(Online/OnlineKernel)
gaudi_depends_on_subdirs(Online/DIM)

find_package(APR REQUIRED)
gaudi_add_library(Stomp
                  src/*.cpp
                  PUBLIC_HEADERS Stomp
                  INCLUDE_DIRS   OnlineBase OnlineKernel APR
                  LINK_LIBRARIES OnlineBase OnlineKernel dim APR ${CMAKE_DL_LIBS})

add_APR_definitions(Stomp)
