//==========================================================================
// LHCb Web Status displays
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For details please see       Online/Stomp/README
// For the licensing terms see  Online/Stomp/LICENSE
//
// Author     : M.Frank
//
//==========================================================================

var lbExt = null;

if ( !lbExt )   {
  lbExt = new Object();
 }

if ( !lbExt.lhcb_logo )   {
  var _dataLogger     = null;

  var LOG_ERROR       = 0;
  var LOG_WARNING     = 1;
  var LOG_INFO        = 2;
  var LOG_DEBUG       = 3;
  var LOG_VERBOSE     = 4;

  /// Check if running with version 3 or lower
  if ( Ext.versions && !Ext.version )  {
    Ext.require(['*']);
    Ext.require(['Ext.chart.*']);
  }

  lbExt.logger      = null;
  lbExt.zoomIn      = zoom_increaseFontSize;
  lbExt.zoomOut     = zoom_decreaseFontSize;
  lbExt.setFontSize = zoom_changeFontSizeEx;

  lbExt.lhcb_dark   = '#0C74B9';
  lbExt.lhcb_light  = '#DAF0FB';

  lbExt._id_count = 123456;
  lbExt.newId = function()   {
    ++lbExt._id_count;
    return lbExt._id_count;
  };

  lbExt.lhcb_logo = function(url)  {
    return '<IMG SRC="'+url+'" HEIGHT="50"></IMG>';
  };

  lbExt.lhcb_online_picture = function()  {
    return this.lhcb_logo("Images/lhcb-online-logo.png");   
  };

  lbExt.setWindowTitle = function(title) {
    try {
      top.document.title = title;
    }
    catch(error) {
    }
  };

  lbExt.gotoPage = function(url)   {
    document.location = url;
  };

  /// Patch the inner component style to get the text properly vertically aligned....
 fix_css: {
    for( var i=0; i< document.styleSheets.length; ++i ) {
      var sheet = document.styleSheets[i];
      if ( sheet.href.indexOf('.css') != -1){
	rules = sheet[document.all ? 'rules' : 'cssRules'];
	for( var n=0; n<rules.length; ++n )   {
	  var rule = rules[n];
	  if( rule.selectorText == '.x-autocontainer-innerCt' )  {
	    rule.style.verticalAlign = 'middle';
	    break fix_css;
	  }
	}
      }
    }
  };

  if ( Ext.define )   {
    Ext.define('Extjs.contribs.chart.axis.layout.Logarithmic', {
      extend: 'Ext.chart.axis.layout.Continuous',
          alias: 'axisLayout.logarithmic',
  
          config: {  adjustMinimumByMajorUnit: false,
            adjustMaximumByMajorUnit: false
            },
  
          /**
           * Convert the value from the normal to the log10 version.
           * NB: The renderer for the field needs to do the inverse.
           * It is also advised that 0 and negative values be excluded, as they will return negative Infinity
           */
          getCoordFor: function (value, field, idx, items) {
          return Math.log10(value);
        },

          /**
           * Called by the parent class to build the 'tick marks', which also determines where the grid lines get shown.
           * The default behaviour is to get an even spread. Instead, we want to show the grid lines on the multiples of the
           * power of tens - e.g. 1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100, and so forth
           */
          snapEnds: function (context, min, max, estStepSize) {
          var segmenter = context.segmenter;
          var axis = this.getAxis();
          var majorTickSteps = axis.getMajorTickSteps();
          // if specific number of steps requested and the segmenter can do such segmentation
          var out = majorTickSteps && segmenter.exactStep ?
            segmenter.exactStep(min, (max - min) / majorTickSteps) :
            segmenter.preferredStep(min, estStepSize);
          var unit = out.unit;
          var step = out.step;
          var from = segmenter.align(min, step, unit);

          // Calculate the steps.
          var steps = [];
	  for (var magnitude = Math.floor(min); magnitude < Math.ceil(max); magnitude++) {
	    var baseValue = Math.pow(10, magnitude);
	    for (var increment = 1; increment < 10; increment++) {
	      var value = baseValue * increment;
	      var logValue = Math.log10(value);
	      if (logValue > min && logValue < max) {
		steps.push(logValue);
	      }
	    }
	  }
	  
          return {
          min: segmenter.from(min),
              max: segmenter.from(max),
              from: from,
              to: segmenter.add(from, steps * step, unit),
              step: step,
              steps: steps.length,
              unit: unit,
              get: function (current) {
              return steps[current]
                };
          };
        },
	  
          // Trimming by the range makes the graph look weird. So we don't.
          trimByRange: Ext.emptyFn
	  
          }, function() {
        // IE (and the PhantomJS system) do not have have a log10 function. So we polyfill it in if needed.
        Math.log10 = Math.log10 || function(x) {
          return Math.log(x) / Math.LN10;
        };
      });
    
    Ext.define('Extjs.contribs.chart.axis.segmenter.Logarithmic', {
      extend: 'Ext.chart.axis.segmenter.Numeric',
	  alias: 'segmenter.logarithmic',
	  config: {  minimum: 200   },
	  renderer: function (value, context) {
	  return (Math.pow(10, value)).toFixed(3);
	} });

    Ext.define('Extjs.contribs.chart.axis.Logarithmic', {
      extend: 'Ext.chart.axis.Numeric',
          requires: [
                     'Extjs.contribs.chart.axis.layout.Logarithmic',
                     'Extjs.contribs.chart.axis.segmenter.Logarithmic'
                     ],
  
          type: 'logarithmic',
          alias: [
                  'axis.logarithmic',
                  ],
          config: {
        layout: 'logarithmic',
            segmenter: 'logarithmic',
            }
      });
  }

      
  lbExt.create = function(name, opts)  {
    if ( name == 'Ext.panel.Panel' )
      return Ext.create('Ext.panel.Panel',opts);
    else if ( name == 'Ext.Panel' )
      return Ext.create('Ext.panel.Panel',opts);
    else if ( name == 'Ext.Viewport' )
      return Ext.create('Ext.Viewport',opts);
    else   {
      var obj = Ext.create(name,opts);
      if ( obj ) return obj;
    }
    alert('Ext.create: Unknown component type:'+name);
  };

  /// Set options to object 'to' from the properties in 'from'
  /**   
   * \author  M.Frank
   * \version 1.0
   */
  lbExt.setOptions = function(to, from)  {
    if ( from && to )  {
      for (var property in from)   {
	if (from.hasOwnProperty(property))  {
	  to[property] = from[property];
	}
      }
      return to;
    }
  };

  lbExt.initZoom = function(opts)  {
    var width = Ext.getBody().getWidth();
    var font_size = (opts && opts.fontSize) ? opts.fontSize : null;
    // Apply zoom depending on the screen size and the browser:
    if      ( null == font_size && width>2000 ) font_size =  4;
    else if ( null == font_size && width>1600 ) font_size =  2;
    else if ( null == font_size && width>1200 ) font_size =  1;
    else if ( null == font_size && width>800  ) font_size =  1;
    else if ( null == font_size && width>600  ) font_size = -1;
    else if ( null == font_size && width>300  ) font_size = -2;
    else font_size = 1;
    console.info('initZoom: body-size: '+width+' x '+Ext.getBody().getHeight()+
		 ' --> font-size:'+font_size);

    if ( _isInternetExplorer() ) zoom_changeFontSizeEx(2);
    else  zoom_changeFontSizeEx(0);
    if ( font_size ) zoom_changeFontSizeEx(font_size);
    font_size = null;
  };

  lbExt.setCursorLink = function()  {
    document.body.style.cursor="pointer";  
  };

  lbExt.setCursorDefault = function()  {
    document.body.style.cursor="default";  
  };

  lbExt.panelDefaults = function(opts, other_opts)  {
    if ( !opts.hasOwnProperty('renderTo')         ) opts.renderTo =         Ext.getBody();
    if ( !opts.hasOwnProperty('xtype')            ) opts.xtype =            'panel';
    if ( !opts.hasOwnProperty('frame')            ) opts.frame =            true;
    if ( !opts.hasOwnProperty('animCollapse')     ) opts.animCollapse =     true;
    if ( !opts.hasOwnProperty('autoScroll')       ) opts.autoScroll =       false;
    if ( !opts.hasOwnProperty('autoHeight')       ) opts.autoHeight =       true;
    if ( !opts.hasOwnProperty('autoWidth')        ) opts.autoWidth =        true;
    if ( !opts.hasOwnProperty('closable')         ) opts.closable =         false;
    if ( !opts.hasOwnProperty('collapsed')        ) opts.collapsed =        false;
    if ( !opts.hasOwnProperty('collapsible')      ) opts.collapsible =      true;
    if ( !opts.hasOwnProperty('hideCollapseTool') ) opts.hideCollapseTool = false;
    if ( !opts.hasOwnProperty('collapseFirst')    ) opts.collapseFirst =    true;
    if ( !opts.hasOwnProperty('width')            ) opts.width =            '100%';
    if ( !opts.hasOwnProperty('height')           ) opts.height =           '100%';
    if ( !opts.hasOwnProperty('cls')              ) opts.cls =              'Bg-lhcblight';
    if ( !opts.hasOwnProperty('bodyCls')          ) opts.bodyCls =          'Bg-lhcblight';
    return lbExt.setOptions(opts, other_opts);
  };

  /// Create error logger panel for debugging.
  /**
   *  Mostly useless these days, since we discovered the console....
   *
   *  \author  M.Frank
   *  \version 1.0
   */
  lbExt.logger = function(opts)   {
    var logger_opts = {
    renderTo:     Ext.getBody(),
    bodyStyle: {  padding: 12,
		  fontweight: 'bold',
		  textalign:  'left',
      },
    title:            'Comet and JavaScript debug messages',
    html:             'Buffer for debug messages from JavaScript',
    visible:          false,
    frame:            true,
    animCollapse:     true,
    autoScroll:       true,
    autoHeight:       true,
    autoWidth:        true,
    closable:         false,
    collapsed:        true,
    collapsible:      true,
    hideCollapseTool: false,
    collapseFirst:    true,
    width:            '100%',
    height:           300
    };

    lbExt.setOptions(logger_opts,opts);
    var panel = lbExt.create('Ext.panel.Panel',logger_opts);
    var len = opts.active ? 200 : -1;
    panel.lines        = null;
    if ( len>0 ) {
      panel.lines = new Array();
      for(var i=0; i<len; ++i) panel.lines.push('');
    }
    panel.length  = len;
    panel.level   = opts.level;
    panel.curr    = 0;

    panel.error   = function(msg)   {  return this.print(LOG_ERROR,msg);     };  
    panel.info    = function(msg)   {  return this.print(LOG_INFO,msg);      };
    panel.warning = function(msg)   {  return this.print(LOG_WARNING,msg);   };
    panel.debug   = function(msg)   {  return this.print(LOG_DEBUG,msg);     };
    panel.verbose = function(msg)   {  return this.print(LOG_VERBOSE,msg);   };

    /// Main function, print a new message
    panel.print = function(level, msg) {
      if ( console && console.log )   {
        var d = new Date().toLocaleString();
        if ( level == LOG_VERBOSE )
          console.debug(d+' [VERBOSE]:  '+msg);
        else if ( level == LOG_DEBUG )
          console.log  (d+ ' [DEBUG]:    '+msg);
        else if ( level == LOG_INFO )
          console.info (d+ ' [INFO]:     '+msg);
        else if ( level == LOG_WARNING )
          console.warn (d+ ' [WARNING]:  '+msg);
        else if ( level == LOG_ERROR )
          console.error(d+' [ERROR]:    '+msg);
        else
          console.warn (d+ ' [UNKNOWN]:  '+msg);
      }
      if ( panel.lines != null && level >= panel.level && panel.length>0 ) {
        if ( panel.lines.length < panel.length ) {
          panel.lines.length = panel.lines.length+1;
        }
        if ( panel.curr>panel.lines.length-1 ) panel.curr = 0;
        if ( level == LOG_VERBOSE ) {
          panel.lines[panel.curr] = panel.format(d+' [VERBOSE]:  '+msg);
        }
        else if ( level == LOG_DEBUG ) {
          panel.lines[panel.curr] = panel.format(d+' [DEBUG]:    '+msg);
        }
        else if ( level == LOG_INFO ) {
          panel.lines[panel.curr] = panel.format(d+' [INFO]:     '+msg);
        }
        else if ( level == LOG_ERROR ) {
          panel.lines[panel.curr] = panel.format(d+' [ERROR]:    '+msg);
        }
        else  {
          panel.lines[panel.curr] = panel.format(d+' [UNKNOWN]:  '+msg);
        }
        panel.showMessages();
        panel.curr = panel.curr + 1;
      }
      d = null;
      return panel;
    };

    /// Format the output message accordingly
    panel.format = function(expr) {
      var s = prettyprint(expr);
      s = htmlescape(s);
      return s;
    };

    /// Escape special characters to not screw up the output
    var htmlescape = function(expr) {
      var s = expr.replace("&", "&amp;", "g");
      s = s.replace("<", "&lt;", "g");
      s = s.replace(">", "&gt;", "g");
      s = s.replace(" ", "&nbsp;", "g");
      s = s.replace("\n", "<br></br>", "g");
      return s;
    };

    /// Pretty printing function
    var prettyprint = function(s) {
      var q = "(";
      if(typeof(s) == "string") return s;
      for (var i=0; i<s.length; i++) {
        if (typeof(s[i]) != "object")
          q += s[i];
        else
          q += prettyprint(s[i]);
        if (i < s.length -1)
          q += " ";
      }
      q += ")";
      return q;
    };

    /// Redraw the buffered messages 
    panel.showMessages = function() {
      if ( panel.lines && panel.lines.length > 0 ) {
        var message = '</b><pre>';
        for(var i=panel.curr+1; i<panel.lines.length; ++i)  {
          if ( panel.lines[i].length > 0 )
            message += '&rarr; ' + i + ': ' + panel.lines[i] + '<br></b>';
        }
        for(var i=0; i<=panel.curr; ++i)  {
          message += '&rarr; ' + i + ': ' + panel.lines[i] + '<br></b>';
        }
        message = message + '</pre>';
        panel.setHtml(message);
        message = null;
      }
      return panel;
    };

    /// Clear message panel
    panel.clear = function() {
      if ( panel.lines )   {
        for(var i=0; i<=panel.lines.length; ++i)
          panel.lines[i] = '';
        panel.curr         = 0;
        panel.showMessages();
      }
    };

    if ( !opts.active ) panel.hide();
    _dataLogger  = panel;
    lbExt.logger = panel;
    return panel;
  };


  /** function element
   *  
   *  Note:
   *  "element" sets up a ExtJs component for lbcomet.
   *
   *  \author  M.Frank
   *  \version 1.0
   */
  lbExt.element = function(el)  {
    /// Attach address to object
    /**
     *   \param addr   Topic address for comet service
     *   \return On success reference to self, null otherwise
     */
    el.address = function(addr)  {
      if ( addr.substr(0,6) == 'lbWeb.' ) this.addr = addr;
      else this.addr = 'lbWeb.'+addr;
      return this;
    };
    /// Attach format for value->string conversion
    /**
     *   \param fmt   Data format as string for sprintf
     *   \return On success reference to self, null otherwise
     */
    el.format = function(fmt)  {
      this.fmt  = fmt;
      return this;
    };

    /// Standard callback for data provider for updates
    /**
     *   \param data   Data value as string
     *   \return On success reference to self, null otherwise
     */
    el.set = function(data)  {
      return this.display(data);
    };

    /// Subscribe item to receive data from data provider object
    /**
     *  \param      provider  Data provider object
     *  \return On success reference to self, null otherwise
     */
    el.subscribe = function(provider) {
      if ( !this.addr )   {
        alert('Bad subscribe:'+this);
      }
      provider.subscribe(this.addr,this);
      return this;
    };

    /// Attach conversion function for value->string conversion
    /**
     *   \param func Data conversion function. Must return a string.
     *   \return On success reference to self, null otherwise
     */
    el.convert = function(func)  {
      this.conversion = func;
      return this;
    };

    /// Attach parasitic data listener to the comet topic.
    /**
     *   \param func Callback function to be called.
     *   \return On success reference to self, null otherwise
     */
    el.listen = function(object,func)  {
      object.listeners.push({object: this,func: func});
      return this;
    };

    /// Primitive extraction function for received data
    /**
     *  \param      data    Display data
     *  \return On success reference to self, null otherwise
     */
    el.data = function(data)  {
      var item_data;
      if ( data[0] == 21 )        // Integer
        item_data = parseInt(data[1]);
      else if ( data[0] == 22 )   // Float
        item_data = parseFloat(data[1]);
      else if ( data[0] == 23 )   // Boolean: handle like string
        item_data = data[1];
      else if ( data[0] == 25 )   // String
        item_data = data[1];
      else
        item_data = data[1];
      return item_data;
    };

    /// Display rendering function for received data
    /**
     *  \param      data    Display data
     *  \return On success reference to self, null otherwise
     */
    el.display = function(data)  {
      var i, item_data = this.data(data);
      if ( this.listeners )  {
        for(i=0; i<this.listeners.length; ++i)   {
          this.listeners[i].func(data);
          this.listeners[i].object.display(data);
        }
      }
      if ( this.conversion != null )   {
        this.conversion.target = this;
        item_data = this.conversion(item_data);
      }
      if ( this.fmt != null ) {
        item_data = sprintf(this.fmt,item_data);
      }
      if ( ''+item_data == 'undefined' ) {
        item_data = '&lt;undefined&gt;';
      }
      //this.el.dom.innerHTML = item_data;
      this.update(item_data);
      i = item_data = null;
      return this;
    };

    /// Data rendering callback
    /**
     *  \param      data    Display data
     *  \return On success reference to self, null otherwise
     */
    el.callback = function(func)  {
      this.set = func;
      return this;
    };

    el.listeners = [];
    return el;
  };


  /// Basic FSM element connected to the message pump.
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   */
  lbExt.FSMelement = function(item, is_child, opts, panel_opts)  {
    var name     = item+'.FSM.state';
    var empty    = { html:'', width: 1 };
    var fontSize = (opts.style && opts.style.fontSize) ? opts.style.fontSize : 15;
    var label    = opts.label ? { html: opts.label.text ? opts.label.text : item, flex: 3} : empty;
    var state    = opts.state ? { html: 'Unknown', flex: 4 } : empty;
    var lock     = opts.lock  ? { html: '' } : empty;
    var style    = opts.style ? opts.style : null;
    var options  = {
    width:         fontSize*15,
    minWidth:      fontSize*15,
    minHeight:     fontSize+10,
    bodyCls:       'Text-Middle Text-Bold',
    layout:      { type: 'hbox', align: 'center'},
    defaults:    { border:      true, 
		   minHeight:   fontSize+4,
		   height:      '100%',
		   style:       style,
		   bodyCls:     'FSMBase Text-Middle Text-Bold' },
    items:       [ label, state, lock ]
    };
    options.defaults.style.fontSize = fontSize;
    lbExt.setOptions(options,opts);
    lbExt.setOptions(options,panel_opts);

    var el       = lbExt.create('Ext.panel.Panel',options);
    this.element(el);
    el.opts     = opts;
    el.format   = null;
    el.fontSize = fontSize;
    el.label    = el.items.get(0);
    el.state    = el.items.get(1);
    el.lock     = el.items.get(2);
    if ( el.opts.style && el.opts.style.fontSize )  {
      el.label.style.fontSize = el.fontSize;
      el.state.style.fontSize = el.fontSize;
      el.lock.style.fontSize  = el.fontSize;
    }
    el.state.addBodyCls('FSMState');
    opts.label ? el.label.addBodyCls('FSMLabel') : el.label.hide();
    opts.state ? el.state.addBodyCls(el.state.__bodyCls='FwDEAD') : el.state.hide();
    opts.lock  ? el.lock.addBodyCls('FwLock') : el.lock.hide();
    el.name       = item+'.FSM.state';
    el.sysname    = item;
    el.childState = 'FwDEAD';
    el.data       = null;
    el._is_child  = is_child;
    lbExt.logger.debug('FSMItem.init:'+item+' -> '+el.name);

    /** Standard callback for data provider for updates
     *
     *  \param data   Data value as string
     *  \return On success reference to self, null otherwise
     */
    el.set = function(data)  {
      if ( data != null ) {
        if ( data.length == 2 )   {
          if ( this.data && this.data[1] == data[1] ) return this;
          this.data = data;
          return this.display(data[1]);
        }
      }
      lbExt.logger.error('FSMItem['+this.name+'].set: Invalid data:'+data);
      return null;
    };

    /** Display rendering function for received data
     *
     *  \param      data    Display data
     *  \return On success reference to self, null otherwise
     */
    el.display = function(data)  {
      var v  = data.split('/',10);
      var st = this.state;
      // State name:  v[0]
      // Meta state:  v[1]  (DEAD,...)
      // In/Excluded & Style: v[2]
      //

      if ( !this._is_child )   {
        this.setState(this.childState);
        st.__bodyCls = 'FwDEAD';
      }
      else   {
        this.setState(v[3]);
        st.removeBodyCls(st.__bodyCls);
        st.addBodyCls(st.__bodyCls=v[2]);
        if ( this.opts.state && this.conversion != null )   {
          this.conversion.target = this;
          this.conversion( v[0],v[1],v[2] );
        }
      }
      if ( this.opts.label )   {
        this.label.innerHTML = v[0];
      }
      if ( this.format != null )
        st.update(sprintf( this.format,v[1] ));
      else
        st.update(v[1]);
      lbExt.logger.debug('FSMItem['+this.name+'].display: '+data+' '+this.childState+' update Done.');
      st = v = null;
      return this;
    };


    el.setChildState = function(name) {
      if ( !(name == 'INCLUDED' || name == 'ENABLED') ) {
        this._is_child = false;
        this.childState = name;
      }
      else {
        this._is_child = true;
        this.childState = name;
      }
      this.setState(name);
    };

    el.setState = function(name) {
      if ( this.opts.lock )  {
	var ext = this.lock.getHeight()-4; // top and bottom padding
        this.lock.update('<IMG SRC="'+_fileBase+'/Images/Modes/'+name+'.gif" ALT="'+name+
                         '" HEIGHT="'+ext+'" WIDTH="'+ext+'"></IMG>');
	ext = null;
      }
      return this;
    };

    /** Set callback function if the detector item is clicked
     *
     *  \param      caller callback handler
     *  \param      call callback function
     *
     *  \return On success reference to self, null otherwise
     */
    el.setCallback = function(caller, call)   {
      this.label.onclick = call;
      this.label.item    = this;
      this.label.handler = caller;
      return this;
    };
    el.address(name);
    //alert('Set address to: lbWeb.'+name+'\n'+el.addr);
    return el;
  };

  /// Subscribe all comet items of a panel
  lbExt.subscribe = function(panel, provider)   {
    var i, o;
    /// Subscribe all required data item to receive data from data provider object
    for(i=0; i<panel.items.getCount(); ++i)  {
      o = panel.items.get(i);
      if ( o.subscribe && o.addr ) o.subscribe(provider);
    }
    i = o = null;
  };

  /// Subscribe all comet items of a panel
  lbExt.unsubscribe = function(panel, provider)   {
    var i, o;
    /// Subscribe all required data item to receive data from data provider object
    for(i=0; i<panel.items.getCount(); ++i)  {
      o = panel.items.get(i);
      if ( o.subscribe && o.addr ) o.unsubscribe(provider);
    }
    i = o = null;
  };


  /// Create the main application frame for a lbcomet application
  /*
   *  The application then holds references to the viewport, the 
   *  data provider object and the error logger.
   *
   *  \author  M.Frank
   *  \version 2.0
   */
  lbExt.createApp = function(opts)   {
    var application = {};
    application.title = '';
    application.messages = null;
    application.fontSize = null;
    lbExt.setOptions(application, opts);
    if ( opts.title )  {
      // Update window title
      lbExt.setWindowTitle(application.title);
    }
    application.logger   = new lbExt.logger({active: application.messages, 
	  level: LOG_INFO,
	  style: { height: 300 }});
    application.provider = new DataProvider(application.logger);

    /// Create the applications viewport
    application.create = function(options)   {
      options.cls     = 'Bg-lhcblight';
      options.bodyCls = 'Bg-lhcblight';
      if ( !options.hasOwnProperty('autoScroll') )
	options.autoScroll = true;
      if ( !options.hasOwnProperty('autoSize') )
	options.autoSize = true;
      if ( !options.hasOwnProperty('scrollable') )
	options.scrollable = true;
      if ( !options.hasOwnProperty('renderTo') )
	options.renderTo = Ext.getBody();
      if ( !options.hasOwnProperty('layout')   ) 
	options.layout =  { type: 'table', 
			    columns: 1,
			    tableAttrs: { style: { width: '100%' }}};
      if ( !options.hasOwnProperty('defaults')   ) 
	options.defaults = { bodyStyle: 'padding: 0px',
			     bodyCls:   'Bg-lhcblight', 
			     cls:       'Bg-lhcblight' };
      options.items.push( {items: [lbExt.LHCb_copyright({}), this.logger] });
      this.view = lbExt.create('Ext.Viewport', options);
      /// Attach these to the view in case we need them later....
      this.view.dataprovider = this.provider;
      this.view.logger = this.logger;
      return this.view;
    };

    /// Start the application. Starts the data pump, initializes tool-tips and zooming
    application.start = function()  {
      /// Initialize tool tips
      Ext.QuickTips.init();
      /// Apply zoom depending on the screen size and the browser:
      lbExt.initZoom({fontSize: this.fontSize});
      /// Start the comet engine
      this.provider.start();
      return this;
    };

    /// One-off function to create and start the application
    application.run = function(opts)   {
      this.create(opts);
      return this.start();
    };
    return application;
  };

  /// Helper to handle 2D drawings on lbExt canvas panels
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   */
  lbExt.canvasGrid = function(width, height, origin)   {
    var grid = { width: width, height: height, origin: origin };
    grid.images = document.createElement('div');
    grid.images.style.display = 'none';
    
    /// Allow to adopt the grid by a canvas
    grid.adopt = function(canvas)  {
      this.canvas  = canvas;
      this.scale   = {x: this.canvas.width/this.width, y: this.canvas.height/this.height};
      this.context = this.canvas.getContext('2d');
    };

    /// Adjust scale according to real canvas size
    grid.rescale = function(point)  {
      return {x: this.scale.x*point.x, y: this.scale.y*point.y};
    };

    /// Convert global coordinates to local coordinates
    grid.local = function(point)  {
      var pt = this.rescale({x: this.origin.x + point.x, y: this.origin.y - point.y});
      //console.info('Local:  x:'+point.x+' y:'+point.y);
      //console.info('Global: x:'+pt.x+   ' y:'+pt.y);
      return pt;
    };

    /// Set options to the drawing context
    grid.setOptions = function(opts)  {
      lbExt.setOptions(this.context,opts);
      return this;
    };

    /// Draw line
    grid.line = function(x_start, y_start, x_end, y_end, opts)  {
      var start = this.local({x: x_start, y: y_start});
      var end   = this.local({x: x_end,   y: y_end});
      lbExt.setOptions(this.context, opts);
      this.context.beginPath();
      this.context.moveTo(start.x, start.y);
      this.context.lineTo(end.x,   end.y);
      this.context.stroke();      
      start = end = null;
      return this;
    };

    /// Draw path as a sequence of line segments
    grid.path = function(points, opts)  {
      var pt = this.local({x: points[0][0], y: points[0][1]});
      lbExt.setOptions(this.context,opts);
      this.context.beginPath();
      this.context.moveTo(pt.x, pt.y);
      for(var i=1; i<points.length; ++i)   {
	pt = this.local({x: points[i][0], y: points[i][1]});
	this.context.lineTo(pt.x, pt.y);
      }
      this.context.closePath();
      this.context.stroke();
      pt = null;
      return this;
    };

    /// Draw a rectangle with a given area to the canvas
    grid.rectangle = function(x, y, width, height, opts)  {
      var p = this.local({x: x, y: y});
      var a = this.rescale({x: width, y: -height});
      lbExt.setOptions(this.context, opts);
      this.context.fillRect(p.x, p.y, a.x, a.y);
      p = a = null;
      return this;
    };

    /// Draw an ellipse with a given area to the canvas
    grid.ellipse = function(x, y, radius_X, radius_Y, rotation, startPhi, endPhi, opts)  {
      var p = this.local({x: x, y: y});
      var r = this.rescale({x: radius_X, y: radius_Y});
      lbExt.setOptions(this.context, opts);
      //console.info('Ellipse:',p.x, p.y, r.x, r.y, rotation, startPhi, endPhi);
      this.context.beginPath();
      this.context.ellipse(p.x, p.y, r.x, r.y, rotation, startPhi, endPhi);
      if ( opts.hasOwnProperty('strokeStyle') || opts.hasOwnProperty('lineWidth') )
	this.context.stroke();
      if ( opts.hasOwnProperty('fillStyle') )
	this.context.fill();
      p = r = null;
      return this;
    };

    /// Draw text into the canvas
    grid.text = function(x, y, text, opts)   {
      var pt   = this.local({x: x, y: y});
      lbExt.setOptions(this.context, opts);
      this.context.fillText(text, pt.x, pt.y);
      return this;
    };

    /// Draw image into the canvas
    grid.image = function(id,src,width,height,opts)   {
      var img = document.createElement('img');
      this.images.append(img);
      img.id      = this.canvas.id+'_'+id;
      img.src     = src;
      img.width   = width;
      img.height  = height;
      img.canvas  = this;
      img.area    = this.rescale({x: width, y: height});
      img.draw = function(x,y)  {
	var p = this.canvas.local({x: x-this.width/2, y: y+this.height/2});
	this.canvas.context.drawImage(this, p.x, p.y, this.area.x, this.area.y);
	p = null;
	return this;
      };
      console.info('Image:',id,src,width,height,opts);
      return img;
    };

    /// Draw the coodinate system to the canvas
    grid.drawSystem = function(opts)  {
      lbExt.setOptions(this.context,opts);
      this.line(-this.width/2,  0, this.width/2,  0);
      this.line(0, -this.height/2, 0, this.height/2);
      return this;
    };

    /// Clear the drawing area
    grid.clear = function()   {
      this.context.clearRect(0,0,this.canvas.width, this.canvas.height);
      //this.context.rectangle(0,0,this.canvas.width, this.canvas.height,{fillStyle: '#DAF0FB'});
    };
    return grid;
  };

  /// Graphical display canvas for general purpose
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   */
  lbExt.canvasPanel = function(opts)   {
    var width   = opts.hasOwnProperty('width')   ? opts.width  : '99.8%';
    var id      = opts.hasOwnProperty('id')      ? opts.id     : 'Graphics_'+lbExt.newId();
    var options = lbExt.panelDefaults({
	  id:               id,
	  width:            width,
	  layout:         { type: 'fit' },
	  minHeight:        210,
	  defaults: {  border:  true,
	    xtype:   'panel',
	    width:   '100%',
	    height:  '100%',
	    bodyCls: 'Bg-lhcblight',
	    cls:     'Text-Center Text-Middle Bg-lhcblight'
	    },
	  items: [{html: '<canvas id="'+id+'-canvas" class="Bg-lhcblight"/>'}]
	  }, opts);
    var panel  = this.create('Ext.panel.Panel',options);
    panel.canvas = document.getElementById(id+'-canvas');
    console.info('Canvas: Width: '+panel.canvas.width+' Height: '+panel.canvas.height);
    console.info('Panel:  Width: '+ panel.getWidth()+  ' Height: '+panel.getHeight());

    // We need some margins to fully contain the canvas in the panel
    panel.canvas.width   = panel.getWidth()-50;
    panel.canvas.height  = panel.getHeight()-50;

    // Install tooltips and callbacks if required
    if ( options.id )  {
      var elt = document.getElementById(id+'-body');
      if ( options.onclick )  {
        elt.onmouseout  = lbExt.setCursorDefault;
        elt.onmouseover = lbExt.setCursorLink;
        elt.onclick     = options.onclick;
      }
      if ( options.tooltip )  {
        elt.tooltip = new Ext.ToolTip({ target: elt.id, trackMouse: true, html: options.tooltip});
      }
    }
    /// Adopt the grid canvas
    panel.setGrid = function(grid)  {
      this.grid = grid;
      this.grid.adopt(this.canvas);
      return this;
    };
    return panel;
  };

  /// Copyright panel to be used as generic footer
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   */
  lbExt.LHCb_copyright = function(opts) {
    var options = {
    renderTo:    Ext.getBody(),
    frame:       true,
    bodyStyle: { textAlign: 'right', 
                 fontSize:  12,
                 paddingRight: 100,
                 border: 0},
    html:        'Comments and suggestions to M.Frank CERN/LHCb' //+'  ExtJs version:'+Ext.versions.extjs.version
    };
    lbExt.setOptions(options,opts);
    var panel = this.create('Ext.panel.Panel',options);
    return panel;
  };
 
  lbExt.showSysInfo = function()  {
    var items = [];
    items.push({ html: 'Navigator',          cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: 'type:',              cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: navigator.appName });

    items.push({ html: '&rarr;',             cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: 'code:',              cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: navigator.appCodeName });

    items.push({ html: '&rarr;',             cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: 'version:',           cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: navigator.appVersion });

    items.push({ html: '&rarr;',             cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: 'platform:',          cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: navigator.platform });

    items.push({ html: '&rarr;',             cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: 'language:',          cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: navigator.language });

    items.push({ html: '&rarr;',             cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: 'browser engine:',    cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: navigator.product });

    items.push({ html: '&rarr;',             cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: 'cookies:',           cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: navigator.cookieEnabled ? 'enabled' : 'disabled'});

    items.push({ html: '&rarr;',             cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: 'Java:',              cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: navigator.javaEnabled() ? 'enabled' : 'disabled'});

    items.push({ html: '&rarr;',             cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: 'Online mode:',       cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: navigator.onLine ? 'ONLINE' : 'OFFLINE'});
    if ( Ext.versions.Firefox )  {
      items.push({ html: 'Firefox',          cls: 'MonitorDataHeader Text-Bold' });
      items.push({ html: 'version:',         cls: 'MonitorDataHeader Text-Bold' });
      items.push({ html:  Ext.versions.Firefox.version});
    }
    items.push({ html: 'Screen',             cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: 'size:',              cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: screen.availWidth+' x '+screen.availHeight });

    items.push({ html: '&rarr;',             cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: 'color-depth:',       cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: ''+screen.colorDepth });

    items.push({ html: '&rarr;',             cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: 'pixel-depth:',       cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: ''+screen.pixelDepth });

    items.push({ html: 'ExtJs',              cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: 'version:',           cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: Ext.versions.ext.version });

    items.push({ html: '&rarr;',             cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: 'body height:',       cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: Ext.getBody().getHeight() });

    items.push({ html: '&rarr;',             cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: 'body width:',        cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: Ext.getBody().getWidth() });

    items.push({ html: 'Comet',              cls: 'MonitorDataHeader Text-Bold' });
    items.push({ html: 'enabled:',           cls: 'MonitorDataHeader Text-Bold' });
    var dp = window['_dataProvider'];
    items.push({ html: dp == null ? 'Not connected' : 'Active' });
    if ( dp )  {
      items.push({ html: '&rarr;',           cls: 'MonitorDataHeader Text-Bold' });
      items.push({ html: 'transport:',       cls: 'MonitorDataHeader Text-Bold' });
      items.push({ html: _transport_in_use});

      items.push({ html: '&rarr;',           cls: 'MonitorDataHeader Text-Bold' });
      items.push({ html: 'host:',            cls: 'MonitorDataHeader Text-Bold' });
      items.push({ html: _dataProvider.host+':'+_dataProvider.port});

      items.push({ html: '&rarr;',           cls: 'MonitorDataHeader Text-Bold' });
      items.push({ html: 'client-id:',       cls: 'MonitorDataHeader Text-Bold' });
      items.push({ html: _dataProvider.clientid});

      items.push({ html: '&rarr;',           cls: 'MonitorDataHeader Text-Bold' });
      items.push({ html: 'last update:',     cls: 'MonitorDataHeader Text-Bold' });
      items.push({ html: _dataProvider.lastMessageTime.toLocaleString()});
      
      items.push({ html: '&rarr;',           cls: 'MonitorDataHeader Text-Bold' });
      items.push({ html: 'check interval:',  cls: 'MonitorDataHeader Text-Bold' });
      items.push({ html: _dataProvider.checkInterval});

      items.push({ html: '&rarr;',           cls: 'MonitorDataHeader Text-Bold' });
      items.push({ html: 'reload interval:', cls: 'MonitorDataHeader Text-Bold' });
      items.push({ html: sprintf('%.0f h %.0f min %.0f sec',
                                 Math.floor(_dataProvider.reloadInterval/3600000),
                                 Math.floor((_dataProvider.reloadInterval%3600000)/60),
                                 (_dataProvider.reloadInterval%360000)%60)
            });
    }

    var dialog = Ext.create("Ext.Window", {
      title : 'System Information',
          closable:    true,
          modal:       true,
          layout:    { type: 'table', columns: 3, tableAttrs: { style: { padding: 0 }}},
          defaults:  { border:  false,
            xtype:              'box',
            height:             '100%',
            style: { textAlign: 'left',
              fontSize:  'big',
              fontWeight:'bold',
              padding:   '4 8 4 8'
              }        },
          items:    items,
          buttons:  [{text: 'Close', listeners: {click: function() { dialog.destroy(); dialog=null; }}}]
          });
    dialog.show();
    items = null;
  };


  /// Build the generic page header for a display page
  /**   
   * \return Reference to ExtJs panel containing all data items of this widget
   *
   * \author  M.Frank
   * \version 1.0
   */
  lbExt.pageHeader = function(sys, opts)    {
    var id    = opts.id ? opts.id : sys;
    var items = [];
    if ( opts.left )  {
      items.push({   id:      id + '_header__left_title',
            html:    opts.left && opts.left.title ? opts.left.title : '',
            style: { paddingLeft: 50, fontSize: 38 }});
    }
    if ( opts.time )  {
      items.push({   id:     id + '_current_time', 
            html:    opts.time ? 'No known data update' : '',
            style: { fontSize: 15}});
    }
    if ( opts.center )  {
      items.push({   id:     id + '_header__center_title',
            html:    opts.center && opts.center.title ? opts.center.title : '',
            style: { paddingLeft: 50, fontSize: 38 }});
    }
    if ( opts.zoom )  {
      items.push({   id:     id + '_increase_font',
            html:   '<B><font size="+3">A<sup>+</sup></font></B>',
            xtype:  'button'});
      items.push({   id:     id + '_decrease_font',
            html:   '<B><font size="+3">A<sup>-</sup></font></B>', 
            xtype:  'button' });
    }
    if ( opts.sysinfo )  {
      items.push({   id:     id + '_system_info',
            html:   opts.sysinfo ? '<B><font size="+3">i<sup>&nbsp;</sup></font></B>' : '', 
            xtype:  opts.sysinfo ? 'button' : 'panel'});
    }
    if ( opts.right )  {
      items.push({   id:     id + '_header__right_title',
            html:   opts.right && opts.right.title ? opts.right.title : '',
            style: { paddingLeft: 50, fontSize: 38 }});
    }

    var panel = lbExt.create('Ext.panel.Panel',{
      id:                id + '_header',
          renderTo:      Ext.getBody(),
          bodyStyle:   { padding: 8 },
	  bodyCls:       'Bg-lhcblight Text-Bold Text-Left',
          frame:         false,
          autoHeight:    true,
          autoWidth:     true,
          collapsible:   false,
          layout:      { type:  'table', columns: 6, tableAttrs: { style: { width: '100%' }}},
          defaults:    { xtype: 'box',
	    cls:         'Text-lhcbdark Text-left',
	    style:     { padding: 3, fontSize: 20}},
          items:         items
          });
      
    if ( opts.zoom )  {
      var el = document.getElementById(id + '_increase_font');
      el.tooltip = new Ext.ToolTip({ target: el.id, trackMouse: true,
            html: 'Click to increase the font size of this page'});
      el.onclick = function() { lbExt.zoomIn(); };
      
      el = document.getElementById(id + '_decrease_font');
      el.tooltip = new Ext.ToolTip({ target: el.id, trackMouse: true,
            html: 'Click to decrease the font size of this page'});        
      el.onclick = function() { lbExt.zoomOut(); };
    }

    if ( opts.sysinfo )  {
      el = document.getElementById(id + '_system_info');
      el.tooltip = new Ext.ToolTip({ target: el.id, trackMouse: true,
            html: 'Show system information'});        
      el.onclick = function() { lbExt.showSysInfo(); };
    }
    if ( opts.left )  {
      var left = document.getElementById(id + '_header__left_title');
      if ( opts.left.onclick )   {
        left.onmouseover = lbExt.setCursorLink;
        left.onmouseout  = lbExt.setCursorDefault;
        left.onclick = function() { document.location = opts.left.onclick; };
      }
      if ( opts.left.tooltip )   {
        left.tooltip = new Ext.ToolTip({target: left.id, trackMouse: true, html: opts.left.tooltip});
      }
      left = null;
    }

    if ( opts.center )  {
      var center = document.getElementById(id + '_header__center_title');
      if ( opts.center.onclick )   {
        center.onclick = function() { document.location = opts.center.onclick; };
      }
      if ( opts.center.tooltip )   {
        center.tooltip = new Ext.ToolTip({target: center.id, trackMouse: true, html: opts.center.tooltip});
      }
      center = null;
    }
    if ( opts.right )  {
      var right = document.getElementById(id + '_header__right_title');
      if ( opts.right.onclick )   {
        right.onclick = function() { document.location = opts.right.onclick; };
      }
      if ( opts.right.tooltip )   {
        right.tooltip = new Ext.ToolTip({target: right.id, trackMouse: true, html: opts.right.tooltip});
      }
      right = null;
    }
    if ( opts.time )  {
      var time = document.getElementById(id + '_current_time');
      time.tooltip = new Ext.ToolTip({target: time.id, trackMouse: true,
            html: 'See here when data were updated last<br>'+
            'If this timestamp is stuck for minutes, you have to reload the page'});
      // Rearm timer for update if time updates from the data provider is requested
      time.handler = function() {
        var e = document.getElementById(time.id);
        e.innerHTML = 'Last data update: '+_dataProvider.lastMessageTime.toLocaleString();
        e = null;
      };
      setInterval(time.handler,2000);
    }
    return panel;
  };


  /** Build the generic header bar showing the LHC state and the LHCb run state
   *   
   * \return Reference to ExtJs panel containing all data items of this widget
   *
   * \author  M.Frank
   * \version 1.0
   */
  lbExt.detectorHeader = function(sys,opts) {
    var run = lbExt.FSMelement(sys, true, {
      label:  false, state: true, lock: true,
          style: { fontSize: 32, height: 40, width: '100%', textAlign: 'center'}});
    run.flex    = 4;
    run.setMinWidth(300);
    var options = {
    renderTo:         Ext.getBody(),
    layout:          'fit',
    title:            null,
    xtype:           'box',
    width:           '100%',
    autoHeight:       true,
    frame:            false,
    header:           false,
    
    bodyCls:         'Bg-lhcbdark Text-lhcblight NoBorder',
    layout:         { type: 'hbox', align: 'stretch', pack: 'center'  },
    defaults: {
      xtype:    'box',
      padding:  '10 15 10 15',
      style:   { textAlign: 'center', fontSize: 34 }
    },
    items: [
      { html: 'Fill'},     
      { html: 'undefined' },
      { html: 'undefined' },
      { html: 'Run'},
      { html: 'undefined' },
      { html: 'LHCb:'}, run ]
    };
    lbExt.setOptions(options,opts);
    var i, o, panel = this.create('Ext.panel.Panel',options);
    this.element(panel.items.get(1)).address('LHCCOM/LHC.LHC.RunControl.FillNumber');
    this.element(panel.items.get(2)).address('LHCCOM/LHC.LHC.RunControl.BeamMode').format('[%s]');
    //lbExt.element(panel.items.get(3)).address('LHCCOM/LHC.LHC.RunControl.MachineMode').format(' (%s) ');
    this.element(panel.items.get(4)).address(''+sys+'_RunInfo.general.runNumber');

    lbExt.subscribe(panel, opts.provider);
    /// Install tooltips and callbacks if required
    if ( options.id )  {
      var elt = document.getElementById(options.id);
      if ( options.onclick )  {
        elt.onclick = options.onclick;
      }
      if ( options.tooltip )  {
        elt.tooltip = new Ext.ToolTip({ target: elt.id, trackMouse: true, html: options.tooltip});
      }
    }
    i = o = options = null;
    return panel;
  };
 
  /// Simple convenience function to save us some typing.
  lbExt.standardHeaderItems = function(opts)  {
    page_id = opts && opts.id ? opts.id : 'dummy_page_id';
    var items = [lbExt.pageHeader('LHCb',{ id: page_id+'_page_header',
            time:     true,
            sysinfo:  true,
            zoom:     true,
            left:     opts.left,
            right:    opts.right,
            center:   opts.center
            }),
      lbExt.detectorHeader('LHCb', {id: page_id+'_generic_header', 
            tooltip:   'Current LHC fill and LHCb run status<br>&nbsp;<br>Click to show the run-control status page.',
            onclick:   function() { document.location = lhcb.constants.urls.lhcb.lhcb_run_status.src;},
            provider:  opts.provider })
      ];
    page_id = null;
    return items;
  };


  /// Build LHCb "Plan of the day" comments panel
  /**
   *   +-------- LHCb Plan of the day  ----------
   *   |Whatever we want to do.............      
   *   |
   *   +-----------------------------------------
   *
   * \return Reference to ExtJs Panel object containing all data items of this widget
   *
   * \author  M.Frank
   * \version 1.0
   */
  lbExt.planOfTheDay = function(opts)   {
    var height  = opts.hasOwnProperty('height') ? opts.height : '100%';
    var id      = opts.hasOwnProperty('id')     ? opts.id     : 'lhcb-plan-of-the-day';
    var options = lbExt.panelDefaults({id: id,
	  title:           'LHCb Plan of the day',
	  height:           height,
	  autoHeight:       opts.hasOwnProperty('height') ? false : true,
	  xtype:            'box',
	  layout:   {  type: 'table', columns: 1, tableAttrs: { style: { padding: 3}}},
	  defaults: {  border:                   false,
	    xtype:                    'box',
	    style: {  textAlign:      'top',
	      width:          '100%',
	      height:         height,
	      paddingTop:     0,
	      paddingBottom:  0
	      }	},
	  items: [{ html: 'undefined', id: id+'-content', cls: 'MonitorDataItem Text-Bold'  }]
	  }, opts);
    var panel = lbExt.create('Ext.panel.Panel',options);
    var elt   = this.element(panel.items.get(0));
    elt.address('LHCCOM/LHC.LHCb.Internal.Plan')
    .convert(function(data) {return data.replace(/\n/g,'<BR>');});
    if ( opts.provider ) elt.subscribe(opts.provider);

    // Install tooltips and callbacks if required
    if ( options.id )  {
      elt = document.getElementById(id+'-body');
      if ( options.onclick )  {
        elt.onmouseout  = lbExt.setCursorDefault;
        elt.onmouseover = lbExt.setCursorLink;
        elt.onclick = options.onclick;
      }
      if ( options.tooltip )  {
        elt.tooltip = new Ext.ToolTip({ target: elt.id, trackMouse: true, html: options.tooltip});
      }
    }
    elt = null;
    return panel;
  };

  /// Build LHC operator comments panel
  /**
   *   +-------- LHC operator comments ----------
   *   |LHC in limbo, awaiting decision ...      
   *   |
   *   +-----------------------------------------
   *
   * \return Reference to ExtJs Panel object containing all data items of this widget
   *
   * \author  M.Frank
   * \version 1.0
   */
  lbExt.LHCComment = function(opts)  {
    var height  = opts.hasOwnProperty('height') ? opts.height : '100%';
    var id      = opts.hasOwnProperty('id')     ? opts.id     : 'LHC_Operator_comments';
    var options = lbExt.panelDefaults({id: id,
	  title:           'LHC Operator comments',
	  height:           height,
	  autoHeight:       opts.hasOwnProperty('height') ? false : true,
	  xtype:            'box',
	  layout:   {  type: 'table', columns: 1, tableAttrs: { style: { padding: 3}}},
	  defaults: {  border:                   false,
	    xtype:                    'box',
	    style: {  textAlign:      'top',
	      width:          '100%',
	      height:         height,
	      paddingTop:     0,
	      paddingBottom:  0
	      }
	},
	  items: [{ html: 'undefined', id: id+'-content', cls: 'MonitorDataItem Text-Bold'  }]
	  }, opts);
    var panel = lbExt.create('Ext.panel.Panel',options);
    var elt   = this.element(panel.items.get(0));
    elt.address('LHCCOM/LHC.LHC.RunControl.Page1Comment')
    .convert(function(data) {
	while (data.length>0 && data[0]=='\n')
          data = data.substr(1);
	return data.replace(/\n/g,'<BR>');}
      );
    if ( opts.provider ) elt.subscribe(opts.provider);

    // Install tooltips and callbacks if required
    if ( options.id )  {
      elt = document.getElementById(id+'-body');
      if ( options.onclick )  {
        elt.onmouseout  = lbExt.setCursorDefault;
        elt.onmouseover = lbExt.setCursorLink;
        elt.onclick = options.onclick;
      }
      if ( options.tooltip )  {
        elt.tooltip = new Ext.ToolTip({ target: elt.id, trackMouse: true, html: options.tooltip});
      }
    }
    elt = null;
    return panel;
  };


  /** Build table with LHC summary information
   *   
   * \return Reference to HTML table containing all data items of this widget
   *
   * \author  M.Frank
   * \version 1.0
   */
  lbExt.LHCSummary = function(opts) {
    var width   = opts.hasOwnProperty('width')   ? opts.width  : '100%';
    var id      = opts.hasOwnProperty('id') ? opts.id : 'LHC_status_summary';
    var options = lbExt.panelDefaults({
      id:              id,
	  title:           'LHC status summary',
	  layout:         { type: 'table', columns: 4, tableAttrs:   { style: { padding: 0 }}},
	  minHeight:        210,
	  defaults: {  border:             true,
	    xtype:              'box',
	    style: { textAlign: 'left',
	      fontSize:  'big',
	      fontWeight:'bold',
	      padding:   '4 8 4 8'
	      }
	},
	  items: [{ html: 'Mode',                 cls: 'MonitorDataHeader Text-Bold' }, // 0
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' },
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold', colspan: 2 },
		  { html: 'Energy',               cls: 'MonitorDataHeader Text-Bold' }, // 3
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' },
		  { html: 'Avg.Luminosity',       cls: 'MonitorDataHeader Text-Bold' }, // 5
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' },              // 6
		  { html: 'Intensity [e]',        cls: 'MonitorDataHeader Text-Bold' }, // 7
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' },
		  { html: '&larr;1 Beam 2&rarr;', cls: 'MonitorDataItem   Text-Bold' },
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' },
		  { html: 'Lifetime [h]',         cls: 'MonitorDataHeader Text-Bold' }, // 11
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' },
		  { html: '&larr;1 Beam 2&rarr;', cls: 'MonitorDataItem   Text-Bold' },
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' },
		  { html: 'Handshakes',           cls: 'MonitorDataItem   Text-Bold'   },
		  { html: 'Dump',                 cls: 'MonitorDataHeader Text-Bold'}, // 16
		  { html: 'Adjust',               cls: 'MonitorDataHeader Text-Bold'},
		  { html: 'Injection',            cls: 'MonitorDataHeader Text-Bold'}, // 18
		  { html: '',                     cls: 'MonitorDataItem   Text-Bold'          }, 
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' },   // 20
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' },
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' }],
	  buttons: []
	  }, opts);
    var panel  = this.create('Ext.panel.Panel',options);
    this.element(panel.items.get(1)).address('LHCCOM/LHC.LHC.RunControl.BeamMode');
    this.element(panel.items.get(2)).address('LHCCOM/LHC.LHC.RunControl.MachineMode');

    this.element(panel.items.get(4)).address('LHCCOM/LHC.LHC.Beam.Energy').format('%7.0f GeV');
    this.element(panel.items.get(6)).address('LHCCOM/LHC.LHCb.Beam.Luminosity.LuminosityAverage').format('%9.2e');
    this.element(panel.items.get(8)).address('LHCCOM/LHC.LHC.Beam.IntensityPerBunch.Beam1.averageBeamIntensity').format('%9.2e');
    this.element(panel.items.get(10)).address('LHCCOM/LHC.LHC.Beam.IntensityPerBunch.Beam2.averageBeamIntensity').format('%9.2e');
    this.element(panel.items.get(12)).address('LHCCOM/LHC.LHC.Beam.IntensityPerBunch.Beam1.bestLifetime').format('%9.2e');
    this.element(panel.items.get(14)).address('LHCCOM/LHC.LHC.Beam.IntensityPerBunch.Beam2.bestLifetime').format('%9.2e');
    this.element(panel.items.get(20)).address('LHCCOM/LHC.LHC.Handshake.LHC_BEAMDUMP');
    this.element(panel.items.get(21)).address('LHCCOM/LHC.LHC.Handshake.LHC_ADJUST');
    this.element(panel.items.get(22)).address('LHCCOM/LHC.LHC.Handshake.LHC_INJECTION');

    // Install tooltips and callbacks if required
    if ( options.id )  {
      var elt = document.getElementById(id+'-body');
      if ( options.onclick )  {
        elt.onmouseout  = lbExt.setCursorDefault;
        elt.onmouseover = lbExt.setCursorLink;
        elt.onclick     = options.onclick;
      }
      if ( options.tooltip )  {
        elt.tooltip = new Ext.ToolTip({ target: elt.id, trackMouse: true, html: options.tooltip});
      }
    }
    /// Subscribe all required data item to receive data from the provider object
    lbExt.subscribe(panel, opts.provider);
    return panel;
  };


  /** Build table with LHC summary information
   *   
   * \return Reference to HTML table containing all data items of this widget
   *
   * \author  M.Frank
   * \version 1.0
   */
  lbExt.page1Summary = function(sys, opts) {
    var items = [
    { html: 'Run type',              cls: 'MonitorDataHeader Text-Bold' },  // 0
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold' },
    { html: 'Run started at',        cls: 'MonitorDataHeader Text-Bold' },
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold', colspan: 3 },

    { html: 'BCM&nbsp;Bkg&nbsp;[&permil;]',cls: 'MonitorDataHeader Text-Bold' },  // 4
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold' },
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold' },
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold' },
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold' },
    { html: '',                      cls: 'MonitorDataItem   Text-Bold' },

    { html: 'Beam&nbsp;Permit',      cls: 'MonitorDataHeader Text-Bold' },  // 10
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold' },
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold' },
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold' },
    { html: 'Velo pos.',             cls: 'MonitorDataHeader Text-Bold' },
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold' },

    { html: 'Magnet',                cls: 'MonitorDataHeader Text-Bold' },  // 16
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold'},
    { html: '',                      cls: 'MonitorDataItem   Text-Bold', colspan: 2  }, 
    { html: 'LV&nbsp;&amp;&nbsp;HV ',cls: 'MonitorDataHeader Text-Bold' },
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold' },
    { html: 'Inst.lumi' ,            cls: 'MonitorDataHeader Text-Bold' }, // 21
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold' },
    { html: 'L0 events',             cls: 'MonitorDataHeader Text-Bold' },
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold' },
    { html: '',                      cls: 'MonitorDataItem   Text-Bold' },
    { html: '',                      cls: 'MonitorDataItem   Text-Bold' },

    { html: 'Rates',                 cls: 'MonitorDataHeader Text-Bold' }, // 27
    { html: 'Interaction',           cls: 'MonitorDataHeader Text-Bold' },
    { html: 'L0',                    cls: 'MonitorDataHeader Text-Bold' },
    { html: 'HLT 1',                 cls: 'MonitorDataHeader Text-Bold' },
    { html: 'HLT 2',                 cls: 'MonitorDataHeader Text-Bold' },
    { html: 'Dead&nbsp;Time',        cls: 'MonitorDataHeader Text-Bold' },

    { html: 'Now',                   cls: 'MonitorDataHeader Text-Bold' }, // 33
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold' },
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold' },
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold' },
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold' },
    { html: '',                      cls: 'MonitorDataItem   Text-Bold' },

    { html: 'Run Average',           cls: 'MonitorDataHeader Text-Bold' }, // 40
    { html: '--',                    cls: 'MonitorDataItem   Text-Bold' },
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold' },
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold' },
    { html: '--',                    cls: 'MonitorDataItem   Text-Bold' },
    { html: 'undefined',             cls: 'MonitorDataItem   Text-Bold' },
    { html: 'Disks ',                cls: 'MonitorDataHeader Text-Bold' }, // 46
    { html: 'total',                 cls: 'MonitorDataItem   Text-Bold' },
    { html: 'free',                  cls: 'MonitorDataItem   Text-Bold'  },
    { html: 'usage',                 cls: 'MonitorDataItem   Text-Bold' },
    { html: 'files',                 cls: 'MonitorDataItem   Text-Bold', colspan: 2 },

    { html: 'TCK Label',             cls: 'MonitorDataHeader Text-Bold', rowspan: 2  }, // 51
    { html: 'tck',                   cls: 'MonitorDataItem   Text-Bold', colspan: 5 },
    { html: '', colspan: 6 }
                 ];

    var options = lbExt.panelDefaults({
      title:            'LHCb DAQ status summary',
	  layout:       {   type: 'table', columns: 6,
	    tableAttrs: { style: { width: '100%', padding: 2 }}},
	  defaults: {  border: true,
	    xtype:             'box',
	    bodyStyle:         'padding:0',
	    style: {  textAlign:  'left',
	      fontWeight: 'bold',
	      border:     true,
	      padding:    '4 8 4 8'
	      }
	},
	  items: items
	  }, opts);

    var panel = this.create('Ext.panel.Panel',options);
    this.element(panel.items.get(1)).address(''+sys+'_RunInfo.general.runType');
    this.element(panel.items.get(3)).address(''+sys+'_RunInfo.general.runStartTime');

    this.element(panel.items.get(5)).address('BCM_DP_S0.RS2_REL').format('S0(RS2):&nbsp;%7.3f&nbsp;&nbsp;&nbsp;&nbsp;');
    this.element(panel.items.get(6)).address('BCM_DP_S0.RS32_REL').format('S0(RS32):&nbsp;%7.3f');
    this.element(panel.items.get(7)).address('BCM_DP_S1.RS2_REL').format('S1(RS2):&nbsp;%7.3f');
    this.element(panel.items.get(8)).address('BCM_DP_S1.RS32_REL').format('S1(RS32):&nbsp;%7.3f');
    this.element(panel.items.get(11)).address('BCM_Interface.BeamPermit.getStatus').format('Global:%s');
    this.element(panel.items.get(12)).address('BCM_Interface.InjPermit1.getStatus').format('Inject&nbsp;1:%s');
    this.element(panel.items.get(13)).address('BCM_Interface.InjPermit2.getStatus').format('Inject&nbsp;2:%s');
    this.element(panel.items.get(15)).address('LHCCOM/LHC.LHCb.Specific.VELO.Position').format('%s');

    this.element(panel.items.get(17)).address('lbHyst.B').format('%7.4f Tesla');
    this.element(panel.items.get(18)).address('lbHyst.Polarity')
    .convert(function(data) {
        if ( data>0 ) return 'Polarity: +&nbsp;(Down)';
        return 'Polarity: -&nbsp;(Up)';
      });
    this.element(panel.items.get(20)).address('LHCb_LHC_HV.FSM.state')
    .convert(function(data) {  return (data.split('/')[1]); });

    this.element(panel.items.get(22)).address('LHCCOM/LHC.LHCb.Internal.Luminosity.LumiInst_GP')
    .format('%7.2f &mu;b<sup>-1</sup>/s');
    this.element(panel.items.get(24)).address(''+sys+'_RunInfo.TFC.nTriggers');

    this.element(panel.items.get(34)).address('LHCCOM/LHC.LHCb.Internal.TriggerRates.TrgRate_calo_bb')
    .format('%10.0f Hz');
    this.element(panel.items.get(35)).address(''+sys+'_RunInfo.TFC.triggerRate').format('%6.0f&nbsp;Hz');
    this.element(panel.items.get(36)).address(''+sys+'_RunInfo.HLTFarm.hltRate').format('%6.0f&nbsp;Hz');
    this.element(panel.items.get(37)).address('LHCb2_RunInfo.HLTFarm.hltRate').format('%6.0f&nbsp;Hz');
    this.element(panel.items.get(38)).address(''+sys+'_RunInfo.TFC.deadTime').format('%8.2f&nbsp;%%');

    this.element(panel.items.get(41)).address(''+sys+'_RunInfo.TFC.runTriggerRate').format('%6.0f&nbsp;Hz');
    this.element(panel.items.get(42)).address(''+sys+'_RunInfo.HLTFarm.runHltRate').format('%6.0f&nbsp;Hz');
    this.element(panel.items.get(44)).address(''+sys+'_RunInfo.TFC.runDeadTime').format('%8.2f&nbsp;%%');

    panel.disk_size = this.element(panel.items.get(46))
    .address('LHCb_RunInfoSplitHLT.DiskSize')
    .convert(function(data)  {  return sprintf('%.0f&nbsp;TB',data/1024.0); });

    panel.disk_free = this.element(panel.items.get(47))
    .address('LHCb_RunInfoSplitHLT.DiskFree')
    .convert(function(data)  {  return sprintf('Free:&nbsp;%.0f&nbsp;TB',data/1024.0); });

    panel.usage = this.element(panel.items.get(48));
    panel.usage.listen(panel.disk_free,function(data)  { panel.usage.disk_free = parseFloat(data[1]); })
    .listen(panel.disk_size,function(data)  { panel.usage.disk_size = parseFloat(data[1]); })
    .convert(function(data)  {
        if ( this.disk_free && this.disk_size )
          return sprintf('Used: %.2f %%',100.0*(1.0-this.disk_free/this.disk_size));
        return '';
      });

    panel.NFilesLeft = this.element(panel.items.get(49))
    .address('LHCb_RunInfoSplitHLT.NFilesLeft')
    .convert(function(data)  {  return data+'&nbsp;files'; });

    this.element(panel.items.get(51)).address(''+sys+'_RunInfo.Trigger.TCKLabel');

    panel.inside = false;
    // Install tooltips and callbacks if required
    if ( options.id )  {
      var elt = document.getElementById(options.id+'-body');
      if ( options.onclick )  {
        elt.onmouseout  = lbExt.setCursorDefault;
        elt.onmouseover = lbExt.setCursorLink;
        elt.onclick     = options.onclick;
      }
      if ( options.tooltip )  {
        elt.tooltip = new Ext.ToolTip({ target: elt.id, trackMouse: true, html: options.tooltip});
      }
    }

    /// Subscribe all required data item to receive data from the provider object
    lbExt.subscribe(panel, opts.provider);
    return panel;
  };


  /// Build LHC operator comments panel
  /**
   *     +---- Shift crew : None --------------------------------------------------------
   *     |
   *     |
   *     +---- Comment updated on 18-SEP-2000 16:28 -------------------------------------
   *
   * \return Reference to ExtJs Panel object containing all data items of this widget
   *
   * \author  M.Frank
   * \version 1.0
   */
  lbExt.LHCb_shift_comments = function(opts)  {
    var options = lbExt.panelDefaults({
      title:            'Shift comments from logbook',
	  autoScroll:       true,
	  xtype:            'box',
	  layout:  {  type: 'table', columns: 1,
	    tableAttrs: { style: { padding: 5, width: '100%'}}},
	  defaults: {  border:               false,
	    xtype:                'box',
	    bodyStyle:            'padding:0',
	    autoScroll:           true,
	    autoHeight:           true,
	    style: {  textAlign:  'top',
	      width:          '100%',
	      height:         '100%',
	      paddingTop:     0,
	      paddingBottom:  0
	      }
	},
	  items: [{ html: 'undefined' }]
	  },opts);
    var panel  = this.create('Ext.panel.Panel',options);
    var elt    = this.element(panel.items.get(0));
    this.element(elt)
    .address('shiftComments')
    .convert(function(data) {return data.replace(/\n/g,'<BR>');})
    //.postUpdate(function(element) { element.updateLayout(false);})
    .subscribe(opts.provider);

    // Install tooltips and callbacks if required
    if ( options.id )  {
      elt = document.getElementById(options.id+'-body');
      if ( options.onclick )  {
        elt.onclick = options.onclick;
        elt.onmouseover = lbExt.setCursorLink;
        elt.onmouseout  = lbExt.setCursorDefault;
      }
      if ( options.tooltip )  {
        elt.tooltip = new Ext.ToolTip({ target: options.id, trackMouse: true, html: options.tooltip});
      }
    }
    return panel;
  };
  lbExt.shiftComments = lbExt.LHCb_shift_comments;

  /** Online widgets. Build property tabel of name value pairs
   *
   *  \author M.Frank
   */
  lbExt.runInfoTable = function(options) {
    var loader = new Object({
      partition:      options.partition, 
          columns:    2,
          store:      new Ext.data.ArrayStore({fields: [{name: 'name'},{name: 'value'}]}),
          items:      [],
          subscribe:  function(provider)  {
          for(var i=0; i<this.item.length; ++i)
            this.items[i].subscribe(provider);
        },
          add:        function(item_id, addr, name, fmt)  {
          var item = [name,'undefined',this,item_id];
          this.element(item).address(''+this.partition+'_RunInfo.'+addr);
          if ( fmt ) item.format(fmt);
          item.update = function(data)  {
            this[2].items.get(this[3]).update(data);
            this.store.loadData(this[2].items);
          };
          this.items.push(item);
          item = null;
          return this.items[this.items.length-1];
        }
      });
    var items = [];
    for (var prop in options.properties)  {
      var p  = options.properties[prop];
      items.push({ html: p.title ? p.title : 'unspecified', 
            xtype: 'box', 
            cls: 'MonitorDataHeader',
            colspan: p.colspan ? p.colspan : 1
            });
      items.push({html: 'undefined', 
            xtype: 'box',
            cls: 'MonitorDataItem Text-100',
            colspan: p.colspan ? p.colspan : 1
            });
    }
    var panel_opts = lbExt.panelDefaults({
      autoExpandColumn: 'value',
	  buttonAlign:      'right',
	  stripeRows:       true,
	  boxMaxWidth:      60,
	  layout:       {   type: 'table', columns: 2,
	    tableAttrs: { style: { width: '100%', padding: 2 }}},
	  defaults: {  border:             true,
	    xtype:              'box',
	    componentCls:       'Text-Left Text-Bold',
	    style: { fontSize:  'big',
	      padding:   '4 8 4 8'
	      }},
	  items: items
	  }, options);
    var panel = Ext.create('Ext.panel.Panel',panel_opts);
    if ( !panel.id ) Ext.id(panel,'Hlt1-deferred-runs-table');
    for (var i=0; i<options.properties.length; ++i)  {
      var p  = options.properties[i];
      var el = lbExt.element(panel.items.get(2*i+1))
        .address(''+options.partition+'_RunInfo.'+p.dp);
      if ( p.format ) el.format(p.format);
      if ( p.conversion ) el.conversion(p.conversion);
      el.subscribe(options.provider);
    }
    loader.panel = panel;
    panel.loader = loader;
    return panel;
  };


  /// Build table with HLT1 defer disk statistics
  /**  
   *  \author  M.Frank
   *  \version 1.0
   */
  lbExt.hltDeferredStatus = function(opts)  {
    var provider = opts.provider;
    var items = [{html: 'Disks',     xtype: 'box', cls: 'MonitorDataHeader Text-Bold' },
    {html: 'Total Disk space',xtype: 'box', cls: 'MonitorDataHeader Text-Bold' },
    {html: 'Free Disk space', xtype: 'box', cls: 'MonitorDataHeader Text-Bold' },
    {html: 'Usage',           xtype: 'box', cls: 'MonitorDataHeader Text-Bold' },
    {html: 'Files',           xtype: 'box', cls: 'MonitorDataHeader Text-Bold' },
    {html: '',                xtype: 'box'  },
    {html: 'undefined',       xtype: 'box', cls: 'MonitorDataItem Text-Bold' },
    {html: 'undefined',       xtype: 'box', cls: 'MonitorDataItem Text-Bold' }, // 7
    {html: 'undefined',       xtype: 'box', cls: 'MonitorDataItem Text-Bold' },
    {html: 'undefined',       xtype: 'box', cls: 'MonitorDataItem Text-Bold' }, // 9
    {html: 'Rates',           xtype: 'box', cls: 'MonitorDataHeader Text-Bold' },
    {html: 'HLT1 Accepted',   xtype: 'box', cls: 'MonitorDataHeader Text-Bold' },
    {html: 'HLT2 Input',      xtype: 'box', cls: 'MonitorDataHeader Text-Bold' },
    {html: 'HLT2 Accepted',   xtype: 'box', cls: 'MonitorDataHeader Text-Bold' },
    {html: 'No. Events',      xtype: 'box', cls: 'MonitorDataHeader Text-Bold' },
    {html: '',                xtype: 'box' },
    {html: 'undefined',       xtype: 'box', cls: 'MonitorDataItem Text-Bold' },
    {html: 'undefined',       xtype: 'box', cls: 'MonitorDataItem Text-Bold' },
    {html: 'undefined',       xtype: 'box', cls: 'MonitorDataItem Text-Bold' },
    {html: 'undefined',       xtype: 'box', cls: 'MonitorDataItem Text-Bold' }];
    var options = lbExt.panelDefaults({
      flex:            1,
	  title:           'HLT2 deferred activity monitor',
	  borderStyle:     'solid',
	  layout:         { type: 'table', columns: 5, tableAttrs: { style: { width: '100%' } } },
	  defaults:       { defaultType: 'textfield', 
	    style: { textAlign:     'center',
	      padding:       '5 5 5 5',
	      verticalAlign: 'middle',
	      width:         '100%' }},
	  items:   items,
	  buttons: []
	  }, opts);
    items = null;
    var panel = new Ext.Panel(options);

    if ( options.id ) panel.id = options.id;
    else Ext.id(panel,'Hlt1-deferred-activity');

    panel.disk_size = this.element(panel.items.get(6))
    .address('LHCb_RunInfoSplitHLT.DiskSize')
    .convert(function(data)  {  return sprintf('%.0f TByte',data/1024); })
    .subscribe(provider);

    panel.disk_free = this.element(panel.items.get(7))
    .address('LHCb_RunInfoSplitHLT.DiskFree')
    .convert(function(data)  {  return sprintf('%.0f TByte',data/1024); })
    .subscribe(provider);

    panel.usage = this.element(panel.items.get(8))
    .listen(panel.disk_free,function(data)  { panel.usage.disk_free = parseFloat(data[1]); })
    .listen(panel.disk_size,function(data)  { panel.usage.disk_size = parseFloat(data[1]); })
    .convert(function(data)  {
        if ( this.disk_free && this.disk_size )
          return sprintf('%.4f %%',100.0*(1.0-this.disk_free/this.disk_size));
        return 0.0;
      });

    panel.NFilesLeft = this.element(panel.items.get(9))
    .address('LHCb_RunInfoSplitHLT.NFilesLeft').subscribe(provider);

    panel.totalHLTRate = this.element(panel.items.get(16))
    .address('LHCb_RunInfoSplitHLT.TotalHLTRate').format('%d Hz').subscribe(provider);

    panel.runHlt2In = this.element(panel.items.get(17))
    .address('LHCb2_RunInfo.TFC.triggerRate').format('%d Hz').subscribe(provider);

    panel.hlt2Rate = this.element(panel.items.get(18))
    .address('LHCb2_RunInfo.HLTFarm.hltRate').format('%d Hz').subscribe(provider);

    panel.hlt2NTriggers = this.element(panel.items.get(19))
    .address('LHCb2_RunInfo.HLTFarm.hltNTriggers').subscribe(provider);
    return panel;
  };

  /** Online widgets. Load HLT2 run info from datapoint for displays.
   *
   *  \author M.Frank
   */
  lbExt.hlt2RunsLoader = function(opts)   {
    var loader = new Object(opts);
    loader.lastUpdate = 0;
    loader.num_runs = 0;
    loader.total_files = 0;
    loader.prepareRuns = function(data)  {
      var r, rr, runno, runs = [], line = [], rundata = data.split('|');
      var last_run = 0;
      this.num_runs = 0;
      this.total_files = 0;
      this.min_files = -1;
      this.max_files = 0;
      for (r = 0; r < rundata.length; r++)   {
        if ( (line.length%this.columns) == 0 && line.length>0 )  {
          runs.push(line);
          line = [];
        }
        rr = rundata[r].split('/');
        var nfile = parseInt(rr[1]);
        if ( nfile > 0.1 )  {
	  if ( this.max_files < nfile ) this.max_files = nfile;
	  if ( this.min_files == -1 || this.min_files > nfile ) this.min_files = nfile;
          this.total_files += nfile;
	  this.num_runs += 1;
	  runno = parseInt(rr[0]);
	  if ( this.addmissing && last_run > 0 )  {
	    for(var i=last_run+1; i<runno; ++i)  {
	      line.push(i);
	      line.push(0);
	    }
	    if ( (line.length%this.columns) == 0 && line.length>0 )  {
	      runs.push(line);
	      line = [];
	    }
	  }
	  last_run = runno;
          line.push(runno);
          line.push((this.minimum && nfile < this.minimum) ? this.minimum : nfile);
        }
        nfile = null;
      }
      if ( line.length > 0 ) runs.push(line);
      rundata = null;
      r = rr = null;
      line = null;
      return runs;
    };
    loader.scan = function(pref,level,el)   {
      if ( el.style && el.style.fontSize )   {
	console.info(pref+'> Level:' + level + '  ' + el.tagName + ' -> ' + el.id +
		     ' Font size:' + el.style.fontSize);
      }
      for(var i = 0; i<el.children.length; ++i)   {
	this.scan(pref+'-',level+1,el.children[i]);
      }
    };
    this.element(loader).address(opts.datapoint).callback(function(items)  {
        var now = (new Date()).getTime();
        if ( (now - this.lastUpdate) > 1000*20 )  { 
          var runs = this.prepareRuns(items[1]);
	  if ( this.id )   {
	  }
          this.store.loadData(runs);
          //this.store.setData(runs);
          if ( this.userUpdate ) this.userUpdate(runs);
          this.lastUpdate = now;
          runs = null;
        }
        now = null;
      });
    return loader;
  };


  /** Online widgets. Load HLT2 run info table
   *
   *  \author M.Frank
   */
  lbExt.hlt2RunsTablePanel = function(opts) {
    var loader = opts.loader ? opts.loader : lbExt.hlt2RunsLoader({
      id:            'hlt2RunsTableLoader',
      datapoint:     opts.datapoint,
          interval:  opts.interval ? opts.interval : 20,
          columns:    3,
          store: new Ext.data.ArrayStore({
            fields: [{name: 'r1'},
                     {name: 'f1', type: 'integer'},
                     {name: 'r2'},
                     {name: 'f2', type: 'integer'},
                     {name: 'r3'},
                     {name: 'f3', type: 'integer'}
                     ]})});
    var panel_opts = lbExt.panelDefaults({
      title:        'HLT2 runs to be processed',
	  width:        '99%',
	  layout:       'fit',
	  defaults:   {  width: '100%', cls: 'Text-Bold' },
	  items: [{
	  xtype:            'grid',
	      buttonAlign:      'right',
	      stripeRows:       true,
	      autoExpandColumn: 'f3',
	      store:            loader.store,
	      viewConfig:       {cls: 'Text-Bold' },
	      defaults:         {width: '100%' },
	      columns: [{id:'r1', header: 'Run', dataIndex: 'r1', sortable: true, cls: 'Text-Bold' },
			{id:'f1', header: 'Files', dataIndex: 'f1', sortable: true, cls: 'Text-Bold' },
			{id:'r2', header: 'Run',   dataIndex: 'r2', sortable: true, cls: 'Text-Bold' },
			{id:'f2', header: 'Files', dataIndex: 'f2', sortable: true, cls: 'Text-Bold' },
			{id:'r3', header: 'Run',   dataIndex: 'r3', sortable: true, cls: 'Text-Bold' },
			{id:'f3', header: 'Files', dataIndex: 'f3', sortable: true, cls: 'Text-Bold' }]
	      }],
	  flex:             1,
	  style: { fontSize: '1em'},
	  minHeight:        150    }, opts);
    var panel  = this.create('Ext.panel.Panel', panel_opts);
    if ( !panel.id ) Ext.id(panel,'Hlt1-deferred-runs-table');
    panel.loader = loader;
    loader.panel = panel;
    loader.title = opts.title;
    loader.userUpdate = function(runs)  {
      if ( this.panel )  {
        this.panel.setTitle(this.title+': '+
                            ' Total Runs:'+this.num_runs+
                            ' Total Files:'+this.total_files);
	// I know this is horrible, 
	// but I do not see any other way to propagate
	// the zooming to the children.
	var data_part = this.panel.body.el.dom.children[0].children[0].children[1];
	zoom_applyFontSize(data_part);
      }
    };
    loader.subscribe(opts.provider);
    return panel;
  };

  function lbExt__hlt2RunsHisto__FileLabels(data) {
    return Math.floor(data);
  };

  /** Online widgets. Display histogram with HLT2 runs to be processed.
   *
   *  \author M.Frank
   */
  lbExt.hlt2RunsHistogramPanel = function(opts) {
    var loader = opts.loader ? opts.loader : lbExt.hlt2RunsLoader({
      datapoint: opts.datapoint, 
          datapoint:     opts.datapoint,
          interval:      opts.interval ? opts.interval : 20,
          columns:       1,
          minimum:       1,
	  addmissing:    true,
          store:         new Ext.data.ArrayStore({
            fields: ['run','files'], 
                data: []})});
    var panel_opts = lbExt.panelDefaults({
      animate:       true,
	  shadow:        true,
	  cls:           'chart',
	  title:         'HLT2 runs currently processing',
	  width:         '99%',
	  maxHeight:     250,
	  minHeight:     120,
	  flipXY:        false,
	  type:          'bar',
	  store:         loader.store,
	  // define the actual bar series.
	  series: [ {
	  type:        'bar',
	      style:     { minGapWidth: 5 },
	      highlight: {
	    strokeStyle: 'black',
		fillStyle:   'gold',
		strokeStyle:  'brown',
		showStroke:   true,
		lineWidth:    2,
		axis:        'left'
		},
	      colors:  ['#0C74B9','#DAF0FB'],
	      label: {
	    field:       'files',
		display:     'over'
		},
	      xField:      'run',
	      yField:      'files'
	      } ],
    
	  axes: [ {
	  type:           'numeric',
	      position:       'left',
	      scale:          'logarithmic', // Not working....
	      grid:           true,
	      titleMargin:    20,
	      title:      {   text: 'Number of nodes' }
	  }, {
	  type:           'category',
	      position:       'bottom',
	      layout:         'discrete',
	      majorTickSteps: 1,
	      minorTickSteps: 1,
	      grid:           true,
	      title:      {   text: 'Runs currently processing' },
	      label:      {   rotate: { degrees: 335   }}
	  } ],
	  animation:         Ext.isIE8 ? false : true
	  }, opts);
    var panel = Ext.create('Ext.chart.CartesianChart',panel_opts);
    if ( !panel.id ) Ext.id(panel,'Hlt-deferred-runs-table');
    loader.panel = panel;
    loader.title = panel.title;
    loader.userUpdate = function(runs)  {
      if ( this.panel )  {  /*
	var m = '';
	for (var i=0; i<runs.length; ++i)
	  m = m + runs[i].toString()+'\n';
	  alert(m);  */
        this.panel.setTitle(this.title+': '+
                            ' Total Runs:'+this.num_runs+
                            ' Nodes processing:'+this.total_files);
	(0 == this.total_files) ? this.panel.collapse() : this.panel.expand();
      }
    };
    loader.subscribe(opts.provider);
    return panel;
  };


  lbExt.hlt2RunsTable = function(options) {
    var loader = this.hlt2RunsLoader({
      datapoint:     options.datapoint, 
          interval:  options.interval ? options.interval : 20,
          columns:    3,
          store: new Ext.data.ArrayStore({
            fields: [{name: 'r1'},
                     {name: 'f1', type: 'integer'},
                     {name: 'r2'},
                     {name: 'f2', type: 'integer'},
                     {name: 'r3'},
                     {name: 'f3', type: 'integer'}
                     ]})});

    Ext.onReady(function()  {
        options.loader = loader;
        lbExt.hlt2RunsTablePanel(options);
      });
    return loader;
  };


  /** Online widgets. Display histogram with HLT2 runs to be processed.
   *
   *  \author M.Frank
   */
  lbExt.hlt2RunsHistogram = function(options) {
    var loader = this.hlt2RunsLoader({
      datapoint: options.datapoint, 
          datapoint:     options.datapoint, 
          interval:      options.interval ? options.interval : 20,
          columns:       1,
          minimum:       1.1,
          store:         new Ext.data.ArrayStore({
            fields: ['run','files'], 
                data: [['0',1]]})});

    Ext.onReady(function(){
        var panel = new Ext.Panel({
          renderTo: options.parent ? options.parent : document.body,
              id:       options.id ? options.id : null,
              title:    options.title,
              style:    options.style ? options.style : null,
              width:    options.width ? options.width : '99%',
	      bodyCls:  'Bg-lhcblight',
              frame:    options.frame,
              boxMaxHeight: 350,
              height: 250,
              items: {
            xtype:  'columnchart',
                store:   loader.store,
                yField: 'files',
                url:    'ExtJs/resources/charts.swf',
                xField: 'run',
                xAxis:   new Ext.chart.CategoryAxis({title: 'Run Number'}),
                yAxis:   new Ext.chart.NumericAxis({
                  title: (options.yAxis && options.yAxis.title) ? options.yAxis.title : 'Files',
                      scale: 'logarithmic',
                      majorUnit: 100,
                      hideOverlappingLabels: true,
                      labelFunction: 'lbExt__hlt2RunsHisto__FileLabels',
                      snapToUnits : true
                      }),
                extraStyle: { xAxis: { labelRotation: -90  },
                  yAxis: { labelRotation: -30 } },
                animCollapse: true,
                closable: false,
                collapsed: false,
                collapsible: true,
                hideCollapseTool: false,
                collapseFirst: true
                }
          });
        if ( !panel.id ) Ext.id(panel,'Hlt1-deferred-runs-table');
        loader.panel = panel;
        loader.title = options.title;
        loader.userUpdate = function(runs)  {
          if ( this.panel )  {
            this.panel.setTitle(this.title+': '+
                                ' Total Runs:'+this.num_runs+
                                ' Total Files:'+this.total_files);
          }
        };
	loader.subscribe(options.provider);
        panel = null;
        loader = null;
      });
    return loader;
  };

 }

