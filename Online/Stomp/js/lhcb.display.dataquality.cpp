//==========================================================================
// LHCb Web Status displays
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For details please see       Online/Stomp/README
// For the licensing terms see  Online/Stomp/LICENSE
//
// Author     : M.Frank
//
//==========================================================================

_loadScript('lhcb.display.items.cpp');
_loadScript('lhcb.display.widgets.cpp');
_loadFile('lhcb.display.general','css');
_loadFile('lhcb.display.fsm','css');

/** Data Quality web widgets
 *
 *  @author  M.Frank
 */
DQ_FSM = function(options) {
  var tab = document.createElement('table');
  var tb = document.createElement('tbody');
  var tr, td, i, item, ctr;

  tab.logger = options.logger;
  tab.className = tb.className   = 'MonitorPage';
  if ( options.legend ) {
    tr = document.createElement('tr');
    tr.style.height = 35;
    tr.appendChild(Cell('Controller',    1, 'MonitorDataHeader'));
    tr.appendChild(Cell('FSM Node State',2, 'MonitorDataHeader'));
    tr.appendChild(Cell('Architecture',  1, 'MonitorDataHeader'));
    tr.appendChild(Cell('Last Command',  1, 'MonitorDataHeader'));
    tr.appendChild(Cell('Task-Count',    1, 'MonitorDataHeader'));
    tr.appendChild(Cell('Partition',     1, 'MonitorDataHeader'));
    tr.appendChild(Cell('Ctrl State',    1, 'MonitorDataHeader'));
    tr.appendChild(Cell('Event Count',   1, 'MonitorDataHeader'));
    tb.appendChild(tr);
  }
  tab.items = [];
  for(i=1; i<5; ++i)   {
    ctr = sprintf('MONA10%02d_R',i);
    tr = document.createElement('tr');
    tr.appendChild(Cell(ctr,1));

    item = FSMItem('lbWeb.'+ctr,options.logger,true);
    tab.items.push(item);
    item.style.width='20%';
    tr.appendChild(item);
    tr.appendChild(item.lock);

    item = StyledItem('lbWeb.'+ctr+'_Controller.architecture');
    item.style.width = '10%';
    tab.items.push(item);
    tr.appendChild(item);

    item = StyledItem('lbWeb.'+ctr+'_Controller.command');
    item.style.width = '10%';
    tab.items.push(item);
    tr.appendChild(item);

    item = StyledItem('lbWeb.'+ctr+'_Controller.count');
    item.style.width = '10%';
    tab.items.push(item);
    tr.appendChild(item);

    item = StyledItem('lbWeb.'+ctr+'_Controller.partName');
    item.style.width = '10%';
    tab.items.push(item);
    tr.appendChild(item);

    item = StyledItem('lbWeb.'+ctr+'_Controller.state');
    item.style.width = '10%';
    tab.items.push(item);
    tr.appendChild(item);
    
    item = null;
    if ( i == 1 )
      item = StyledItem('lbWeb.MONA10_R_Work.eventsRead',null,'%d events read from disk');
    else if ( i == 2 )
      item = StyledItem('lbWeb.MONA10_R_Work.eventsReceived1',null,'%d events received');
    else if ( i == 3 )
      item = StyledItem('lbWeb.MONA10_R_Work.eventsReceived2',null,'%d events received');
    else if ( i == 4 )
      item = StyledItem('lbWeb.MONA10_R_Work.eventsReceived3',null,'%d events received');
    if ( item )  {
      item.style.width = '15%';
      tab.items.push(item);
      tr.appendChild(item);
    }
    tb.appendChild(tr);
  }
  tr = document.createElement('tr');
  tr.appendChild(Cell('MONA10_R_Work',1));
  item = FSMItem('lbWeb.MONA10_R_Work',options.logger,true);
  tab.items.push(item);
  tr.appendChild(item);
  tr.appendChild(item.lock);
  tr.appendChild(Cell('',5));
  tb.appendChild(tr);

  tab.appendChild(tb);

  tab.subscribe = function(provider) {
    for(var i=0; i<this.items.length; ++i)
      provider.subscribeItem(this.items[i]);
  };
  ctr  = null;
  item = null;
  i    = null;
  td   = null;
  tr   = null;
  tb   = null;
  return tab;
};

var DQ_Summary = function(options)  {
  var tab = document.createElement('table');
  var tb = document.createElement('tbody');
  var tr, td, item;

  tab.logger = options.logger;
  tab.className = tb.className   = 'MonitorPage';
  tab.style.height = '85px';
  if ( options.legend ) {
    tr = document.createElement('tr');
    tr.style.height = 25;
    tr.appendChild(td=Cell('DataQuality driver overview',1,'MonitorDataHeader'));
    td.colSpan = 4;
    tb.appendChild(tr);

    tr = document.createElement('tr');
    tr.style.height = 25;
    tr.appendChild(Cell('Status',1,'MonitorDataHeader'));
    tr.appendChild(Cell('Automatic',1,'MonitorDataHeader'));
    tr.appendChild(Cell('Current Run',1,'MonitorDataHeader'));
    tr.appendChild(Cell('Workload Status',1,'MonitorDataHeader'));
    tb.appendChild(tr);
  }

  tab.items = [];
  tr = document.createElement('tr');
  tb.appendChild(tr);

  item = StyledItem('lbWeb.MONA10_R_Work.status');
  tab.items.push(item);
  tr.appendChild(item);

  item = StyledItem('lbWeb.MONA10_R_Work.automaticRampUp');
  item.conversion = function(data)  {
    if ( data == '1' ) return 'Autostart ENABLED';
    return 'Autostart DISABLED';
  };
  tab.items.push(item);
  tr.appendChild(item);

  item = StyledItem('lbWeb.MONA10_R_Work.runNumber');
  tab.items.push(item);
  tr.appendChild(item);

  item = StyledItem('lbWeb.MONA10_R_Work.work');
  item.conversion = function(data)  {
    if ( data == '0' ) return 'Nothing to be processed';
    return 'More runs queued for processing....';
  };
  tab.items.push(item);
  tr.appendChild(item);

  tab.appendChild(tb);
  item = null;
  td   = null;
  tr   = null;
  tb   = null;
  tab.subscribe = function(provider) {
    for(var i=0; i<this.items.length; ++i)
      provider.subscribeItem(this.items[i]);
  };
  return tab;  
};

var DQ_Result = function(options)  {
  var tab = document.createElement('table');
  var tb = document.createElement('tbody');
  var tr, td, item;

  tab.logger = options.logger;
  tab.className = tb.className   = 'MonitorPage';
  if ( options.legend ) {
    tr = document.createElement('tr');
    tb.appendChild(tr);
    tr.style.height = 35;
    tr.appendChild(td=Cell('Data Quality Results and Runs to be Checked',1,'MonitorDataHeader'));
    td.style.textAlign = 'center';
    tab.legend = td;
  }

  tr = document.createElement('tr');
  tb.appendChild(tr);
  td = document.createElement('td');
  td.style.width  = '100%';
  td.style.height = '200px';
  td.style.border = 'none';
  td.className    = 'MonitorData';
  tr.appendChild(td);
  var fr = document.createElement('iframe');
  td.appendChild(fr);
  fr.src = lhcb.constants.lhcb_comet_url()+'/DQ/DQ_Result.html';
  fr.id           = 'external_dq_result';
  fr.bgcolor      = '#CC';
  fr.style.width  = '100%';
  fr.style.height = '200px';
  fr.scrolling    = 'auto';
  fr.marginWidth  = 1;
  fr.marginHeight = 1;
  tab[fr.id] = fr;
  tab.appendChild(tb);
  tab.subscribe = function(provider) {
    var fr = this['external_dq_result'];
    var date = new Date();
    fr.src = lhcb.constants.lhcb_comet_url()+'/DQ/DQ_Result.html?'+date.valueOf();
    if ( tab.legend )  {
      tab.legend.innerHTML = 'Data Quality Results and Runs to be Checked. Download from: '+
	date.toTimeString().substr(0,8)+'  '+date.toDateString();
    }
    date = fr = null;
  };
  tab.subscribe[fr.id] = fr;
  setInterval(tab.subscribe, 30000);
  fr = null;
  return tab;  
};


var DQ_DBDump = function(options)  {
  var tab = document.createElement('table');
  var tb = document.createElement('tbody');
  var tr, td, td1, td2, item, fr;

  tab.appendChild(tb);
  tab.style.width  = '100%';
  tb.style.width  = '100%';
  tab.logger = options.logger;
  tab.className = tb.className   = 'MonitorPage';
  if ( options.legend ) {
    tr = document.createElement('tr');
    tb.appendChild(tr);
    tr.style.height = 35;
    tr.appendChild(td1=Cell('Data Quality database dump',1,'MonitorDataHeader'));
    td1.style.textAlign = 'center';
    tooltips.set(td1,'Click to update data if availible');
    tr.appendChild(td2=Cell('Data Quality Files',1,'MonitorDataHeader'));
    td2.style.textAlign = 'center';
    tooltips.set(td2,'Click to update data if availible');
    tab.legend = { 'left': td1, 'right': td2};
  }
  tr = document.createElement('tr');
  tr.style.width  = '100%';
  tb.appendChild(tr);
  td = document.createElement('td');
  tr.appendChild(td);
  td.style.width  = '60%';
  td.style.height = '400px';
  td.style.border = 'none';
  td.className    = 'MonitorData';
  fr = document.createElement('iframe');
  td.appendChild(fr);
  fr.src = lhcb.constants.lhcb_comet_url()+'/DQ/DQ_Dump.html';
  fr.id           = 'external_dq_dbdump';
  fr.bgcolor      = '#CC';
  fr.style.width  = '800px';
  fr.style.width  = '100%';
  fr.style.height = td.style.height;
  fr.scrolling    = 'auto';
  fr.marginWidth  = 1;
  fr.marginHeight = 1;
  tab[fr.id] = fr;
  fr.subscribe = function(provider) {
    var fr = tab['external_dq_dbdump'];
    var date = new Date();
    fr.src = lhcb.constants.lhcb_comet_url()+'/DQ/DQ_Dump.html?'+date.valueOf();
    if ( tab.legend )  {
      tab.legend.left.innerHTML = 'Data Quality database dump. Download from: '+
	date.toTimeString().substr(0,8)+'  '+date.toDateString();
    }
    date = fr = null;
  };
  if ( tab.legend ) {
    tab.legend.left.onclick = function() {
      var fr = tab['external_dq_dbdump'];
      fr.subscribe(null);
    };
  };
  setInterval(fr.subscribe, 50000);

  td = document.createElement('td');
  tr.appendChild(td);
  td.style.width  = '40%';
  td.style.height = '400px';
  td.style.border = 'none';
  td.className    = 'MonitorData';
  fr = document.createElement('iframe');
  td.appendChild(fr);
  fr.src = lhcb.constants.lhcb_comet_url()+'/DQ/DQ_Files.html';
  fr.id           = 'external_dq_files';
  fr.bgcolor      = '#CC';
  fr.style.width  = '450px';
  fr.style.width  = '100%';
  fr.style.height = td.style.height;
  fr.scrolling    = 'auto';
  fr.marginWidth  = 1;
  fr.marginHeight = 1;
  tab[fr.id] = fr;
  fr.subscribe = function(provider) {
    var fr   = tab['external_dq_files'];
    var date = new Date();
    fr.src = lhcb.constants.lhcb_comet_url()+'/DQ/DQ_Files.html?'+date.valueOf();
    if ( tab.legend )  {
      tab.legend.right.innerHTML = 'Data Quality files on mona1001. Download from: '+
	date.toTimeString().substr(0,8)+'  '+date.toDateString();
    }
    date = fr = null;
  };
  if ( tab.legend ) {
    tab.legend.right.onclick = function() {
      var fr = tab['external_dq_files'];
      fr.subscribe(null);
    };
  };
  setInterval(fr.subscribe, 30000);
  tab.subscribe = function(provider) {
    var fr = tab['external_dq_files'];
    fr.subscribe(provider);
    fr = tab['external_dq_dbdump'];
    fr.subscribe(provider);
  };
  td = tr = tb = item = fr = null;
  return tab;  
};


var DQ_Files = function(options)  {
  var tab = document.createElement('table');
  var tb = document.createElement('tbody');
  var tr, td, item;

  tab.logger = options.logger;
  tab.className = tb.className   = 'MonitorPage';
  if ( options.legend ) {
    tr = document.createElement('tr');
    tb.appendChild(tr);
    tr.style.height = 35;
    tr.appendChild(td=Cell('Data Quality database dump',1,'MonitorDataHeader'));
    td.style.textAlign = 'center';
    tab.legend = td;
  }

  tr = document.createElement('tr');
  tb.appendChild(tr);
  td = document.createElement('td');
  td.style.width  = '100%';
  td.style.height = '200px';
  td.style.border = 'none';
  td.className    = 'MonitorData';
  tr.appendChild(td);
  var fr = document.createElement('iframe');
  td.appendChild(fr);
  fr.src = lhcb.constants.lhcb_comet_url()+'/DQ/DQ_Files.txt';
  fr.id           = 'external_dq_files';
  fr.bgcolor      = '#CC';
  fr.style.width  = td.style.width;
  fr.style.height = td.style.height;
  fr.scrolling    = 'auto';
  fr.marginWidth  = 1;
  fr.marginHeight = 1;
  tab[fr.id] = fr;
  tab.appendChild(tb);
  tab.subscribe = function(provider) {
    var fr = this['external_dq_files'];
    var date = new Date();
    fr.src = lhcb.constants.lhcb_comet_url()+'/DQ/DQ_Files.txt?'+date.valueOf();
    if ( tab.legend )  {
      tab.legend.innerHTML = 'Data Quality files on mona1001. Download from: '+
	date.toTimeString().substr(0,8)+'  '+date.toDateString();
    }
    date = fr = null;
  };
  tab.subscribe[fr.id] = fr;
  setInterval(tab.subscribe, 30000);
  fr = null;
  return tab;  
};

var DQ_Current = function(options)  {
  var tab = document.createElement('table');
  var tb = document.createElement('tbody');
  var tr, td, item;

  tab.logger = options.logger;
  tab.className = tb.className   = 'MonitorPage';
  if ( options.legend ) {
    tr = document.createElement('tr');
    tb.appendChild(tr);
    tr.appendChild(td=Cell('Data Quality Results and Runs to be Checked',1,'MonitorDataHeader'));
    td.style.textAlign = 'center';
    tab.legend = td;
  }

  tr = document.createElement('tr');
  tb.appendChild(tr);
  td = document.createElement('td');
  td.style.width  = '100%';
  td.style.height = '50px';
  td.style.border = 'none';
  td.className    = 'MonitorData';
  tr.appendChild(td);
  var fr = document.createElement('iframe');
  td.appendChild(fr);
  fr.src = lhcb.constants.lhcb_comet_url()+'/DQ/DQ_Input.txt';
  fr.id           = 'external_dq_current';
  fr.bgcolor      = '#CC';
  fr.style.width  = '100%';
  fr.style.height = '50px';
  fr.scrolling    = 'auto';
  fr.marginWidth  = 1;
  fr.marginHeight = 1;
  tab[fr.id] = fr;
  tab.appendChild(tb);
  tab.subscribe = function(provider) {
    var fr = this['external_dq_current'];
    var date = new Date();
    fr.src = lhcb.constants.lhcb_comet_url()+'/DQ/DQ_Input.txt?'+date.valueOf();
    if ( tab.legend )  {
      tab.legend.innerHTML = 'Last run processing. Download from '+
	date.toTimeString().substr(0,8)+'  '+date.toDateString();
    }
    date = fr = null;
  };
  tab.subscribe[fr.id] = fr;
  setInterval(tab.subscribe, 30000);
  fr = null;
  return tab;  
};

var DQ_Help = function(options)  {
  var tab = document.createElement('table');
  var tb = document.createElement('tbody');
  var tr, td, item;

  tab.logger = options.logger;
  tab.className = tb.className   = 'MonitorPage';
  tab.style.width = '100%';
  if ( options.legend ) {
    tr = document.createElement('tr');
    tb.appendChild(tr);
    tr.appendChild(td=Cell('Data Quality Piquet Help: '+
			   'Click here for recipes '+
			   'if things get stuck!',
			   1,'MonitorBigHeader'));
    td.style.textAlign = 'center';
    td.style.backgroundColor = 'yellow';
    tab.legend = td;
  }
  tab.appendChild(tb);
  tab.width = '100%';
  tab.subscribe = function(provider) {};
  tab.onclick = function()  {
    var date = new Date();
    var url = '//lbcomet.cern.ch/Online/html/DQhelp.htm?'+date.valueOf();
    document.location = url;
    //window.open(url);
    date = url = null;
  };
  return tab;  
};

/** DataQuality web widgets
 *
 *  @author  M.Frank
 */
var DQstatus = function(msg)   {
  var table = lhcb.widgets.SubdetectorPage('DataQuality Operation State');

  table.options = {logo:    lhcb_logo('http://lhcb.web.cern.ch/lhcb/lhcb_page/lhcb_logo.png'),
        	   logo_url:'http://cern.ch/lhcb-public',
		   title:   'Online DataQuality Status',
		   tips:    'DataQuality operations status'
  };

  lhcb.widgets.dq = new Object();
  table.attachWidgets = function()   {
    var sum   = DQ_Summary({legend:true,logger:this.logger});
    var fsm   = DQ_FSM({legend:true,logger:this.logger});
    var res   = DQ_Result({legend:true,logger:this.logger});
    var input = DQ_Current({legend:true,logger:this.logger});
    var help  = DQ_Help({legend:true,logger:this.logger});
    //var files = DQ_Files({legend:true,logger:this.logger});
    var dump  = DQ_DBDump({legend:true,logger:this.logger});
    this.left.addItem(sum);
    this.left.style.width='33%';
    this.right.addItem(input);
    this.right.style.width='77%';
    this.top.addItem(fsm);
    this.bottom.addItem(help);
    this.bottom.addItem(res);
    this.bottom.addItem(dump);
    //this.bottom.addItem(files);
    sum = fsm = res = help = dump = files = null;
  };
  return table;
};

var dataquality_unload = function() {
  lhcb.widgets.SubdetectorPage.stop();
};
var dataquality_body = function()   {
  return lhcb.widgets.SubdetectorPage.start(DQstatus);
};
