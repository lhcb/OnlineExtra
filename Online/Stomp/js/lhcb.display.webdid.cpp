//==========================================================================
// LHCb Web Status displays
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For details please see       Online/Stomp/README
// For the licensing terms see  Online/Stomp/LICENSE
//
// Author     : M.Frank
//
//==========================================================================

Ext.require(['*']);

Ext.onReady(function(){
    Ext.QuickTips.init();
    var webdid = function()  {
      var tree_opts = {};
      var idx = document.location.pathname.lastIndexOf('/');
      this.internal_data = false;
      this.home_page = document.location.pathname.substr(0,idx) + '/Code/html/WebdidNews.htm';

      /// Build panel data programatically
      if ( this.internal_data )   {
	/// Add a new DNS domain to the panel
	this.addDomain = function(root, name)   {
	  root.children.push({text: name, id: name, qtip: name, leaf: true });
	};

	/// Add a new sub-farm to the panel
	this.addSubfarm = function(root, name, first, last)   {
	  var i, dns = { text: name, id: name, expanded: false, children: []};
	  root.children.push(dns);
	  for(i = first; i<=last; ++i)
	    this.addDomain(dns, sprintf("%s%02d",name,i));
	  i = dns = null;
	};

	this.domain_data = { children: [] };
	this.domain_data.children.push({ text: "Webdid", id: "DNS-Domains", expanded: true, children: []});
	this.addSubfarm(this.domain_data.children[0],'hlta',1,10);
	this.addSubfarm(this.domain_data.children[0],'hltb',1,10);
	this.addSubfarm(this.domain_data.children[0],'hltc',1,10);
	this.addSubfarm(this.domain_data.children[0],'hltd',1,10);
	this.addSubfarm(this.domain_data.children[0],'hlte',1,10);
	this.addSubfarm(this.domain_data.children[0],'hltf',1,12);
	this.addDomain(this.domain_data.children[0],'mona08');
	this.addDomain(this.domain_data.children[0],'mona09');
	this.addDomain(this.domain_data.children[0],'mona10');
	this.addDomain(this.domain_data.children[0],'storectl01');
	//this.addDomain(this.domain_data.children[0],'ecs01');
	//this.addDomain(this.domain_data.children[0],'ecs02');
	this.addDomain(this.domain_data.children[0],'ecs03');
	this.addDomain(this.domain_data.children[0],'ecs04');

	tree_opts.root = this.domain_data;
      }
      else	{        /// Load panel data from ajax
	this.randomNumber = Math.floor(Math.random()*10000001);
	this.dnsDomains = Ext.create('Ext.data.TreeStore', {
	  autoLoad: false,
	      folderSort: true,
	      sorters: [{
	      property: 'text',
		  direction: 'ASC'
		  }],
	      root: {
	    text: 'TopNode',
		id: null,
		expanded: true
		},

	      proxy: {
	    type: 'rest',
		url: 'Code/lhcb.webdid.nodes.json',
		extraParams: {  browser: this.randomNumber   },
		reader: {
	      type: 'json',
		  root: 'children'
		  }
	    },
	      listeners: {
	    load: function(myStore, node, records, successful, eopts) {
		setTimeout(this.expandRoot, 100);
	      }
	    }
	  });
    
	this.expandRoot = function()    {
	  var node = this.dnsDomains.getRootNode();
	  node.expandChildren();
	};
	tree_opts.store = this.dnsDomains;
      }

      /// Setup options for the domain tree:
      tree_opts.title        = 'DIM DNS Domains',
      tree_opts.width        = 360,
      tree_opts.height       = 150,
      tree_opts.root         = this.domain_data,
      tree_opts.rootVisible  = false,
      tree_opts.autoScroll   = true,
      tree_opts.collapsible  = true,
      tree_opts.webdid       = this,
      tree_opts.iframe       = null,
      tree_opts.listeners    = {}; 
      tree_opts.listeners.itemclick = function(view, rec, item, index, evtObj) {
	if ( rec.get('leaf') == true)   {
	  this.iframe.src = "/did/"+rec.raw.text;
	  document.title = 'LHCb Online: DIM Domain information for '+rec.raw.text;
	}
      };
      tree_opts.listeners.cellclick = function(view, rec, item, index, evtObj) {
	if ( rec.textContent.toLowerCase() == 'webdid' )  {
	  this.iframe.src = this.webdid.home_page;
	}
      };

      this.dnsTree = Ext.create('Ext.tree.Panel', tree_opts);
      tree_opts = null;

      /// Iframe containing the DID information
      this.didPanel = Ext.create('Ext.Panel', {
	header:          false,
	    width:       360,
	    height:      150,
	    store:       null,
	    autoScroll:  true,
	    collapsible: true,
	    webdid:      this,
	    html: '<iframe id="did-iframe" src="'+this.home_page+'" width="100%" height="100%"></iframe>',
	    id: 'specific_panel_id'
	  });

      /// Create view port
      Ext.create('Ext.Viewport', {
	layout: 'border',
	    title: 'DID',
	    items: [{ layout: 'fit',
		id:           'tree-panel',
		region:       'west',
		border:       false,
		split:        true,
		margins:      '2 0 5 5',
		width:        150,
		minSize:      100,
		maxSize:      500,
		webdid:       this,
		items:        [this.dnsTree]
		},{ layout:   'fit',
		id:           'did-browser',
		region:       'center',
		border:       false,
		split:        true,
		margins:      '2 0 5 5',
		width:        580,
		minSize:      100,
		maxSize:      600,
		items:        [this.didPanel]
		}],
	    renderTo: Ext.getBody()
	    });
      this.dnsTree.iframe = document.getElementById('did-iframe');
      this.dnsTree.iframe.onload = function() {
	document.title = 'LHCb Online DIM Domain information';
      };
    };
    /// Instantiate all:
    webdid();
  });
