//==========================================================================
// LHCb Web Status displays
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For details please see       Online/MQTT/README   ---- CHANGE FOLDER NAME, CL 27/07
// For the licensing terms see  Online/MQTT/LICENSE  ---- CHANGE FOLDER NAME, CL 27/07
//
// Author     : M.Frank
//
//==========================================================================

if ( null == _dataLogger )   {
  var _dataLogger     = null;

  _loadScriptAbs(_lhcbScriptBase,'lhcb.tools.cpp');

  var LOG_ERROR       = 0;
  var LOG_WARNING     = 1;
  var LOG_INFO        = 2;
  var LOG_DEBUG       = 3;
  var LOG_VERBOSE     = 4;

  /**@class OutputLogger
   *
   *
   *  @author  M.frank
   *  @version 1.0
   */
  var OutputLogger = function(parent, len, level, style)  {
    this.className    = style;
    this.lines        = null;
    if ( len>0 ) {
      this.lines = new Array();
      this.lines.length = 0;
    }
    this.parent       = parent;
    this.length       = len;
    this.level        = level;
    this.curr         = 0;
    this.table        = document.createElement('table');
    this.body         = document.createElement('tbody');
    this.messages     = document.createElement('tr');
    this.output_td    = document.createElement('td');
    this.output       = document.createElement('div');
    this.b_hide       = document.createElement('td');
    this.b_show       = document.createElement('td');
    this.b_clear      = document.createElement('td');

    _dataLogger = this;

    this.output.className = this.className;
    this.output.innerHTML = '';
    this.output_td.colSpan = 3;
    this.output_td.appendChild(this.output);
    this.messages.appendChild(this.output_td);

    var row = document.createElement('tr');
    row.appendChild(this.b_hide);
    row.appendChild(this.b_show);
    row.appendChild(this.b_clear);

    this.body.appendChild(row);
    this.body.appendChild(this.messages);

    this.table.appendChild(this.body);
    this.parent.appendChild(this.table);

    this.table.style.width     = '100%';
    this.body.style.width      = '100%';
    this.output.style.width    = '100%';
    this.output_td.style.width = '100%';
    this.messages.style.width  = '100%';
    this.output.style.fontSize = 'small';

    this.showMessages = function() {
      if ( this.lines ) {
	if ( this.output != null && this.lines.length > 0 ) {
	  var message = '';
	  for(var i=this.curr+1; i<this.lines.length; ++i)
	    message += '&rarr; ' + this.lines[i] + '<br></br>';
	  for(var i=0; i<=this.curr; ++i)
	    message += '&rarr; ' + this.lines[i] + '<br></br>';
	  this.output.innerHTML = message;
	  this.output.scrollTop = this.output.scrollHeight;
	}
      }
      return this;
    };

    this.print = function(level, msg) {
      if ( console && console.log )   {
	var d = new Date().toLocaleString();
	if ( level == LOG_VERBOSE )
	  console.debug(d+' [VERBOSE]:  '+msg);
	else if ( level == LOG_DEBUG )
	  console.log(d+ ' [DEBUG]:    '+msg);
	else if ( level == LOG_INFO )
	  console.info(d+ ' [INFO]:     '+msg);
	else if ( level == LOG_WARNING )
	  console.warn(d+ ' [WARNING]:  '+msg);
	else if ( level == LOG_ERROR )
	  console.error(d+' [ERROR]:    '+msg);
	else
	  console.warn(d+ ' [UNKNOWN]:  '+msg);
	d = null;
      }
      if ( this.lines != null && level <= this.level && this.length>0 ) {
	if ( this.lines.length < this.length ) {
	  this.lines.length = this.lines.length+1;
	}
	if ( this.curr>this.lines.length-1 ) this.curr = 0;
	if ( level == LOG_VERBOSE ) {
	  this.lines[this.curr] = this.format(Date().toString()+' [VERBOSE]:  '+msg);
	}
	else if ( level == LOG_DEBUG ) {
	  this.lines[this.curr] = this.format(Date().toString()+' [DEBUG]:    '+msg);
	}
	else if ( level == LOG_INFO ) {
	  this.lines[this.curr] = this.format(Date().toString()+' [INFO]:     '+msg);
	}
	else if ( level == LOG_ERROR ) {
	  this.lines[this.curr] = this.format(Date().toString()+' [ERROR]:    '+msg);
	}
	else  {
	  this.lines[this.curr] = this.format(Date().toString()+' [UNKNOWN]:  '+msg);
	}
	this.showMessages();
	this.curr = this.curr + 1;
      }
      /*
       */
      return this;
    };

    this.hide = function() {
      this.b_hide.innerHTML = '';
      this.b_clear.innerHTML = '';
      if ( this.length > 0 ) {
	this.b_show.innerHTML = '<BUTTON class="DisplayButton" onclick="_dataLogger.show()">Show Messages</BUTTON>';
      }
      else {
	this.b_show.innerHTML = '';
      }
      this.output_td.removeChild(this.output);
    };
  
    this.show = function() {
      this.b_show.innerHTML = '';
      this.b_clear.innerHTML = '<BUTTON class="DisplayButton" onclick="_dataLogger.clear()">Clear</BUTTON>';
      this.b_hide.innerHTML  = '<BUTTON class="DisplayButton" onclick="_dataLogger.hide()">Hide Messages</BUTTON>';
      this.output_td.appendChild(this.output);
      this.showMessages();
    };
  
    this.error = function(msg)     {  return this.print(LOG_ERROR,msg);     };
  
    this.info = function(msg)      {  return this.print(LOG_INFO,msg);      };
  
    this.warning = function(msg)   {  return this.print(LOG_WARNING,msg);   };
  
    this.debug = function(msg)     {  return this.print(LOG_DEBUG,msg);     };

    this.verbose = function(msg)   {  return this.print(LOG_VERBOSE,msg);   };

    this.clear = function() {
      if ( this.lines ) {
	this.curr         = 0;
	this.lines.length = 0;
	this.output.innerHTML = '';
	this.output.scrollTop = this.output.scrollHeight;
      }
    };

    this.format = function(expr) {
      var s = prettyprint(expr);
      s = htmlescape(s);
      return s;
    };
  
    var htmlescape = function(expr) {
      var s = expr.replace("&", "&amp;", "g");
      s = s.replace("<", "&lt;", "g");
      s = s.replace(">", "&gt;", "g");
      s = s.replace(" ", "&nbsp;", "g");
      s = s.replace("\n", "<br></br>", "g");
      return s;
    };

    var prettyprint = function(s) {
      var q = "(";
      if(typeof(s) == "string") return s;
      for (var i=0; i<s.length; i++) {
	if (typeof(s[i]) != "object")
	  q += s[i];
	else
	  q += prettyprint(s[i]);
	if (i < s.length -1)
	  q += " ";
      }
      q += ")";
      return q;
    };
  
    this.hide();
    return this;
  };

  var dataLoggerTest = function(id) {
    var logger  = new OutputLogger(document.getElementById(id),10,LOG_DEBUG);
    logger.print(LOG_DEBUG,   'LOG_DEBUG: hello 1');
    logger.print(LOG_INFO,    'LOG_INFO: hello 1');
    logger.print(LOG_WARNING, 'LOG_WARNING: hello 1');
    logger.print(LOG_ERROR,   'LOG_ERROR: hello 1');
    for(var k=0; k<20; ++k)  {
      logger.info(k+' : hello 1  --> '+k);
    }
  };

  if ( _debugLoading ) alert('Script lhcb.display.logger.cpp loaded successfully');

 }
