//==========================================================================
// LHCb Web Status displays
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For details please see       Online/Stomp/README
// For the licensing terms see  Online/Stomp/LICENSE
//
// Author     : M.Frank
//
//==========================================================================

_loadFile('lhcb.display.ext','css');
_loadFile('lhcb.display.ext.fsm','css');
_loadScript('lhcb.display.zoom.cpp');
_loadScript('lhcb.display.extWidgets.cpp');
_loadScript('lhcb.display.extPanels.cpp');

/// Main entry point to create the 'LHCb Velo_Move' web page
/*
 *    \author  M.Frank
 *    \version 2.0
 */
var lhcb_test_page = function(id, messages, font_size) {

  /** ExtJs widget to display the velo vacuum status
   *
   *  +--- Velo Vacuum Status. Pressures in [mbar] ------------
   *  +
   *  + Device Device Device Device Device Device 
   *  +
   *  + value  value  value  value  value  value  
   *  +--------------------------------------------------------
   *
   *  \author  M.Frank
   *  \version 1.0
   */
  var test_func = lbExt.veloInfrastructureStatus = function(opts) {
    var width   = opts.hasOwnProperty('width')   ? opts.width  : '99.8%';
    var title   = opts.hasOwnProperty('title')   ? opts.title  : 'Velo Infrastructure Status';
    var id      = opts.hasOwnProperty('id')      ? opts.id     : 'lhcb-velo-infrastructure-status';
    var options = lbExt.panelDefaults({id: id,
	  title:            title,
	  width:            width,
	  layout:         { type: 'table', columns: 6,
	    tableAttrs:   { style: { padding: '0 10 0 10', width: '100%' }}},
	  defaults: {       border:             true,
	    xtype:              'box',
	    style: { textAlign: 'left',
	      padding:   '4 8 4 8'
	      }
	},
	  items: [{ html: 'AP411',                cls: 'MonitorDataHeader Text-Bold' }, // 0
		  { html: 'DP411',                cls: 'MonitorDataHeader Text-Bold' },
		  { html: 'PE411',                cls: 'MonitorDataHeader Text-Bold' },
		  { html: 'PE412',                cls: 'MonitorDataHeader Text-Bold' },
		  { html: 'PE421',                cls: 'MonitorDataHeader Text-Bold' },
		  { html: 'PE422',                cls: 'MonitorDataHeader Text-Bold' }]
	  }, opts);
    var panel  = lbExt.create('Ext.panel.Panel',options);
    //lbExt.element(panel.items.get(6)).address('lbWeb.veloVac_Barotrons.AP411.Pressure').format('%7.1f');
    //lbExt.element(panel.items.get(7)).address('lbWeb.veloVac_Barotrons.DP411.Pressure').format('%7.1e');
    //lbExt.element(panel.items.get(8)).address('lbWeb.veloVac_Meters.PE411.Vacuum').format('%7.1e');
    //lbExt.element(panel.items.get(9)).address('lbWeb.veloVac_Meters.PE412.Vacuum').format('%7.1e');
    //lbExt.element(panel.items.get(10)).address('lbWeb.veloVac_Meters.PE421.Vacuum').format('%7.1e');
    //lbExt.element(panel.items.get(11)).address('lbWeb.veloVac_Meters.PE422.Vacuum').format('%7.1e');
    // Install tooltips and callbacks if required
    if ( options.id )  {
      var elt = document.getElementById(id+'-body');
      if ( options.onclick )  {
	elt.onmouseout  = lbExt.setCursorDefault;
	elt.onmouseover = lbExt.setCursorLink;
	elt.onclick     = options.onclick;
      }
      if ( options.tooltip )  {
	elt.tooltip = new Ext.ToolTip({ target: elt.id, trackMouse: true, html: options.tooltip});
      }
    }
    /// Subscribe all required data item to receive data from the provider object
    lbExt.subscribe(panel, opts.provider);
    return panel;
  };

  /// Create the application object
  var app = lbExt.createApp({title: 'lbExt/ExtJs test page',
	messages: messages,
	fontSize: font_size});

  /// Create view port
  try  {
    app.run({ id: id,
	  maxWidth:    1200,
	  minHeight:    600,
	  height:       800,
	  defaults:       { cls: 'Bg-lhcblight NoPadding Text-Center Text-Bold Text-Big',
	    bodyCls:   'Bg-lhcblight NoPadding Text-Center Text-Bold Text-Big',
	    bodyStyle: 'padding: 0px'
	    },
	  items: [
		  { html: 'Header pane', height: 30},
		  { autoScroll:      true,
		      items:[//lbExt.veloPositionSummary({provider: app.provider}),
			     //lbExt.veloVacuumStatus(   {provider: app.provider, units: true}),
			     //lbExt.LHCComment({provider: app.provider}),
			     //lbExt.LHCSummary({provider: app.provider}),
			     //lbExt.page1Summary('LHCb',{provider: app.provider}),
			     //lbExt.shiftComments({provider: app.provider}),
			     //lbExt.planOfTheDay({provider: app.provider}),
			     test_func({id: id+'-test',
				   tooltip:  'blabla',
				   minHeight: 200,
				   provider: app.provider})]
		      }
		  ]
	  });
  }
  catch(e)  {
    app.logger.error('[EXCEPTION] Ext:'+Ext.version+'\n\nException: '+e);
  }
};

var test_unload = function()  {
  dataProviderReset();
};

var test_body = function()  {
  var msg  = the_displayObject['messages'];
  var siz  = the_displayObject['size'];
  return lhcb_test_page('test-main', msg, siz);
};
console.info('Script lhcb.display.test.cpp loaded successfully');
