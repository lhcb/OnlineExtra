//==========================================================================
// LHCb Web Status displays
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For details please see       Online/Stomp/README
// For the licensing terms see  Online/Stomp/LICENSE
//
// Author     : M.Frank
//
//==========================================================================

_loadFile('lhcb.display.ext','css');
_loadFile('lhcb.display.ext.fsm','css');
_loadScript('lhcb.display.zoom.cpp');
_loadScript('lhcb.display.extWidgets.cpp');

/// Main entry point to create the 'LHCb Page1' web page
/*
 *    \author  M.Frank
 *    \version 2.0
 */
var lhcb_page1 = function(id, system, messages, font_size) {

  // Update window title
  lbExt.setWindowTitle(system+' Page 1');

  /// Once the body is ready, this function is called
  Ext.onReady(function()  {
      var logger   = lbExt.logger({active: messages, level: LOG_INFO, style: { height: 300 }});
      var provider = new DataProvider(logger);

      /// Create view port
      var view = null, msg = null, elt = null;
      try {
	view = lbExt.create('Ext.Viewport', {
	  id:                 id, autoScroll: true,
	      padding:        '6px',
	      //layout:         'border',
	      layout:         'vbox',
	      fontSize:       '1em',
	      defaults:     { bodyStyle:    'padding: 0px', width: '100%' },
	      items: [{ id: 'hlt2-header',
		  region:   'north',
		  minHeight:   150,
		  autoScroll:  true,
		  items:    lbExt.standardHeaderItems({id: id, 
			left:   { title: lbExt.lhcb_online_picture()+'&nbsp;<B>'+system+' Page1</B>',
			  onclick: lhcb.constants.urls.lhcb.home_page.src,
			  tooltip: 'LHCb experiment:<br>General status information of<ul>'+
			  '<li> the LHC collider and</li>'+
			  '<li> the LHCb experiment</li></ul>'+
			  'Click to go to the experiment home page'},
			provider: provider})
		  },
		{   flex:       5,
                    autoScroll: false,
		    minHeight:  150,
		    height:   '100%',
		    layout: {   type: 'hbox' },
		    items:[ {
			layout:   {type: 'vbox' , defaults: { split: true } },
			flex:     6,
			height:   '100%',
			items:    [
				   lbExt.LHCSummary( {id: 'page1_lhc_1', 
					 onclick:  function() { document.location = lhcb.constants.operations_url("LHC1"); },
					 tooltip:  'Comments from the LHC operator<br>&nbsp;<br>'+
					 'Click to move to LHC Page 1.',
					 provider:   provider,
					 scrollable: { direction: 'vertical' },
					 minHeight:  150,
					 flex:       8}),
				   /*
				   lbExt.LHCComment( {id: 'page1_lhc_2',
					 onclick:  function() { document.location = lhcb.constants.operations_url("LHCCOORD"); },
					 tooltip:  'Comments from the LHC operator<br>&nbsp;<br>'+
					 'Click to move to LHC coodination planning page.',
					 provider:   provider, 
					 scrollable: { direction: 'vertical' },
					 minHeight:  100,
					 flex:       3}),
				   */
				   lbExt.planOfTheDay( {id: 'page1_lhcb_plan',
					 onclick:  function() { document.location = lhcb.constants.urls.lhcb.lhcb_run_status.src; },
					 tooltip:  'LHCb plan of the day<br>&nbsp;<br>'+
					 'Click to see the LHCb run control status.',
					 provider:   provider, 
					 scrollable: { direction: 'vertical' },
					 minHeight:  100,
					 flex:       5})
				   ]
			},
		      lbExt.page1Summary(system, {id: 'page1-lhcb',
			    onclick:  function() { document.location = lhcb.constants.urls.lhcb.detstatus.src; },
			    tooltip:  'LHCb detector information<br>&nbsp;<br>Click to see LHCb detector hardware status.',
			    scrollable: { direction: 'vertical' },
			    provider:   provider,
			    height:     '100%',
			    minHeight:  200,
			    flex:       10}),
		      ]
		    },
		{   flex:       3,
		    autoScroll: false,
		    layout:   {type: 'vbox' , defaults: { split: true } },
		    items:    [
			       lbExt.LHCb_shift_comments({id: 'page1-elog', 
				     onclick:   function() { document.location = lhcb.constants.urls.lhcb.elog.src; },
				     tooltip:   'The last entries from elog (LHCb)<br>&nbsp;<br>'+
				     'Click to see full elog<br>in the electrinic logbook.',
				     provider:  provider,
				     scrollable: { direction: 'vertical' },
				     minHeight:  150,
				     maxHeight:  350,
				     flex:       3}),
			       lbExt.LHCb_copyright({ minHeight: 30, width: '100%' }),
			       logger]
		    }
		],
	      renderTo: Ext.getBody()
	      });
	/// Attach these to the view in case we need them later....
	view.dataprovider = provider;
	view.logger = logger;
      }
      catch(e)  {
	msg = 'Ext:'+Ext.version+'\n\nException: '+e;
	console.error('[EXCEPTION] ' + msg);
      }

      /// Initialize tool tips
      Ext.QuickTips.init();
      /// Apply zoom depending on the screen size and the browser:
      lbExt.initZoom({fontSize: font_size});
      /// Start the comet engine
      provider.start();
    });
};

var page1_new_unload = function()  {
  dataProviderReset();
};

var page1_new_body = function()  {
  var msg  = the_displayObject['messages'];
  var sys  = the_displayObject['system'];
  var siz  = the_displayObject['size'];
  return lhcb_page1('lhcb_page1_main', sys == null ? 'LHCb' : sys, msg, siz);
};

console.info('Script lhcb.display.page1.cpp loaded successfully');
