//==========================================================================
// LHCb Web Status displays
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For details please see       Online/Stomp/README
// For the licensing terms see  Online/Stomp/LICENSE
//
// Author     : M.Frank
//
//==========================================================================

var lhcb = null;
var _lhcb = function() {
  if ( lhcb == null ) lhcb = new Object();
  return lhcb;
};
/// Flag to determine specialities for internet explorer
var _isInternetExplorer = function() 
{  return navigator.appName == "Microsoft Internet Explorer"; };

/// Flag to indicate debugging the loading of scripts
var _debugLoading = false;

/// Function to determine if the browser is only emulated and has no window structure
var _emulateBrowser = function()
{  return navigator.appCodeName == 'Envjs';                   };

/// Load static script using an absolute path
_loadStatic = function(name)
{  document.write('<SCRIPT type="'+_javascriptType+'" src="'+name+'"></SCRIPT>');   };

/// Load script using the absolute path from a base
var _loadScriptAbs = function(base,name) { _loadStatic(base+'/'+name);     };
/// Load script using the base path of the containing document
var _loadScript    = function(name)      { _loadScriptAbs(_fileBase,name); };

/// Load file by type
function _loadFileAbs(base, filename, filetype)   {
  // this somehow does not work!!!!
  if (filetype=="cpp"){ //if filename is a external JavaScript file
    var fileref=document.createElement('script');
    fileref.setAttribute("type",_javascriptType);
    fileref.setAttribute("src", base+'/'+filename+'.'+filetype);
  }
  else if (filetype=="js"){ //if filename is a external JavaScript file
    var fileref=document.createElement('script');
    fileref.setAttribute("type",_javascriptType);
    fileref.setAttribute("src", base+'/'+filename+'.'+filetype);
  }
  else if (filetype=="css"){ //if filename is an external CSS file
    var fileref=document.createElement("link");
    fileref.setAttribute("rel", "stylesheet");
    fileref.setAttribute("type", "text/css");
    fileref.setAttribute("href", base+'/Style/'+filename+'.css');
  }
  if (typeof fileref!="undefined")
    document.getElementsByTagName("head")[0].appendChild(fileref);
};

/// Load mime-type file using the base path of the containing document
function _loadFile(filename, filetype)   {  _loadFileAbs(_fileBase,filename,filetype); };

// Load basic stuff
_loadScript('lhcb.tools.cpp');
_loadScript('lhcb.display.tooltips.cpp');
_loadScript('lhcb.display.constants.cpp');

if ( !window._have_extjs )  {
  _loadScript('lhcb.display.logger.cpp');
}
else  {
  //_loadScriptAbs(_lhcbScriptBase,'lhcb.display.ext.logger.cpp');
}

/** Setup data transport protocol used for this web-page
 *
 *  There are 2 possibilities:
 *  1) (deprecated) stomp over orbited.
 *  2) Paho mqtt to talk directly to the activeMQ relay on the web-server
 *
 *  Depending on the value of the variable _transport_in_use
 *  the corresponding transport protocol implementations are loaded.
 *
 */
if ( _transport_in_use=='stomp' )  {
  var TCPSocket = Orbited.TCPSocket;
  //var org_transport = Orbited.util.chooseTransport;
  //Orbited.util.chooseTransport = function() {
  //  return Orbited.CometTransports.LongPoll;
  //};
  _loadStatic('/static/protocols/stomp/stomp.js');
  _loadScriptAbs(_lhcbScriptBase,'lhcb.display.data.stomp.cpp');
}
else if ( _transport_in_use == 'stomp-ws' )  {
  _loadScriptAbs(_lhcbScriptBase,'stomp.web-socket.js');
  _loadScriptAbs(_lhcbScriptBase,'lhcb.display.data.stomp.web-socket.cpp');
}
else if ( _transport_in_use == 'amq' )  {
  _loadScriptAbs(_lhcbScriptBase,'lhcb.display.data.amq.cpp');
}
else if ( _transport_in_use == 'mqtt' )  {
  _loadScriptAbs(_lhcbScriptBase,'paho-mqttws31.cpp');
  _loadScriptAbs(_lhcbScriptBase,'lhcb.display.data.mqtt.cpp');
}

/** Setup the invocation trampolin to load the individual web pages from
 *  a single html file.
 *
 *  Depending on the arguments the corresponding java script code to produce the 
 *  web page is loaded and the default routines are setup.
 *
 */
var display_type = function(opts)   {
  this.type   = null;
  this.header = null;
  this.body   = null;
  var url     = document.location.toString();
  var pars    = url.split('?');

  this.header = function() {
    var msg = 'The URL\n'+url+'\nis not a valid display URL!\n';
    alert(msg);
  };
  this.body   = function() {  };
  this.unload = function() { alert('Bye, Bye my friend....'); };

  for( o in opts ) {
    this[o] = opts[o];
  }
  if ( pars.length > 0 || this.type != null )  {
    var disp_func = '';
    if ( pars.length > 1 )   {
      var p = pars[1].split('&');
      this.params = p;
      for ( var i=0; i < p.length; ++i ) {
	var v = p[i].split('=');
	this[v[0]] = v[1];
      }
    }
    this.url_base = pars[0];
    if ( this.type != null ) {
      eval("this.header = function()     { _loadScriptAbs(_fileBase,'lhcb.display."+this.type+".cpp'); }");
      eval("this.body   = function()     { return "+this.type+"_body(); }");
      eval("this.unload = function()     { return "+this.type+"_unload(); }");
    }
  }

  this.dump = function() {
    var text = '';
    for( p in this) {
      text += p.toString() + '=' + this[p] +'\n';
    }
    alert('Dump:'+text);
  };
};

// The main display creation object:
var the_displayObject = new display_type(_the_displayObject_opts || {});
the_displayObject.header();

// All done....
if ( _debugLoading ) alert('Script lhcb.display.select.cpp loaded successfully');
