//==========================================================================
// LHCb Web Status displays
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For details please see       Online/Stomp/README
// For the licensing terms see  Online/Stomp/LICENSE
//
// Author     : M.Frank
//
//==========================================================================

/** @class DataItem
 *
 * Simple scripting class to place streaming data items into an html file.
 *
 *  @author  M.frank
 *  @version 1.0
 */
var DataItem = function(provider, name)   {
  this._name  = name;
  this._elem = null;
  provider.subscribe(this._name,this);
  
  /// Late access of the document's element
  this.element = function() {
    if ( this._elem == null )   {
      this._elem = document.getElementById(this._name);
      if ( this._elem == null ) {
	alert('Invalid document:'+document.location+'\n\nNo element found with ID:'+this._name+'\n');
      }
    }
    return this._elem;
  };
  
  /// Default callback for dataprovider on feeding data
  this.set = function(data) {
    var i, e=this.element();
    for(i=e.childNodes.length-1; i>=0; --i)
      e.removeChild(e.childNodes[i]);
    e.innerHTML = data;
  };
};

/** @class ElementItem
 *
 * Simple scripting class to place streaming data items into an html file.
 *
 *  @author  M.frank
 *  @version 1.0
 */
var ElementItem = function(provider, name, fmt, element)   {
  e = element;
  e._name  = name;
  e._format = fmt;
  provider.subscribe(e._name,e);
  
  /// Default callback for dataprovider on feeeding data
  e.set = function(data) {
    var i, item_data = 'Unknown';
    if ( this._format != null ) {
      if ( data[0] == 21 )        // Integer
	item_data = sprintf(this._format,parseInt(data[1]));
      else if ( data[0] == 22 )   // Float
	item_data = sprintf(this._format,parseFloat(data[1]));
      else if ( data[0] == 25 )   // String
	item_data = sprintf(this._format,data[1]);
      else
	item_data = data[1];
    }
    else {
      item_data = data[1];
    }
    for(i=this.childNodes.length-1; i>=0; --i)
      this.removeChild(this.childNodes[i]);
    this.innerHTML = item_data;
    item_data = null;
  };
  return e;
};

var RawItem = function(provider, name, fmt, element)   {
  e = element;
  e._name  = name;
  provider.subscribe(e._name,e);
  
  /// Default callback for dataprovider on feeeding data
  e.set = function(data) {
    var i, s = ''+data;
    for(i=this.childNodes.length-1; i>=0; --i)
      this.removeChild(this.childNodes[i]);

    this.innerHTML = s.replace("<", "&lt;", "g").replace(">", "&gt;", "g").replace(" ", "&nbsp;", "g").replace("\n", "<br></br>", "g");
    s = null;
  };
  return e;
};

_lhcb().setup = function(show_log) {
  var body = document.getElementsByTagName('body')[0];
  lhcb.data = new Object();
  lhcb.logWindow = document.createElement('div');
  lhcb.logWindow.id = 'LHCb_LogWindow_std';
  body.appendChild(lhcb.logWindow);
  body.onunload = function() { dataProviderReset(); };

  lhcb.data.logger   = new OutputLogger(lhcb.logWindow, -1, LOG_INFO, 'RunStatusLogger');
  lhcb.data.provider = new DataProvider(lhcb.data.logger);
  lhcb.data.provider.topic = '/topic/farm';
  lhcb.data.stomp    = new Object();
  lhcb.data.stomp.scanDocument = function() {
    var elts = document.getElementsByTagName('STOMP');
    var items = new Array();
    var provider = lhcb.data.provider;
    var e = null, item = null, fmt = null;
    items.length = elts.length;
    for (var i=0; i<elts.length;++i) {
      e = elts[i];
      item = e.getAttribute('data');
      fmt  = e.getAttribute('format');
      items[i] = ElementItem(provider,item,fmt,e.parentNode);
    }
    lhcb.data.items = items;
    elts = document.getElementsByTagName('DIM');
    var raw_items = new Array();
    raw_items.length = elts.length;
    for (var i=0; i<elts.length;++i) {
      e = elts[i];
      item = e.getAttribute('data');
      fmt  = e.getAttribute('format');
      raw_items[i] = RawItem(provider,item,fmt,e.parentNode);
    }
    lhcb.data.raw_items = raw_items;
    e = item = fmt = raw_items = null;
    provider.start();
  };
  return lhcb;
};

if ( _debugLoading ) alert('Script lhcb.display.data.raw.cpp loaded successfully');
