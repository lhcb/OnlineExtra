//==========================================================================
// LHCb Web Status displays
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For details please see       Online/MQTT/README   ---- CHANGE FOLDER NAME, CL 27/07
// For the licensing terms see  Online/MQTT/LICENSE  ---- CHANGE FOLDER NAME, CL 27/07
//
// Author     : M.Frank
//
//==========================================================================
if ( null == _dataLogger )  {

  var _dataLogger     = null;

  var LOG_ERROR       = 0;
  var LOG_WARNING     = 1;
  var LOG_INFO        = 2;
  var LOG_DEBUG       = 3;
  var LOG_VERBOSE     = 4;

  _loadScriptAbs(_lhcbScriptBase,'lhcb.tools.cpp');

  /**@class OutputLogger
   *
   *
   *  @author  M.frank
   *  @version 1.0
   */
  var OutputLogger = function(panel, len, level, opts)  {
    this.lines        = null;
    if ( len>0 ) {
      this.lines = new Array();
      for(var i=0; i<len; ++i) this.lines.push('');
    }
    this.panel  = panel;
    this.length = len;
    this.level  = level;
    this.curr   = 0;

    _dataLogger = this;

    this.error = function(msg)     {  return this.print(LOG_ERROR,msg);     };
  
    this.info = function(msg)      {  return this.print(LOG_INFO,msg);      };
  
    this.warning = function(msg)   {  return this.print(LOG_WARNING,msg);   };
  
    this.debug = function(msg)     {  return this.print(LOG_DEBUG,msg);     };

    this.verbose = function(msg)   {  return this.print(LOG_VERBOSE,msg);   };

    this.print = function(level, msg) {
      if ( console && console.log )   {
	var d = new Date().toLocaleString();
	if ( level == LOG_VERBOSE )
	  console.debug(d+' [VERBOSE]:  '+msg);
	else if ( level == LOG_DEBUG )
	  console.log  (d+ ' [DEBUG]:    '+msg);
	else if ( level == LOG_INFO )
	  console.info (d+ ' [INFO]:     '+msg);
	else if ( level == LOG_WARNING )
	  console.warn (d+ ' [WARNING]:  '+msg);
	else if ( level == LOG_ERROR )
	  console.error(d+' [ERROR]:    '+msg);
	else
	  console.warn (d+ ' [UNKNOWN]:  '+msg);
      }
      if ( this.lines != null && level >= this.level && this.length>0 ) {
	if ( this.lines.length < this.length ) {
	  this.lines.length = this.lines.length+1;
	}
	if ( this.curr>this.lines.length-1 ) this.curr = 0;
	if ( level == LOG_VERBOSE ) {
	  this.lines[this.curr] = this.format(d+' [VERBOSE]:  '+msg);
	}
	else if ( level == LOG_DEBUG ) {
	  this.lines[this.curr] = this.format(d+' [DEBUG]:    '+msg);
	}
	else if ( level == LOG_INFO ) {
	  this.lines[this.curr] = this.format(d+' [INFO]:     '+msg);
	}
	else if ( level == LOG_ERROR ) {
	  this.lines[this.curr] = this.format(d+' [ERROR]:    '+msg);
	}
	else  {
	  this.lines[this.curr] = this.format(d+' [UNKNOWN]:  '+msg);
	}
	this.showMessages();
	this.curr = this.curr + 1;
      }
      d = null;
      return this;
    };

    this.format = function(expr) {
      var s = prettyprint(expr);
      s = htmlescape(s);
      return s;
    };
  
    var htmlescape = function(expr) {
      var s = expr.replace("&", "&amp;", "g");
      s = s.replace("<", "&lt;", "g");
      s = s.replace(">", "&gt;", "g");
      s = s.replace(" ", "&nbsp;", "g");
      s = s.replace("\n", "<br></br>", "g");
      return s;
    };

    var prettyprint = function(s) {
      var q = "(";
      if(typeof(s) == "string") return s;
      for (var i=0; i<s.length; i++) {
	if (typeof(s[i]) != "object")
	  q += s[i];
	else
	  q += prettyprint(s[i]);
	if (i < s.length -1)
	  q += " ";
      }
      q += ")";
      return q;
    };

    this.showMessages = function() {
      if ( this.lines && this.lines.length > 0 ) {
	var message = '<pre>';
	for(var i=this.curr+1; i<this.lines.length; ++i)  {
	  if ( this.lines[i].length > 0 )
	    message += '&rarr; ' + i + ': ' + this.lines[i] + '<br></b>';
	}
	for(var i=0; i<=this.curr; ++i)  {
	  message += '&rarr; ' + i + ': ' + this.lines[i] + '<br></b>';
	}
	message = message + '</pre>';
	this.panel.setHtml(message);
	message = null;
      }
      return this;
    };

    this.hide = function() {
      this.panel.collapse(Ext.Component.DIRECTION_BOTTOM);
    };
  
    this.show = function() {
      this.showMessages();
      this.panel.expand(true);
    };
  
    this.clear = function() {
      if ( this.lines )   {
	for(var i=0; i<=this.lines.length; ++i)
	  this.lines[i] = '';
	this.curr         = 0;
	this.showMessages();
      }
    };
    return this;
  };

  if ( _debugLoading ) alert('Script lhcb.display.logger.cpp loaded successfully');
 }
