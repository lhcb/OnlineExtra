//==========================================================================
// LHCb Web Status displays
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For details please see       Online/Stomp/README
// For the licensing terms see  Online/Stomp/LICENSE
//
// Author     : M.Frank
//
//==========================================================================

_loadFile('lhcb.display.ext','css');
_loadFile('lhcb.display.ext.fsm','css');
_loadScript('lhcb.display.zoom.cpp');
_loadScript('lhcb.display.extWidgets.cpp');
_loadScript('lhcb.display.extPanels.cpp');

/// Main entry point to create the 'LHCb Magnet Status' web page
/*
 *    \author  M.Frank
 *    \version 2.0
 */
var lhcb_magnet_status = function(id, messages, font_size) {

  // Update window title
  lbExt.setWindowTitle('Magnet Status Monitor');

  /// Once the body is ready, this function is called
  Ext.onReady(function()  {
      var app = lbExt.createApp({title: 'VELO Motion Monitor',
	    messages: messages,
	    fontSize: font_size});
      /// Create view port and add elements
      try  {
	app.run({id:          id,
	      items: [{ items: lbExt.standardHeaderItems({id: id+'-magnet-status-header', 
		      left:   { title: lbExt.lhcb_online_picture()+'&nbsp;<B>Magnet Status Monitor</B>',
			onclick: lhcb.constants.urls.lhcb.home_page.src,
			tooltip: 'Magnet status monitor:<br>&nbsp;<br>'+
			'General status information of<br>'+
			'the LHCb magnet'},
		      right:  { title: lbExt.lhcb_logo(lhcb.constants.images.magnet_big.src) },
		      provider: app.provider})
		  },
		{   items:[{html: '&nbsp;', bodyCls: 'Bg-lhcblight'},
			   lbExt.magnetStatus({id: id+'-magnet-status',
				 tooltip:  'Status information about the LHCb magnet',
				 provider: app.provider})
		      ]
		    }
		]
	      });
      }
      catch(e)  {
	app.logger.error('[EXCEPTION] Ext:'+Ext.version+'\n\nException: '+e);
      }
    });
};

var magnet_new_unload = function()  {
  dataProviderReset();
};

var magnet_new_body = function()  {
  var msg  = the_displayObject['messages'];
  var siz  = the_displayObject['size'];
  return lhcb_magnet_status('lhcb_magnet_status_main', msg, siz);
};

console.info('Script lhcb.display.magnet_new.cpp loaded successfully');
