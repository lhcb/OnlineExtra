//==========================================================================
// LHCb Web Status displays
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For details please see       Online/MQTT/README   ---- CHANGE FOLDER NAME, CL 27/07
// For the licensing terms see  Online/MQTT/LICENSE  ---- CHANGE FOLDER NAME, CL 27/07
//
// Author     : M.Frank
//
//==========================================================================

var _dataProvider   = null;

/** @class DataProvider
 *
 *  @author  M.Frank
 *  @version 1.0
 */
var DataProvider = function(logger)  {
  this.calls = new Array();
  this.items = new Object();
  this.calls.length   = 0;
  this.logger         = logger;
  this.isConnected    = false;
  this.needConnection = false;
  this.topic          = '/status';
  this.command        = '/topic/clientCommands';
  this.pageName       = 'dataPage';
  this.service        = null;
  _dataProvider       = this;

  if ( the_displayObject ) this.pageName=the_displayObject.type;

  this.reconnectHandler = function() {
    _dataProvider.logger.info('Starting reconnect timer');
    if ( !_dataProvider.isConnected && this._dataProvider.needConnection ) {
      _dataProvider.reconnect();
      setTimeout(_dataProvider.reconnectHandler,10000);
    }
  };


  /// Connect to item topics
  this.onConnect = function() {  
    this.logger.info("Connected ..");
    this.isConnected = true;
    this.logger.info("Connecting all pending data leaves to services ..");
    this.service.subscribe(this.command,{});
    for (var i=0; i < this.calls.length; ++i) {
      var svc = this.calls[i];
      var msg = 'SUBSCRIBE:/'+this.calls[i];//.replace(/\./g,'/');
      if ( svc.substring(0,7)=='/topic/' ) svc = svc.substring(7);
      else if ( svc.substring(0,1) != '/' ) svc = '/'+svc;
      svc = svc.replace(/\./g,'/');
      this.logger.info('Subscribe MQTT service:'+svc);
      //this.service.subscribe(svc,this.subscribeOpts);
      this.service.subscribe(svc);
      var message = new Messaging.Message(msg);
      message.destinationName = this.topic;
      this.service.send(message);
      message = null;
      svc = null;
    }
  };

  this.onError = function(error) {
    // display the error's message header:
    alert(error.headers.message);
  };


  /** Connect to mqtt channel
   *
   *  @return  Reference to self
   */
  // Added-Modified CL
  this.start = function() {
    var clientid = 'ID_'+parseInt(Math.floor((Math.random() * 100000000000000000000) + 1));
    try {
      var b, l, s = ''+document.location, d = new Date();
      s = s.replace(/type=/g,'');
      if ( (b=s.lastIndexOf('/')+1) < 1 ) b = 0;
      if ( (l=s.indexOf('&'))       < 0 ) l = s.length;
      // Added CL, 25/07/2017 - Max number of characters for 
      var clid = get_ip_address(); // IPv4, max 15characters
      clientid = 'ID_'+clid+'_'+parseInt(Math.floor((Math.random() * 10000) + 1));
      clid = null;
    }
    catch (e) {
    }
    this.host    = location.hostname;
    var url = url = ["wss://", host, ":/stomp"].join("");
    this.service = Stomp.client(url);
    this.service.connect('guest', 'guest', this.onConnect, this.onError);
    this.logger.info("Created STOMP web-socket client....");
    clientid = null;


    /// Callback when the connection is lost
    this.service.onConnectionLost = function(responseObject)    {
      _dataProvider.isConnected = false;
      if (responseObject.errorCode !== 0) {
	_dataProvider.logger.info("Transport closed (code: " + responseObject.errorCode + "," + responseObject.errorMessage + ")");
      }
      _dataProvider.logger.info("Reconnect flag:"+_dataProvider.needConnection);
      if ( _dataProvider.needConnection ) {
	_dataProvider.logger.info('Starting reconnection timer');
	setTimeout(_dataProvider.reconnectHandler,10000);
      }
    };
    /// Data dispatch callback once mqtt receives data
    this.service.onMessageArrived = function(frame) {
      var i, d, o, len, v = frame.payloadString.split('#');
      if ( v.length >= 2 ) {
	var itm  = v[1];
	var data = v.slice(2);
	v = null;
	if ( data != 'DEAD' ) {
	  o = _dataProvider.items[itm];
	  len = o.length;
	  if ( !o.prev_data ) {
	    o.prev_data = data;
	  }
	  else if ( o.prev_data+'' == data+'' ) {
	    _dataProvider.logger.info('Ignore: [' +frame.payloadString.length+' bytes] '+itm+'='+o.prev_data);
	    return;
	  }
	  o.prev_data = data;
	  for(i=0; i<len; ++i) {
	    if ( o[i] ) {
	      o[i].set(data);
	    }
	    else {
	      alert('Debug: Dead element: '+itm+'['+i+'] out of '+len);
	    }
	  }
	  d = new String(data);
	  if (d.length > 10) d = d.substr(0,10);
	  _dataProvider.logger.info("Update: [" +frame.payloadString.length+' bytes] '+itm+'='+d+' prev:'+o.prev_data);
	  d = o = item = data = null;
	}
	return;
      }
      else if ( frame.payloadString == this.parent.pageName+':reload' ) {
	this.parent.reset();
	window.location.replace(document.location);
      }
      else if ( frame.payloadString == 'allPages:reload' ) {
	this.parent.reset();
	window.location.replace(document.location);
      }
      else if ( frame.payloadString.substring(0,this.parent.pageName.length+5) == this.parent.pageName+':url:' ) {
	this.parent.reset();
	window.location.assign(frame.payloadString.substring(this.parent.pageName.length+5));
      }
      _dataProvider.logger.error('onmessage: retrieved data with invalid item number');
    };
    try {
      this.connectOpts['hosts'] = [this.host,this.host,this.host,this.host,this.host,this.host,this.host];
      this.connectOpts['ports'] = [this.port,this.port,this.port,this.port,this.port,this.port,this.port];
      this.service.connect(this.connectOpts);
    }
    catch (e) {
      alert('Exception: MQTT connect:'+e);
    }
    this.needConnection = true;
    this.logger.info("Connecting MQTT client....Done"); 
    host = null;
    port = null;
    return this;
  };

  /** Disconnect from mqtt channel
   *
   *  @return  Reference to self
   */
  this.reset = function()  {
    if ( this.isConnected ) {
      if ( this.service )  {
	delete this.service;
      }
      this.isConnected = false;
      this.service = null;
    }
    return this;
  };

  this.reconnect = function() {
    this.isConnected = true;
    this.reset();
    this.start();
    return this;
  };

  /** Pre-Subscribe to data items
   *  @param item      mqtt topic to subscribe to. Must be an object with a "name" property.
   *
   *  @return  Reference to self
   */
  this.subscribeItem = function(item)  {
    return this.subscribe(item.name,item);
  };


  /** Pre-Subscribe to data items
   *  @param item      Name of mqtt topic to subscribe to
   *  @param callback  Object implementing "set" method when new data is received.
   *
   *  @return  Reference to self
   */
  this.subscribe = function(item,callback)  {
    var svc, msg, len = this.calls.length;

    this.calls.length = this.calls.length+1;
    this.calls[len] = item;

    if ( !this.items.hasOwnProperty(item) )
      this.items[item] = new Array(callback);
    else
      this.items[item].push(callback);
    this.logger.debug('DataProvider: Subscribed to data item:'+item+'   '+this.service);
    if ( this.isConnected && this.service )   {
      svc = this.calls[len];
      msg = 'SUBSCRIBE:/'+svc;
      if ( svc.substring(0,7)=='/topic/' ) svc = svc.substring(7);
      else if ( svc.substring(0,1) != '/' ) svc = '/'+svc;
      svc = svc.replace(/\./g,'/');
      this.logger.info('Subscribe MQTT service:'+svc);
      this.service.subscribe(svc);
      var message = new Messaging.Message(msg);
      message.destinationName = this.topic;
      this.service.send(message);
      message = null;
    }
    svc = msg = len = null;
    return this;
  };

  /** Unubscribe to data items
   *  @param item      Name of mqtt topic to subscribe to
   *
   *  @return  Reference to self
   */
  this.unsubscribe = function(item)  {
    if ( this.isConnected && this.service ) {
      this.service.unsubscribe(this.command,{});
      for(var i=0; i<this.calls.length;++i) {
	if ( this.calls[i] == item ) {
	 this.service.unsubscribe(item,{});
	 delete this.calls[i];
	 this.calls.length = this.calls.length-1;
	 return this;
	}
      }
    }
    else {
      this.unsubscribeAll();
    }
    this.service = null;
    return null;
  };

  /** Disconnect from all item topics
   *
   *  @return  Reference to self
   */
  this.unsubscribeAll = function()  {
    this.logger.info("Disconnect all pending data services ..");
    if ( this.isConnected && this.service ) {
      this.service.unsubscribe(this.command,{});
      for(var i=0; i<this.calls.length;++i) {
	var item = this.calls[i];
	this.service.unsubscribe(item,{});
	item = null;
      }
    }
    this.calls = new Array();
    this.calls.length = 0;
    this.items = new Object();
    return this;
  };
  /** Update all data items by requesting a "SUBSCRIBE:<item> call to the server
   *
   *  @return  Reference to self
   */
  this.update = function() {
    if ( this.service )  {
      for (var i=0; i < this.calls.length; ++i)  {
	var msg = 'SUBSCRIBE:/'+this.calls[i];//.replace(/\./g,'/');
	var message = new Messaging.Message(msg);
	message.destinationName = this.topic;
	this.service.send(message);
	this.logger.verbose('DataProvider: Connect data item:'+msg);
	msg = message = null;
      }
    }
    return this;
  };

  /// Disconnect to item topics
  this.disconnect = function()  {
    this.unsubscribeAll();
    if ( this.service )  {
      this.service.disconnect();
      this.service.reset();
    }
  };

  /// Since long running documents have memory leaks, we reload the page regularly to avoid them.
  this.reloadHandler = function() {
    //window.location.reload();
  };

  // Setup subscrtion options for ActiveMQ
  this.connectOpts = {};
  this.connectOpts['useSSL'] = true;
  this.connectOpts.onSuccess = function() {
    _dataProvider.onConnect();  
  };
  this.connectOpts.onFailure = function(error) {
    var msg = 'MQTT: OnFailure: Error No. '+error.errorCode+'\n['+ error.errorMessage+']';
    _dataProvider.logger.error(msg);
    alert(msg);
    setTimeout(_dataProvider.reconnectHandler,1000);
  };

  setTimeout(_dataProvider.reloadHandler,3600000);
  return this;
};

var dataProviderReset = function() {
  if ( null != _dataProvider ) {
    /*
    // Commented out CL
    _dataProvider.needConnection = false;
    */
    _dataProvider.unsubscribeAll();
    _dataProvider.isConnected = true;
    _dataProvider.reset();
  }
};

if ( _debugLoading ) alert('Script lhcb.display.data.mqtt.cpp loaded successfully');
