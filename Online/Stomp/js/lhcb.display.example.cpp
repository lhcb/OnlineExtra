//==========================================================================
// LHCb Web Status displays
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For details please see       Online/Stomp/README
// For the licensing terms see  Online/Stomp/LICENSE
//
// Author     : M.Frank
//
//==========================================================================

_loadFile('lhcb.display.general','css');
_loadFile('lhcb.display.ext.fsm','css');
_loadScript('lhcb.display.zoom.cpp');
_loadScript('lhcb.display.extWidgets.cpp');

var Page1 = function(siz, msg, sys) {
  var table = document.createElement('div');
  table.id = 'main_lbcomet_element';
  table.font_size = siz;
  table.messages  = msg;
  table.system    = sys;
  table.provider  = null;
  table.logger    = null;

  table.build = function()  {
    Ext.onReady(function()  {
	var logPanel = lbExt.create('Ext.panel.Panel',{
	  renderTo:     Ext.getBody(),
	      style: { height: 300 },
	      bodyStyle: {  padding: 12,
		fontweight: 'bold',
		textalign: 'left',
		},
	      title:            'Comet and MQTT script debugging messages',
	      html:             'Buffer for debug messages from JavaScript',
	      visible: false,
	      frame:            true,
	      animCollapse:     true,
	      autoScroll:       true,
	      autoHeight:       true,
	      autoWidth:        true,
	      closable:         false,
	      collapsed:        true,
	      collapsible:      true,
	      hideCollapseTool: false,
	      collapseFirst:    true,
	      width:            '100%',
	      height:           300
	      });
	table.logDisplay = logPanel;
	if ( table.messages > 0 )
	  table.logger = new OutputLogger(table.logDisplay, 200, LOG_INFO, {});
	else
	  table.logger = new OutputLogger(table.logDisplay,  -1, LOG_INFO, {});
	lbExt.logger = table.logger;
	table.provider = new DataProvider(table.logger);
	if ( !table.messages ) logPanel.hide();

	/// Create view port
	var view = null, msg = null, elt = null;
	try {
	  view = lbExt.create('Ext.Viewport', {
	    layout: 'border',
		fontSize: '1em',
		defaults:     {
	      margins:      '15 15 15 15',
		  cmargins:     '3 3 3 3',
		  autoHeight:   false,
		  border:       false
		  },
		items: [{ id: 'page1-frame',
		    region:   'north',
		    renderTo: Ext.getBody(),
		    items:    [lbExt.pageHeader(sys,{ id: 'lhcb-page1',
			  time:   true,
			  left: { title: lbExt.lhcb_online_picture()+'&nbsp;<B>'+sys+' Page1</B>',
			    onclick: lhcb.constants.urls.lhcb.home_page.src,
			    tooltip: 'LHCb page1:<br>General status information of<br>'+
			    '-- the LHC collider and<br>'+
			    '-- the LHCb experiment<br>'+
			    'Click to go to home page'}
		      }),
		      lbExt.detectorHeader(sys, {id: 'page1-runinfo', provider: table.provider, width: '100%'})]
		    },
		  {   region:   'center',
		      renderTo: Ext.getBody(),
		      autoScroll: true,
		      layout: { type: 'hbox' },
		      items:  [	 {   autoScroll: true,
			  height: '100%',
			  flex: 3,
			  renderTo: Ext.getBody(),
			  layout:     { type: 'vbox' , defaults: { split: true } },
			  items:    [lbExt.LHCSummary( {id: 'xpage1-lhc1', provider: table.provider, flex: 8}),
				     lbExt.LHCComment( {id: 'xpage1-lhc2', provider: table.provider, flex: 5})]
			  },
			lbExt.page1Summary(sys, {id: 'page1-lhcb', provider: table.provider, flex: 5}),
			]
		      },
		  /*
		  {   region:   'west',
		      autoScroll: true,
		      width:  '35%',
		      height: '50%',
		      renderTo: Ext.getBody(),
		      layout:     { type: 'vbox' ,
			defaults: { split: true }
		    },
		      items:    [lbExt.LHCSummary( {id: 'page1-lhc1', provider: table.provider, flex: 8}),
				 lbExt.LHCComment( {id: 'page1-lhc2', provider: table.provider, flex: 5})]
		      },
		  */
		  { id:         'page1-elog',
		      region:   'south',
		      renderTo: Ext.getBody(),
		      items:    [lbExt.LHCb_shift_comments({provider: table.provider, height: '35%' /*, height: 280 */}),
				 lbExt.LHCb_copyright({}),
				 logPanel]
		      }
		  ],
		renderTo: Ext.getBody()
		});
	}
	catch(e)  {
	  msg = 'Ext:'+Ext.version+'\n\nException: '+e;
	}
	if ( msg ) alert(msg);
	/*
	// Tool tips and links to other pages:

	elt = document.getElementById('page1-runinfo');
	elt.tooltip = new Ext.ToolTip({ target:   elt.id, trackMouse: true,
	html: 'Current LHC fill and LHCb run<br>Click to show the run-control status page.'});
	elt.onclick = function() { document.location = lhcb.constants.urls.lhcb.lhcb_run_status.src;};
	
	elt = document.getElementById('page1-lhc1');
	elt.tooltip = new Ext.ToolTip({ target:   elt.id, trackMouse: true,
	html: 'Comments from the LHC operator<br>Click to move to LHC coodination planning page.'});
	elt.onclick = function() { document.location = lhcb.constants.operations_url("LHCCOORD"); };

	elt = document.getElementById('page1-lhc2');
	elt.tooltip = new Ext.ToolTip({ target:   elt.id, trackMouse: true,
	html: 'Comments from the LHC operator<br>Click to move to LHC coodination planning page.'});
	elt.onclick = function() { document.location = lhcb.constants.operations_url("LHCCOORD"); };

	elt = document.getElementById('page1-lhcb');
  	elt.tooltip = new Ext.ToolTip({ target:   elt.id, trackMouse: true,
	html: 'LHCb detector information<br>Click to see LHCb detector hardware status.'});
	elt.onclick = function() { document.location = lhcb.constants.urls.lhcb.detstatus.src; };

	elt = document.getElementById('page1-elog');
	elt.tooltip = new Ext.ToolTip({ target:   elt.id, trackMouse: true,
	html: 'The last entries from elog (LHCb)<br>Click to see full elog.'});
	elt.onclick = function() { document.location = lhcb.constants.urls.lhcb.elog.src; };
	*/

	Ext.QuickTips.init();
	lbExt.setWindowTitle(sys+' Page 1');
	if ( null == table.font_size && screen.width>2000 ) table.font_size = 3;
	if ( _isInternetExplorer() ) zoom_changeFontSizeEx(2);
	else  zoom_changeFontSizeEx(0);
	if ( table.font_size != null ) zoom_changeFontSizeEx(table.font_size);
	table.provider.start();
      });
    return table;
  };
  return table;
};

var page1_new_unload = function()  {
  dataProviderReset();
};

var page1_new_body = function()  {
  var msg  = the_displayObject['messages'];
  var sys  = the_displayObject['system'];
  var siz  = the_displayObject['size'];

  if ( sys == null ) sys = 'LHCb';
  return Page1(siz, msg, sys).build();
};

if ( _debugLoading ) alert('Script lhcb.display.status.cpp loaded successfully');
