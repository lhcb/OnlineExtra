//==========================================================================
// LHCb Web Status displays
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For details please see       Online/Stomp/README
// For the licensing terms see  Online/Stomp/LICENSE
//
// Author     : M.Frank
//
//==========================================================================
_loadFile('lhcb.display.ext','css');
_loadFile('lhcb.display.ext.fsm','css');
_loadScript('lhcb.display.zoom.cpp');
_loadScript('lhcb.display.extWidgets.cpp');
_loadScript('lhcb.display.extFSM.cpp');

var Hlt2 = function(page_id, messages, font_size) {
  /// Create the application object
  var app = lbExt.createApp({title: 'HLT2 Status Page',
	messages: messages,
	fontSize: font_size});
  try {
    var run_props = [{dp: 'general.runNumber',    title: 'Run number',                   colspan: 1},
		     {dp: 'Trigger.HLTType',      title: 'Trigger configuration',        colspan: 1},
		     {dp: 'general.runType',      title: 'Activity',                     colspan: 1},
		     {dp: 'HLTFarm.nSubFarms',    title: 'HLT: Number of Subfarms',      colspan: 1},
		     {dp: 'HLTFarm.architecture', title: 'HLT: Architecture',            colspan: 1},
		     {dp: 'HLTFarm.hltRate',      title: 'HLT: Accept Rate',             colspan: 1, format: '%.0f Hz'}
		     ];
    /// Create view port and attach panels to it
    app.run({id: page_id,
	  minHeight:         900,
	  minWidth:         1200,
	  items: [ { id: page_id+'_top_panel',
	      items:    lbExt.standardHeaderItems({id: page_id,
		    left: { title:    lbExt.lhcb_online_picture()+'&nbsp;<B>HLT2 Status Monitor</B>',
		      tooltip:  'HLT2 processing status<br>&nbsp;<br>'+
		      'Click to go to the LHCb home page',
		      onclick:        lhcb.constants.urls.lhcb.home_page.src},
		    provider: app.provider})
	      },
	    {   id:             page_id+'_central_panel',
		layout:     {   type: 'table', columns: 1, tableAttrs: { style: { width: '100%'}}},
		defaults:   {   autoscroll: true,  bodyCls: 'Bg-lhcblight', width: '99.9%', flex: 2 },
		items:[     {   minHeight: 225,
		    layout: {   type: 'hbox', defaults: { autoScroll: true, split: true } },
		    defaults: { autoScroll: true, maxHeight: 225, minHeight: 220 },
		    items:  [   lbFSM.simpleRunControl({partition:        'LHCb2',
			  title:            'LHCb2 run status summary',
			  provider:         app.provider,
			  logger:           app.logger,
			  trace:            true}),
		      lbExt.runInfoTable({partition: 'LHCb2', 
			    title: 'LHCb2 run properties',
			    properties:   run_props,
			    provider:     app.provider,
			    frame:        false,
			    flex: 1})]
		    },
		  {   minHeight: 175,
		      layout:   { type: 'hbox', defaults: { split: true }  } , 
		      defaults: { autoScroll: true, height: 120, width: '99.8%'},
		      items: [lbExt.hlt2RunsTablePanel({
			title:          'All runs with files to be processed',
			    datapoint:  'lbWeb.LHCb_RunInfoSplitHLT.RunsLeftCorr',
			    provider:   app.provider}),
			lbExt.hltDeferredStatus({ provider: app.provider,legend:   true})
			]
		      },
		  {   layout:     'fit',
		      items: [lbExt.hlt2RunsHistogramPanel({
			id:             page_id+'_current_runs',
			    autoScroll: true,
			    title:      'Current runs processing in HLT2',
			    datapoint:  'lbWeb.LHCb_RunInfoSplitHLT.FarmStatus.currRunsSummary',
			    minHeight:  200,
			    maxHeight:  370,
			    provider:   app.provider})
			]
		      } /* ,
			    {   layout: 'fit',
			    autoScroll: true,
			    scrollable: true,
			    items: [lbExt.hlt2RunsHistogramPanel({
			    id:             page_id+'_all_runs_queued',
			    autoScroll: true,
			    title:      'All runs with files to be processed in HLT2',
			    datapoint:  'lbWeb.LHCb_RunInfoSplitHLT.RunsLeftCorr',
			    collapsed:  true,
			    userUpdate: false,
			    maxHeight:  300,
			    provider:   app.provider})
			    ]
			    }, */ 
		  ]
		}
	    ]
	  });
  }
  catch(e)  {
    console.error('[EXCEPTION] '+e);
  }
};

var hlt2_new_unload = function()  {
  dataProviderReset();
};

var hlt2_new_body = function()  {
  var msg  = the_displayObject['messages'];
  var siz  = the_displayObject['size'];
  Hlt2('lhcb_hlt2_main', msg, siz);
};

if ( _debugLoading ) console.info('Script lhcb.display.hlt2.cpp loaded successfully');
