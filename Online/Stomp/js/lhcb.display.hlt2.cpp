//==========================================================================
// LHCb Web Status displays
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For details please see       Online/Stomp/README
// For the licensing terms see  Online/Stomp/LICENSE
//
// Author     : M.Frank
//
//==========================================================================
_loadScript('lhcb.display.items.cpp');
_loadScript('lhcb.display.listener.cpp');
_loadScript('lhcb.display.widgets.cpp');
_loadScript('lhcb.display.partition.cpp');
_loadScript('lhcb.display.zoom.cpp');
_loadFile('lhcb.display.general','css');
_loadFile('lhcb.display.status','css');
_loadFile('lhcb.display.fsm','css');

_loadFile('../ExtJs/resources/css/ext-all','css');
_loadFile('../ExtJs/examples/shared/examples','css');
_loadFile('../ExtJs/examples/grid/grid-examples','css');

_loadScript('../ExtJs/adapter/ext/ext-base.js');
_loadScript('../ExtJs/ext-all.js');

_loadScript('lhcb.display.extWidgets.cpp');
//_loadScript('ExtJs/ext-all-debug.js');

var s_display_font_size = null;
var s_org_display_font_size = null;

function fileLabels(data) {
  //return Math.floor(data/10,10)*10;
  return Math.floor(data);
};

var hlt2Properties2 = function(table)  {
  var cell = Cell('Hello world',1);
  table.runPropertyDisplay = cell;
  table.run_properties.appendChild(table.runPropertyDisplay);
  var prop = lbExt.runInfoTable({
    partition:    table._partition,
	parent:   cell,
	frame:    true,
	title:    'LHCb2 Runinfo'
    });
  prop.add(0,'general.runNumber','Run number',null);
  cell.build = function() { prop.subscribe(); };
  cell.build_horizontal = function() { prop.subscribe(); };
  table.bottom.appendChild(cell);
};

var hlt2Properties = function(table)  {
  var prefix = 'lbWeb.'+table._partition+'_RunInfo.';
  table.runPropertyDisplay = PropertyTable(table._provider, 
					   table._logger, 
					   2,
					   'PropertyTableItem',
					   'PropertyTableValue');
  var prop = table.runPropertyDisplay;
  prop.add(prefix+'general.runNumber',          'Run number',1);
  prop.add(prefix+'Trigger.HLTType',            'Trigger configuration',1);
  prop.add(prefix+'general.runType',            'Activity',1);

  prop.add(prefix+'general.runStartTime',       'Run start time',1);
  prop.add(prefix+'general.dataType',           'Data type',1);
  prop.add(prefix+'HLTFarm.nSubFarms',          'HLT: Number of Subfarms',1);
  prop.add(prefix+'HLTFarm.architecture',       'HLT: Architecture',1);
  prop.add(prefix+'HLTFarm.hltNTriggers',       'HLT: Number of Accept events',1);
  prop.addFormat(prefix+'HLTFarm.hltRate',      'HLT: Accept Rate',1,'%8.2f Hz');
  //prop.addFormat(prefix+'HLTFarm.runHltRate',   'HLT: Integrated accept rate',1,'%8.2f Hz');
  prop.build_horizontal();
  table.run_properties.appendChild(table.runPropertyDisplay);
  table.run_properties.width='50%';
  table.ctrl_status.width='50%';
};

var __showDeferredState = function(table) {}
var showDeferredState = function(table) {
  var tab = document.createElement('table');
  var body = document.createElement('tbody');
  var row, cell; 
  var test = false;

  if ( !test )  {
    tab.style.width = '100%';
    cell = Cell('',2);
    table.currRuns = lbExt.hlt2RunsHistogram({
      id:            'current-runs',
	  parent:    cell,
	  title:     'Runs currently processing',
	  datapoint: 'lbWeb.LHCb_RunInfoSplitHLT.FarmStatus.currRunsSummary',
	  yAxis:    { title:'No. Nodes'},
	  frame:      true,
	  provider:   table.provider});
    row = document.createElement('tr');
    row.appendChild(cell);
    body.appendChild(row);
  }
  /*
  table.allRuns = hlt2RunsHisto(opts,
				'lbWeb.LHCb_RunInfoSplitHLT.RunsLeftCorr',
  {   tag: 'all-runs',
      title: 'All runs with files to be processed',
      yAxis: {title:'No. Files'} });
  table.allRuns.subscribe(table.provider);
  cell = Cell('',2);
  cell.appendChild(table.allRuns);
  row = document.createElement('tr');
  row.appendChild(cell);
  body.appendChild(row);
  */
  if ( !test )  {
    row = document.createElement('tr');
    row.style.width='100%';
    cell = Cell('',1);
    table.allRunsTable = lbExt.hlt2RunsTable({
      renderTo: cell,
	  title: 'All runs with files to be processed',
	  datapoint: 'lbWeb.LHCb_RunInfoSplitHLT.RunsLeftCorr',
	  frame: true,
	  provider: table.provider});
    cell.style.width = '50%';
    row.appendChild(cell);

    cell = Cell('',1,'VerticalAlignTop');
    table.deferred = lbExt.hltDeferredStatus({
      renderTo: cell,
	  frame: true,
	  legend: true,
	  provider: table.provider});
    cell.style.width = '50%';
    row.appendChild(cell);

    body.appendChild(row);
    tab.appendChild(body);
    table.bottom.appendChild(tab);
  }
};

var createDisplay = function(selector)   {
  var partition = selector.selectBox.get_value();
  var opts = {'size_call': selector.size_call};
  if ( null != selector.listener )   {
    selector.listener.close();
    selector.provider.start();
  }
  selector.callbackNo = 3;
  selector.listener = new DetectorListener(selector.logger,selector.provider,selector.display,selector.messages,this);
  selector.listener.createProperties = hlt2Properties;
  selector.listener.trace = false;
  selector.listener.start(partition,'lbWeb.'+partition+'.FSM.children');
  selector.heading.innerHTML = partition+' Run Status Display';
  setWindowTitle(partition+' Run Status');
};

var hlt2_unload = function()  {
  dataProviderReset();
};

var hlt2_body = function()  {
  var prt  = the_displayObject['external_print'];
  var msg  = the_displayObject['messages'];
  var body = document.getElementsByTagName('body')[0];
  var tips = init_tooltips(body);
  var selector = new PartitionSelector(msg);

  var hasFlash = false;
  try {
    hasFlash = Boolean(new ActiveXObject('ShockwaveFlash.ShockwaveFlash'));
  } catch(exception) {
    hasFlash = ('undefined' != typeof navigator.mimeTypes['application/x-shockwave-flash']);
  }
  if ( !hasFlash )   {
    alert('Flash player is not present.\n'+
	  'This page may not display properly!\n\n'+
	  'The histograms will not be filled.');
  }

  s_display_font_size = the_displayObject['size'];
  body.appendChild(selector);
  body.className = 'MainBody';

  if ( msg > 0 )
    selector.logger = new OutputLogger(selector.logDisplay, 200, LOG_INFO, 'RunStatusLogger');
  else
    selector.logger = new OutputLogger(selector.logDisplay,  -1, LOG_INFO, 'RunStatusLogger');
  if ( prt ) selector.logger.print = prt;
  selector.provider = new DataProvider(selector.logger);
  setWindowTitle('LHCb HLT 2 Status');
  selector.selectBox.add('LHCb2','LHCb2',true);
  createDisplay(selector); 
  selector.hideInput();
  selector.showLHCstate();
  showDeferredState(selector);
  selector.provider.start();
};

if ( _debugLoading ) alert('Script lhcb.display.status.cpp loaded successfully');
