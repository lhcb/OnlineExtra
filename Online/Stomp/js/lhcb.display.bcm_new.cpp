//==========================================================================
// LHCb Web Status displays
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For details please see       Online/Stomp/README
// For the licensing terms see  Online/Stomp/LICENSE
//
// Author     : M.Frank
//
//==========================================================================
_loadFile('lhcb.display.ext','css');
_loadFile('lhcb.display.ext.fsm','css');
_loadScript('lhcb.display.zoom.cpp');
_loadScript('lhcb.display.extWidgets.cpp');


var bcm_new_unload = function()  {
  dataProviderReset();
};

var bcm_new_body = function()  {
  var msg  = the_displayObject['messages'];
  var siz  = the_displayObject['size'];
  var sensors= the_displayObject['sensors'];
  bcm_page('lhcb_bcm_page', msg, siz, sensors);
};

console.info('Script lhcb.display.bcm.cpp loaded successfully');
