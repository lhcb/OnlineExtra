//==========================================================================
// LHCb Web Status displays
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For details please see       Online/Stomp/README
// For the licensing terms see  Online/Stomp/LICENSE
//
// Author     : M.Frank
//
//==========================================================================

_loadFile('lhcb.display.ext','css');
_loadFile('lhcb.display.ext.fsm','css');
_loadScript('lhcb.display.zoom.cpp');
_loadScript('lhcb.display.extWidgets.cpp');

var collimator_page = function(page_id, messages, font_size)   {
  // Update window title
  lbExt.setWindowTitle('LHC Collimator setting @ Point 8');

  /// Once the body is ready, this function is called
  Ext.onReady(function()  {
      /// Create view port
      var view = null;
      try {
	var bgcls   = 'MonitorDataItem  Bg-lhcblight Text-Bold';
	var bgcls_r = bgcls + ' Text-Right';
	var bgcls_l = bgcls + ' Text-Left';
	var coll_items = 
	  [
	   // The first line: headers
	   { html: '&nbsp;',                    bodyCls: 'MonitorDataHeader Text-Bold Text-Center' },
	   { html: 'Down&nbsp;&larr;&nbsp;LEFT&nbsp;&rarr;&nbsp;Up',    bodyCls: 'MonitorDataHeader Text-Bold Text-Center', colspan: 2},
	   { html: 'Down&nbsp;&larr;&nbsp;GAP&nbsp;&rarr;&nbsp;Up',     bodyCls: 'MonitorDataHeader Text-Bold Text-Center', colspan: 2},
	   { html: 'Down&nbsp;&larr;&nbsp;RIGHT&nbsp;&rarr;&nbsp;Up',   bodyCls: 'MonitorDataHeader Text-Bold Text-Center', colspan: 2},
	   // Next line:
	   { html: 'TCLIB_6L8_B2',              bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_r},
	   { html: 'undefined',                 bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_r},
	   { html: 'undefined',                 bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_r},
	   // Next line:
	   { html: 'TCTH_4L8_B1',               bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_r},
	   { html: 'undefined',                 bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_r},
	   { html: 'undefined',                 bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_r},
	   // Next line:
	   { html: 'TCTVB_4L8',                 bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_r},
	   { html: 'undefined',                 bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_r},
	   { html: 'undefined',                 bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_r},
	   // Next line:
	   { html: 'TCTH_4R8_B2',               bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_r},
	   { html: 'undefined',                 bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_r},
	   { html: 'undefined',                 bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_r},
	   // Next line:
	   { html: 'TDI_4R8',                   bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_r},
	   { html: 'undefined',                 bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_r},
	   { html: 'undefined',                 bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_r},
	   // Next line:
	   { html: 'TCTVB_4R8',                 bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_r},
	   { html: 'undefined',                 bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_r},
	   { html: 'undefined',                 bodyCls: bgcls_l},
	   { html: 'undefined',                 bodyCls: bgcls_r}];

	var ted_items = [ 
			 {  html: 'TED',                       bodyCls: 'MonitorDataHeader Text-Bold Text-Center' },
			 {  html: 'TEDTI2',                    bodyCls: 'MonitorDataHeader Text-Bold Text-Center' },
			 {  html: 'TEDTI8',                    bodyCls: 'MonitorDataHeader Text-Bold Text-Center' },
			 {  html: 'TEDTT40',                   bodyCls: 'MonitorDataHeader Text-Bold Text-Center' },
			 {  html: 'TEDTT60',                   bodyCls: 'MonitorDataHeader Text-Bold Text-Center' },
			 {  html: 'Position',                  bodyCls: 'MonitorDataHeader Text-Bold Text-Center' },
			 {  html: 'undefined',                 bodyCls: bgcls+' Text-Center'},
			 {  html: 'undefined',                 bodyCls: bgcls+' Text-Center'},
			 {  html: 'undefined',                 bodyCls: bgcls+' Text-Center'},
			 {  html: 'undefined',                 bodyCls: bgcls+' Text-Center'}];

	var logger   = lbExt.logger({active: messages, level: LOG_INFO, height: 700});
	var provider = new DataProvider(logger);

	var coll_panel = lbExt.create('Ext.panel.Panel', {   id:         page_id+'_collimators',
	      title:      '&nbsp;&nbsp;&#151;&#151;&#62; LHC collimators positions around LHCb at Point 8',
	      layout:     { type:  'table', columns: 7, tableAttrs: { style: { width: '100%'} } },
	      defaults:   { bodyStyle: {fontSize: '1em', padding: '5 15 5 15' } },
	      items:      coll_items
	      });
	var ted_panel = lbExt.create('Ext.panel.Panel', {   id:       page_id+'_teds',
	      title:    '&nbsp;&nbsp;&#151;&#151;&#62; LHC TED positions around LHCb at Point 8',
	      layout:   { type: 'table', columns: 5, tableAttrs: { style: { width: '100%'} } },
	      defaults: { bodyStyle: {fontSize: '1em', padding: '5 15 5 15'} },
	      items:    ted_items
	      });
	view = lbExt.create('Ext.Viewport', {
	  id:                 page_id,
	      renderTo:       Ext.getBody(),
	      padding:        '4px',
	      layout:         'border',
	      autoScroll:     true,
	      autoSize:       true,
	      scrollable:     true,
	      defaults:     { minWidth: 1200 },
	      items: [ { id: page_id+'_header',
		  region:   'north',
		  items:    lbExt.standardHeaderItems({id: page_id,
			left: { title:    lbExt.lhcb_online_picture()+
			  '&nbsp;<B>LHCb Collimator Monitor</B>',
			  tooltip:  'Settings of LHC collimators around LHCb<br>'+
			  'Click to go to the LHCb home page',
			  onclick:  lhcb.constants.urls.lhcb.home_page.src},
			provider: provider})
		  },
 		{   id: page_id+'_central_panel',
		    region:     'center',
		    renderTo: Ext.getBody(),
		    layout:   { type: 'table', columns: 1, tableAttrs: { style: { width: '100%', padding: 0 }}},
		    defaults: { autoscroll: true,
		      width:            '100%',
		      padding:          0,
		      closable:         false,
		      collapsed:        false,
		      collapsible:      true,
		      hideCollapseTool: false,
		      collapseFirst:    true,
		      },
		    items:[coll_panel, ted_panel,
			   {   
			   id: page_id+'_copyright',
			       frame:            false,
			       collapsible:      false,
			       hideCollapseTool: true,
			       items:  [lbExt.LHCb_copyright({})]
			       }
			   ]
		    },
		{   id:      page_id+'_bottom_panel',
		    region:  'south',
		    items:   [logger]
		    }
		]
	      });

	var base = 'lbWeb.LHCCOM/LHC.LHC.Machine.CollimatorPositions.TCLIB_6L8_B2.lvdt_';
	lbExt.element(coll_panel.items.get(5)).address(base+'left_downstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(6)).address(base+'left_upstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(7)).address(base+'gap_downstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(8)).address(base+'gap_upstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(9)).address(base+'right_downstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(10)).address(base+'right_upstream').format('%8.2f mm');

	base = 'lbWeb.LHCCOM/LHC.LHC.Machine.CollimatorPositions.TCTH_4L8_B1.lvdt_';
	lbExt.element(coll_panel.items.get(12)).address(base+'left_downstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(13)).address(base+'left_upstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(14)).address(base+'gap_downstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(15)).address(base+'gap_upstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(16)).address(base+'right_downstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(17)).address(base+'right_upstream').format('%8.2f mm');

	base = 'lbWeb.LHCCOM/LHC.LHC.Machine.CollimatorPositions.TCTH_4R8_B2.lvdt_';
	lbExt.element(coll_panel.items.get(19)).address(base+'left_downstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(20)).address(base+'left_upstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(21)).address(base+'gap_downstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(22)).address(base+'gap_upstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(23)).address(base+'right_downstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(24)).address(base+'right_upstream').format('%8.2f mm');

	base = 'lbWeb.LHCCOM/LHC.LHC.Machine.CollimatorPositions.TDI_4R8.lvdt_';
	lbExt.element(coll_panel.items.get(26)).address(base+'left_downstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(27)).address(base+'left_upstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(28)).address(base+'gap_downstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(29)).address(base+'gap_upstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(30)).address(base+'right_downstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(31)).address(base+'right_upstream').format('%8.2f mm');

	base = 'lbWeb.LHCCOM/LHC.LHC.Machine.CollimatorPositions.TCTVB_4L8.lvdt_';
	lbExt.element(coll_panel.items.get(33)).address(base+'left_downstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(34)).address(base+'left_upstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(35)).address(base+'gap_downstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(36)).address(base+'gap_upstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(37)).address(base+'right_downstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(38)).address(base+'right_upstream').format('%8.2f mm');

	base = 'lbWeb.LHCCOM/LHC.LHC.Machine.CollimatorPositions.TCTVB_4R8.lvdt_';
	lbExt.element(coll_panel.items.get(40)).address(base+'left_downstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(41)).address(base+'left_upstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(42)).address(base+'gap_downstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(43)).address(base+'gap_upstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(44)).address(base+'right_downstream').format('%8.2f mm');
	lbExt.element(coll_panel.items.get(45)).address(base+'right_upstream').format('%8.2f mm');

	ted_panel.conversion = function(value)   {
	  var v = '';
	  if ( value == 0 ) return 'Moving'+v;
	  else if ( value == 1 ) return 'Installation'+v;
	  else if ( value == 2 ) return 'Open'+v;
	  else if ( value == 3 ) return 'Closed'+v;
	  else if ( value == 4 ) return 'Transport'+v;
	  else if ( value == 5 ) return 'Undefined'+v;
	  return 'Unknown ('+value+')';
	};

	base = 'lbWeb.LHCCOM/LHC.LHC.Machine.TEDPosition.';
	lbExt.element(ted_panel.items.get(6))
	  .address(base+'TEDTI2.position').convert(ted_panel.conversion);
	lbExt.element(ted_panel.items.get(7))
	  .address(base+'TEDTI8.position').convert(ted_panel.conversion);
	lbExt.element(ted_panel.items.get(8))
	  .address(base+'TEDTT40.position').convert(ted_panel.conversion);
	lbExt.element(ted_panel.items.get(9))
	  .address(base+'TEDTT60.position').convert(ted_panel.conversion);
    
	lbExt.subscribe(coll_panel, provider);
	lbExt.subscribe(ted_panel, provider);
      }
      catch(e)  {
	var msg = '\nException: '+e;
	console.error('[EXCEPTION] ' + msg);
      }
      /// Initialize tool tips
      Ext.QuickTips.init();
      /// Apply zoom depending on the screen size and the browser:
      lbExt.initZoom({fontSize: font_size});
      /// Start the comet engine
      provider.start();
    });
};

var collimators_new_unload = function()  {
  dataProviderReset();
};


var collimators_new_body = function()  {
  var msg  = the_displayObject['messages'];
  var siz  = the_displayObject['size'];
  collimator_page('lhcb_collimator_page', msg, siz);
};

console.info('Script lhcb.display.collimators.cpp loaded successfully');
