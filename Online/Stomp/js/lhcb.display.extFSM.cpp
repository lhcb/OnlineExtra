
lbFSM = new Object();

/// Main entry point to create the 'LHCb Hlt2' web page
/*
 *    \author  M.Frank
 *    \version 2.0
 */
lbFSM.simpleRunControl = function(opts)   {
  var listener = {
  logger      : null,
  provider    : null,
  data        : '',
  trgListener : null,
  detListener : null,
  item        : 'lbWeb.'+opts.partition+'.FSM.children',
  panel       : lbExt.create('Ext.panel.Panel', {
    style: { width:       '100%', height:  '100%' },
	title:            opts.title,
	flex:             1,
	width:            '100%',
	height:           '100%',
	frame:            true,
	closable:         false,
	collapsed:        false,
	collapsible:      true,
	hideCollapseTool: false,
	collapseFirst:    true,
	animCollapse:     true,
	autoScroll:       false,
	layout:         { type: 'vbox', align: 'stretch' }, 
	defaults:       {  autoScroll: true, 
	  flex: 3, 
	  width: '100%'}
    }),

  start: function() {
      this.provider.subscribe(this.item,this);
      return this;
    },

  stop: function() {
      this.provider.unsubscribe(this.item);
      return this;
    },
  
  subscribeSubListeners: function() {},
  handle_no_data: function()  { return null; },
  handle_data:    function(partition, sys_names, sys_states) {
      var txt, item, space;
      lbExt.unsubscribe(this.panel,this.provider);
      this.panel.removeAll(true);
      item = lbExt.FSMelement(this.partition, true,  {
	label: {text: this.partition}, 
	    state: true, 
	    lock:  true, 
	    style: {fontSize: this.panel.style.fontSize}});
      this.panel.add(item);
      space = lbExt.create('Ext.panel.Panel',  {
	html: '',
	    height: 5,
	    width: '100%',
	    bodyCls: 'Bg-lhcblight NoBorder'});
      this.panel.add(space);
      for (var i=0; i<sys_names.length; ++i)  {
	txt  = sys_names[i].substr(this.partition.length+1);
	item = lbExt.FSMelement(sys_names[i], true,{
	  label: {text: txt}, 
	      state: true, 
	      lock: true,
	      style: {fontSize: this.panel.style.fontSize}});
	this.panel.add(item);
      }
      txt = item = null;
      lbExt.subscribe(this.panel,this.provider);
    },

  set:       function(data)  {
      this.logger.info('DetectorListener: Got data:'+data);
      if ( data.length < 2 )  {
	if ( this.handle_no_data(this.partition) != null )
	  return this;
	else if ( this.trace )
	  this.logger.info('(1) The service '+this._item+' looks not sane....\n\n'+
			   'These are the data I received:\n'+data);
	return null;
      }
      if ( this.data == data[1] )
	return null;

      this.data = data[1];
      var systems = data[1].split('/');
      if ( (systems.length < 1 || systems[0] == "DEAD") && this.trace ) {
	this.logger.info('(2) The service '+this._item+' looks not sane....\n\n'+
			 'These are the data I received:\n'+data[0]+'\n'+data[1]);
	return null;
      }

      var sys_names  = new Array();
      var sys_states = new Array();

      for(var i=0; i<systems.length; ++i)  {
	var sys = systems[i].split('|');
	// Remove HV, INF and injector from LHCb.
	// These are not controlled by the run-control itself.
	if ( sys[0]=='LHCb_HV' ) continue;
	if ( sys[0]=='LHCb_INF' ) continue;
	if ( sys[0]=='LHCb_Injector' ) continue;
	sys_names.push(sys[0]);
	sys_states.push(sys[1]);
	sys = null;
      }
      this.logger.debug('DetectorListener: Got data:'+data+'\n'+
			'sys_names:'+sys_names+'\n'+
			'sys_states:'+sys_states+'\n'
			);
      this.handle_data(this.partition, sys_names, sys_states);
      this.subscribeSubListeners();
      if ( this._opts && this._opts.size_call ) this._opts.size_call();
      sys_names = sys_states = null;
      return this;
    }
  };

  // Copy all other properties from the options to the object:
  for (var property in opts)
    if (opts.hasOwnProperty(property))
      listener[property] = opts[property];

  listener.panel.control = listener;
  listener.start();
  return listener.panel;
};
