//==========================================================================
// LHCb Web Status displays
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For details please see       Online/Stomp/README
// For the licensing terms see  Online/Stomp/LICENSE
//
// Author     : M.Frank
//
//==========================================================================

_loadFile('lhcb.display.ext','css');
_loadFile('lhcb.display.ext.fsm','css');
_loadScript('lhcb.display.zoom.cpp');
_loadScript('lhcb.display.extWidgets.cpp');
_loadScript('lhcb.display.extFSM.cpp');

var Hlt2 = function(page_id, messages, font_size) {
  /// Update window title
  lbExt.setWindowTitle('HLT2 Status Page');

  /// Once the body is ready, this function is called
  Ext.onReady(function()  {
      try {
	var logger    = lbExt.logger({active: messages, level: LOG_INFO, style: { height: 300 }});
	var provider  = new DataProvider(logger);
	var run_props = [{dp: 'general.runNumber',    title: 'Run number',                   colspan: 1},
			 {dp: 'Trigger.HLTType',      title: 'Trigger configuration',        colspan: 1},
			 {dp: 'general.runType',      title: 'Activity',                     colspan: 1},
			 //{dp: 'general.runStartTime', title: 'Run start time',               colspan: 1},
			 //{dp: 'general.dataType',     title: 'Data type',                    colspan: 1},
			 {dp: 'HLTFarm.nSubFarms',    title: 'HLT: Number of Subfarms',      colspan: 1},
			 {dp: 'HLTFarm.architecture', title: 'HLT: Architecture',            colspan: 1}
			 //{dp: 'HLTFarm.hltNTriggers', title: 'HLT: Number of Accept events', colspan: 1},
			 //{dp: 'HLTFarm.hltRate',      title: 'HLT: Accept Rate',             colspan: 1, format: '%8.2f Hz'}
			 ];

	var list = lbFSM.simpleRunControl({
	  provider:      provider,
	      partition: 'LHCb2',
	      logger:    logger,
	      item:      'lbWeb.LHCb2.FSM.children',
	      trace:     true});

	/// Create view port and attach panels to it
	var view = lbExt.create('Ext.Viewport', {
	  id:                 page_id,
	      renderTo: Ext.getBody(),
	      padding:        '6px',
	      layout:         'border',
	      fontSize:       '1em',
	      autoScroll:     true,
	      defaults:     { header:           false,bodyStyle:    'padding: 0px' },
	      items: [ { id: page_id+'_top_panel',
		  region:   'north',
		  items:    lbExt.standardHeaderItems({id: page_id,
			left: { title:    lbExt.lhcb_online_picture()+
			  '&nbsp;<B>HLT2 Status Monitor</B>',
			  tooltip:  'HLT2 processing status<br>'+
			  'Click to go to the LHCb home page',
			  onclick_no:        lhcb.constants.urls.lhcb.home_page.src},
			provider: provider})
		  },
		{   id:           page_id+'_central_panel',
		    region:       'center',
		    layout:     {  type: 'table', columns: 1, tableAttrs: { style: { width: '100%'}}},
		    //layout:     {  type: 'vbox', defaults: { split: true }  } ,
		    defaults:     {  autoscroll: true,  width: '100%', flex: 2 },
		      items:[     {
		      layout: {  type: 'hbox', defaults: { autoScroll: true, split: true } },
			  defaults: {  autoScroll: true, maxHeight:   220, minHeight: 200 },
			  items:  [list.panel,
				   lbExt.runInfoTable({partition: 'LHCb2', 
					 title: 'LHCb2 run properties',
					 properties: run_props,
					 provider:   provider,
					 flex: 1})]
			  },
			{   layout:   { type: 'hbox', defaults: { split: true }  } , 
			    defaults: { autoScroll: true, height: 250 },
			    items: [
				    lbExt.hlt2RunsHistogramPanel({
				      id:            page_id+'_current_runs',
					  title:     'Current runs processing in HLT2',
					  minHeight:  300,
					  datapoint: 'lbWeb.LHCb_RunInfoSplitHLT.FarmStatus.currRunsSummary',
					  provider:  provider})
				    ]
			    },
			{   layout:   { type: 'hbox', defaults: { split: true }  } , 
			    defaults: { autoScroll: true, height: 400 },
			    items: [
				    lbExt.hlt2RunsTablePanel({
				      title:     'All runs with files to be processed',
					  datapoint: 'lbWeb.LHCb_RunInfoSplitHLT.RunsLeftCorr',
					  provider:  provider,
					  height:    300}),
				    lbExt.hltDeferredStatus({
				      provider:      provider,
					  legend:    true,
					  height:    300})
				    ]
			    }]}
		,
		{   region:   'south',
		    items:    [lbExt.LHCb_copyright({}), logger]
		    }
		]});
      }
      catch(e)  {
	console.error('[EXCEPTION] '+e);
      }
      
      /// Initialize tool tips
      Ext.QuickTips.init();
      /// Apply zoom depending on the screen size and the browser:
      lbExt.initZoom({fontSize: font_size});

      list.start();
      //_dataProvider.printData = true;
      /// Start the comet engine
      provider.start();

      var b = Ext.getBody();
    });
};

var hlt2_test_unload = function()  {
  dataProviderReset();
};

var hlt2_test_body = function()  {
  var msg  = the_displayObject['messages'];
  var siz  = the_displayObject['size'];
  var body = document.getElementsByTagName('body')[0];
  body.style.autoScroll = true;
  body.style.height = 1200;
  return Hlt2('lhcb_hlt2_main', msg, siz);
};

if ( _debugLoading ) console.info('Script lhcb.display.status.cpp loaded successfully');
