//==========================================================================
// LHCb Web Status displays
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For details please see       Online/Stomp/README
// For the licensing terms see  Online/Stomp/LICENSE
//
// Author     : M.Frank
//
//==========================================================================
//
// Please note: 
// ------------
//
// This script can only be included AFTER having loaded:
//  _loadScript('lhcb.display.extWidgets.cpp');
//
//==========================================================================

/// Check if panel was already defined in another script:
if ( !lbExt.hasOwnProperty('veloVacuumStatus') )   {

  /** ExtJs widget to display the velo vacuum status
   *
   *  +--- Velo Vacuum Status. Pressures in [mbar] ------------
   *  +
   *  + Device Device Device Device Device Device 
   *  +
   *  + value  value  value  value  value  value  
   *  +--------------------------------------------------------
   *
   *  \author  M.Frank
   *  \version 1.0
   */
  lbExt.veloVacuumStatus = function(opts) {
    var width   = opts.hasOwnProperty('width')   ? opts.width  : '99.8%';
    var title   = opts.hasOwnProperty('title')   ? opts.title  : 'Velo Vacuum Status. Pressures in [mbar]';
    var id      = opts.hasOwnProperty('id')      ? opts.id     : 'lhcb-velo-vacuum-status';
    var units   = opts.hasOwnProperty('units')   ? '&nbsp;mbar' : '';
    var fsm     = lbExt.FSMelement('VELO_VACUUM', true, {label: false, state: true, lock: true, style: {fontSize: 18}});
    fsm.rowspan = 2;
    var options = lbExt.panelDefaults({id: id,
	  title:            title,
	  width:            width,
	  layout:         { type: 'table', columns: 8,
	    tableAttrs:   { style: { padding: '0 10 0 10', width: '100%' }}},
	  defaults: {       border:             true,
	    xtype:              'box',
	    style: { textAlign: 'left', padding:   '2 10 2 10' } },
	  items: [fsm,
		  { html: '&nbsp;',               cls: 'Text-Bold' },
		  { html: 'AP411',                cls: 'MonitorDataHeader Text-Bold' }, // 2
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' },
		  { html: 'DP411',                cls: 'MonitorDataHeader Text-Bold' },
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' },
		  { html: 'PE411',                cls: 'MonitorDataHeader Text-Bold' },
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' },
		  { html: '&nbsp;',               cls: 'Text-Bold' },
		  { html: 'PE412',                cls: 'MonitorDataHeader Text-Bold' },
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' },
		  { html: 'PE421',                cls: 'MonitorDataHeader Text-Bold' },
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' },
		  { html: 'PE422',                cls: 'MonitorDataHeader Text-Bold' },
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' }]
	  }, opts);
    var panel  = lbExt.create('Ext.panel.Panel',options);
    lbExt.element(panel.items.get(3)).address('veloVac_Barotrons.AP411.Pressure').format('%7.1f'+units);
    lbExt.element(panel.items.get(5)).address('veloVac_Barotrons.DP411.Pressure').format('%7.1e'+units);
    lbExt.element(panel.items.get(7)).address('veloVac_Meters.PE411.Vacuum').format('%7.1e'+units);
    lbExt.element(panel.items.get(10)).address('veloVac_Meters.PE412.Vacuum').format('%7.1e'+units);
    lbExt.element(panel.items.get(12)).address('veloVac_Meters.PE421.Vacuum').format('%7.1e'+units);
    lbExt.element(panel.items.get(14)).address('veloVac_Meters.PE422.Vacuum').format('%7.1e'+units);
    // Install tooltips and callbacks if required
    if ( options.id )  {
      var elt = document.getElementById(id+'-body');
      if ( options.onclick )  {
	elt.onmouseout  = lbExt.setCursorDefault;
	elt.onmouseover = lbExt.setCursorLink;
	elt.onclick     = options.onclick;
      }
      if ( options.tooltip )  {
	elt.tooltip = new Ext.ToolTip({ target: elt.id, trackMouse: true, html: options.tooltip});
      }
    }
    /// Subscribe all required data item to receive data from the provider object
    lbExt.subscribe(panel, opts.provider);
    return panel;
  };
 }  /// Main file guard to avoid double inclusion

/// Check if panel was already defined in another script:
if ( !lbExt.hasOwnProperty('veloPositionSummary') )   {

  /// Basic numeric display of the LHCb verlo position
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   */
  lbExt.veloPositionSummary = function(opts)   {
    var width   = opts.hasOwnProperty('width')   ? opts.width  : '99.8%';
    var title   = opts.hasOwnProperty('title')   ? opts.title  : 'Velo Position';
    var id      = opts.hasOwnProperty('id')      ? opts.id     : 'lhcb-velo-position-numeric';
    var fsm     = lbExt.FSMelement('VELO_MOTION', true, 
    {label: false, state: true, lock: true, style: { fontSize: 18}});
    var options = lbExt.panelDefaults({
      id:               id,
	  title:            title,
	  width:            width,
	  layout:         { type: 'table', columns: 9,
	    tableAttrs:   { style: { padding: '0 10 0 10', width: '100%' }}},
	  defaults:       { border:             true,
	    xtype:          'box',
	    style:        { padding:   '2 10 2 10' } },
	  items: [fsm,
		  { html: 'Position',             cls: 'MonitorDataHeader Text-Bold Text-Left' }, // 1
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold Text-Center' },
		  { html: 'X<sub>A</sub>',        cls: 'MonitorDataHeader Text-Bold' },
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' },
		  { html: 'X<sub>C</sub>',        cls: 'MonitorDataHeader Text-Bold' }, // 5
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' },
		  { html: 'Y',                    cls: 'MonitorDataHeader Text-Bold' },
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' }],
	  buttons: []
	  }, opts);
    var panel  = this.create('Ext.panel.Panel',options);
    this.element(panel.items.get(2)).address('LHCCOM/LHC.LHCb.Specific.VELO.Position');
    this.element(panel.items.get(4)).address('veloMove_Xle.Potm.Av').format('%.2f mm');
    this.element(panel.items.get(6)).address('veloMove_Xri.Potm.Av').format('%.2f mm');
    this.element(panel.items.get(8)).address('veloMove_Yy.Potm.Av').format('%.2f mm');
    // Install tooltips and callbacks if required
    if ( options.id )  {
      var elt = document.getElementById(id+'-body');
      if ( options.onclick )  {
	elt.onmouseout  = lbExt.setCursorDefault;
	elt.onmouseover = lbExt.setCursorLink;
	elt.onclick     = options.onclick;
      }
      if ( options.tooltip )  {
	elt.tooltip = new Ext.ToolTip({ target: elt.id, trackMouse: true, html: options.tooltip});
      }
    }
    /// Subscribe all required data item to receive data from the provider object
    lbExt.subscribe(panel, opts.provider);
    return panel;
  };
 }  /// Main file guard to avoid double inclusion

/// Check if panel was already defined in another script:
if ( !lbExt.hasOwnProperty('veloPositionDisplay') )   {

  /// Graphical display of the LHCb verlo position
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   */
  lbExt.veloPositionDisplay = function(opts)   {
    if ( !opts.hasOwnProperty('id')    ) opts.id = 'lhcb-velo-position-monitor';
    if ( !opts.hasOwnProperty('title') ) opts.title = 'Velo Position Monitor';
    var panel = lbExt.canvasPanel(opts);

    panel.setGrid(lbExt.canvasGrid(100,50,{x:50,y:23}));
    var drawing = panel.grid;
    drawing.imgC = drawing.image('Image_Velo_Right_Small','Images/VeloRight.png',30,46);
    drawing.imgA = drawing.image('Image_Velo_Left_Small', 'Images/VeloLeft.png', 30,46);

    panel.updateXA = function(value)  {
      this.panel.XA.value = this.panel.XA.data(value);
      this.panel.updateDrawing();
    };
    panel.updateXC = function(value)  {
      this.panel.XC.value = this.panel.XC.data(value);
      this.panel.updateDrawing();
    };
    panel.updateY = function(value)  {
      this.panel.Y.value = this.panel.Y.data(value);
      this.panel.updateDrawing();
    };

    panel.updateDrawing = function()   {
      var xa = this.XA.value;
      var xc = this.XC.value;
      var y  = this.Y.value;
      console.info('XA: '+xa,'XC: '+xc,'Yy: '+y);
      if ( xa && xc && y )  {
	this.grid.clear();
	this.draw(xa,xc,y);
      }
    };

    panel.draw = function(Xa, Xc, Y)   {
      var drawing = this.grid;
      var RED   = '#F31';
      var GREEN = '#091';
      var BLACK = 'black';
      if ( !Y ) Y = 0.0;
      drawing.imgA.draw(-15-(Xa ? Xa : -10)+0.9, Y ? Y : 0);
      drawing.imgC.draw( 15-(Xc ? Xc :  10)-0.9, Y ? Y : 0);
      drawing.setOptions({textAlign: 'left', 
	    lineCap:     'round', 
	    lineJoin:    'round',
	    strokeStyle: BLACK, 
	    lineWidth:   1,
	    fillStyle:   BLACK,
	    font:        'bold 12pt sans-serif'});
      if ( !Xc || Xc < -5 )  {
	drawing.rectangle(-16, -2, 14, 4,{fillStyle: 'grey'});
	drawing.path([[-16, 2],[-2,2],[-2,-2], [-16,-2], [-16,2]],{strokeStyle: BLACK, lineWidth: 2});
	drawing.line(-16,  -2, -16, -25, {strokeStyle: BLACK, lineWidth: 1});
	drawing.text(-18, -27, '+16 mm', {fillStyle:   BLACK});
      }
      if ( !Xa || Xa >  5 )  {
	drawing.rectangle(  2, -2, 14, 4,{fillStyle: 'grey'});
	drawing.path([[ 16, 2],[ 2,2],[ 2,-2], [ 16,-2], [ 16,2]],{strokeStyle: BLACK, lineWidth: 2});
	drawing.line( 16,  -2, 16,  -25, {strokeStyle: BLACK, lineWidth: 1});
	drawing.text( 15, -27, '-16 mm', {fillStyle:   BLACK});
	drawing.line(-100, -2, -16, -2,  {strokeStyle: GREEN, lineWidth: 1});
	drawing.line(-100,  2, -16,  2,  {strokeStyle: GREEN, lineWidth: 1});
      }
      else  {
	drawing.line(-100, -2, -2, -2, {strokeStyle: GREEN, lineWidth: 1});
	drawing.line(-100,  2, -2,  2, {strokeStyle: GREEN, lineWidth: 1});
      }
      drawing.path([[ -2, 2],[ 2,2],[ 2,-2], [ -2,-2], [ -2,2]],{strokeStyle: BLACK, lineWidth: 2});
      drawing.ellipse(0,0,1.9,1.9,Math.PI,0.,2.0*Math.PI,{fillStyle: '#999', strokeStyle: GREEN, lineWidth: 5});
      drawing.text( -49.8,  2.2,  '2 mm', {fillStyle: GREEN, textAlign: 'left'});
      drawing.text( -49.8, -3.2, '-2 mm', {fillStyle: GREEN, textAlign: 'left'});

      drawing.line( -2,-2, -2,-25, {strokeStyle: BLACK, lineWidth: 1});
      drawing.line(  2,-2,  2,-25, {strokeStyle: BLACK, lineWidth: 1});

      drawing.drawSystem({strokeStyle: 'blue', lineWidth: 2});
      drawing.ellipse(0,0,.5,.5,Math.PI,0.,2.0*Math.PI,{fillStyle: RED,  strokeStyle: RED, lineWidth: 1});
      drawing.text( -4, -27,  '+2 mm', {fillStyle: BLACK});
      drawing.text(  1, -27,  '-2 mm', {fillStyle: BLACK});

      drawing.setOptions({font:     'bold 16pt sans-serif'});
      drawing.line( -100, Y,   -16,  Y, {strokeStyle: '#0C2',  lineWidth: 3});
      drawing.text(-49.8, Y+5, sprintf('Y=%.2f mm',Y), {fillStyle: '#0C2', textAlign: 'left'});
      if ( Xa )  {
	drawing.line(-Xa+0.1, -4, -Xa+0.1, -25, {strokeStyle: RED,    lineWidth: 3});
	drawing.text(-Xa-1,   -8, sprintf('XA=%.2f mm',Xa), {fillStyle: RED, textAlign: 'right'});
      }
      if ( Xc )  {
	drawing.line(-Xc-0.1, -4, -Xc-0.1, -25, {strokeStyle: 'yellow', lineWidth: 3});
	drawing.text(-Xc+1,   -8, sprintf('XC=%.2f mm',Xc), {fillStyle: 'yellow', textAlign: 'left'});
      }
    };
    panel.draw(16,-16,0);

    this.element(panel.XA={panel: panel})
    .address('veloMove_Xle.Potm.Av')
    .subscribe(opts.provider)
    .display = panel.updateXA;

    this.element(panel.XC={panel: panel})
    .address('veloMove_Xri.Potm.Av')
    .subscribe(opts.provider)
    .display = panel.updateXC;

    this.element(panel.Y={panel: panel})
    .address('veloMove_Yy.Potm.Av')
    .subscribe(opts.provider)
    .display = panel.updateY;

    return panel;
  };

 }  /// Main file guard to avoid double inclusion

/// Check if panel was already defined in another script:
if ( !lbExt.hasOwnProperty('magnetStatus') )   {

  /** ExtJs widget to display the magnet status
   *
   *  \author  M.Frank
   *  \version 1.0
   */
  lbExt.magnetStatus = function(opts)   {
    var width   = opts.hasOwnProperty('width')   ? opts.width  : '99.8%';
    var title   = opts.hasOwnProperty('title')   ? opts.title  : 'LHCb Magnet Status';
    var id      = opts.hasOwnProperty('id')      ? opts.id     : 'lhcb-magnet-status';
    var units   = opts.hasOwnProperty('units')   ? '&nbsp;mbar' : '';
    var options = lbExt.panelDefaults({id: id,
	  title:            title,
	  width:            width,
	  layout:         { type: 'table', columns: 6,
	    tableAttrs:   { style: { padding: '0 10 0 10', width: '100%' }}},
	  minHeight:        100,
	  defaults: {       border:             true,
	    xtype:              'box',
	    style: { padding:   '4 10 4 10', width: '100%'	}
	},
	  items: [{ html: 'Status',               cls: 'MonitorDataHeader Text-Center Text-Bold Text-Huge', rowspan: 2 }, // 0
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Center Text-Bold Text-Huge Text-Border', rowspan: 2 },
		  { html: 'Polarity',             cls: 'MonitorDataHeader Text-Center Text-Bold Text-Huge', rowspan: 2 },
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Center Text-Bold Text-Huge', rowspan: 2 },
		  { html: 'Current Set<sub>&nbsp;</sub>',          cls: 'MonitorDataHeader Text-Bold' },
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' },
		  { html: 'Current Measured<sub>&nbsp;</sub>',     cls: 'MonitorDataHeader Text-Bold' },
		  { html: 'undefined',            cls: 'MonitorDataItem   Text-Bold' },
		  { html: '&nbsp;',                                       colspan: 6 },

		  { html: 'Sensors<sub>&nbsp;</sub>',                     cls: 'MonitorDataHeader Text-Center Text-Bold Text-Big' }, // 9
		  { html: '<B>|B<sub>abs</sub>|</B> [&nbsp;Tesla&nbsp;]', cls: 'MonitorDataHeader Text-Center Text-Bold Text-Big' },
		  { html: 'B<sub>x</sub> [&nbsp;Tesla&nbsp;]',            cls: 'MonitorDataHeader Text-Center Text-Bold Text-Big' },
		  { html: 'B<sub>y</sub> [&nbsp;Tesla&nbsp;]',            cls: 'MonitorDataHeader Text-Center Text-Bold Text-Big' },
		  { html: 'B<sub>z</sub> [&nbsp;Tesla&nbsp;]',            cls: 'MonitorDataHeader Text-Center Text-Bold Text-Big' },
		  { html: 'Temperature<sub>&nbsp;</sub>',                 cls: 'MonitorDataHeader Text-Center Text-Bold Text-Big' },

		  { html: 'Sensor&nbsp;0',                                cls: 'MonitorDataHeader Text-Center Text-Bold Text-Big' }, // 15
		  { html: 'undefined',                                    cls: 'MonitorDataItem  Text-Center Text-Bold' },
		  { html: 'undefined',                                    cls: 'MonitorDataItem  Text-Center Text-Bold' },
		  { html: 'undefined',                                    cls: 'MonitorDataItem  Text-Center Text-Bold' },
		  { html: 'undefined',                                    cls: 'MonitorDataItem  Text-Center Text-Bold' },
		  { html: 'undefined',                                    cls: 'MonitorDataItem  Text-Center Text-Bold' },
		  { html: 'Sensor&nbsp;1',                                cls: 'MonitorDataHeader Text-Center Text-Bold Text-Big' }, // 21
		  { html: 'undefined',                                    cls: 'MonitorDataItem  Text-Center Text-Bold' },
		  { html: 'undefined',                                    cls: 'MonitorDataItem  Text-Center Text-Bold' },
		  { html: 'undefined',                                    cls: 'MonitorDataItem  Text-Center Text-Bold' },
		  { html: 'undefined',                                    cls: 'MonitorDataItem  Text-Center Text-Bold' },
		  { html: 'undefined',                                    cls: 'MonitorDataItem  Text-Center Text-Bold' },
		  { html: 'Sensor&nbsp;2',                                cls: 'MonitorDataHeader Text-Center Text-Bold Text-Big' }, // 27
		  { html: 'undefined',                                    cls: 'MonitorDataItem  Text-Center Text-Bold' },
		  { html: 'undefined',                                    cls: 'MonitorDataItem  Text-Center Text-Bold' },
		  { html: 'undefined',                                    cls: 'MonitorDataItem  Text-Center Text-Bold' },
		  { html: 'undefined',                                    cls: 'MonitorDataItem  Text-Center Text-Bold' },
		  { html: 'undefined',                                    cls: 'MonitorDataItem  Text-Center Text-Bold' },
		  { html: 'Sensor&nbsp;3',                                cls: 'MonitorDataHeader Text-Center Text-Bold Text-Big' }, // 33
		  { html: 'undefined',                                    cls: 'MonitorDataItem  Text-Center Text-Bold' },
		  { html: 'undefined',                                    cls: 'MonitorDataItem  Text-Center Text-Bold' },
		  { html: 'undefined',                                    cls: 'MonitorDataItem  Text-Center Text-Bold' },
		  { html: 'undefined',                                    cls: 'MonitorDataItem  Text-Center Text-Bold' },
		  { html: 'undefined',                                    cls: 'MonitorDataItem  Text-Center Text-Bold' }]
	  }, opts);
    var panel  = lbExt.create('Ext.panel.Panel',options);
    lbExt.element(panel.items.get(1)).address('lbHyst.B').convert(function(data){
	var d = Math.abs(data);
	this.el.removeCls('FwStateOKPhysics');
	this.el.removeCls('FwStateAttention1');
	this.el.removeCls('FwStateAttention2');
	if ( d>0.75 ) {
	  this.el.addCls('FwStateOKPhysics');
	  return 'ON';
	}
	else if ( d<0.1 ) {
	  this.el.addCls('FwStateAttention1');
	  return 'OFF';
	}
	this.el.addCls('FwStateAttention2');
	return 'RAMP';
      });
    lbExt.element(panel.items.get(3)).address('LbMagnet.Polarity').convert(function(data){
	var x = document.getElementById('polarity_arrow');
	if ( data>0 ) {
	  return '+&nbsp;&nbsp;'+lbExt.lhcb_logo(_fileBase+'/Images/Down.png');
	}
	return '+&nbsp;&nbsp;'+lbExt.lhcb_logo(_fileBase+'/Images/Up.png');
      });
    lbExt.element(panel.items.get(5)).address('LbMagnet.SetCurrent').format('%7.0f Ampere');
    lbExt.element(panel.items.get(7)).address('LbMagnet.Current').format('%7.0f Ampere');

    lbExt.element(panel.items.get(16)).address('LbMagnet.BSensor0.Babs').format('%7.4f Tesla');
    lbExt.element(panel.items.get(17)).address('LbMagnet.BSensor0.Bx').format('%7.4f Tesla');
    lbExt.element(panel.items.get(18)).address('LbMagnet.BSensor0.By').format('%7.4f Tesla');
    lbExt.element(panel.items.get(19)).address('LbMagnet.BSensor0.Bz').format('%7.4f Tesla');
    lbExt.element(panel.items.get(20)).address('LbMagnet.BSensor0.Temp').format('%7.2f &#186;C');

    lbExt.element(panel.items.get(22)).address('LbMagnet.BSensor1.Babs').format('%7.4f Tesla');
    lbExt.element(panel.items.get(23)).address('LbMagnet.BSensor1.Bx').format('%7.4f Tesla');
    lbExt.element(panel.items.get(24)).address('LbMagnet.BSensor1.By').format('%7.4f Tesla');
    lbExt.element(panel.items.get(25)).address('LbMagnet.BSensor1.Bz').format('%7.4f Tesla');
    lbExt.element(panel.items.get(26)).address('LbMagnet.BSensor1.Temp').format('%7.2f &#186;C');

    lbExt.element(panel.items.get(28)).address('LbMagnet.BSensor2.Babs').format('%7.4f Tesla');
    lbExt.element(panel.items.get(29)).address('LbMagnet.BSensor2.Bx').format('%7.4f Tesla');
    lbExt.element(panel.items.get(30)).address('LbMagnet.BSensor2.By').format('%7.4f Tesla');
    lbExt.element(panel.items.get(31)).address('LbMagnet.BSensor2.Bz').format('%7.4f Tesla');
    lbExt.element(panel.items.get(32)).address('LbMagnet.BSensor2.Temp').format('%7.2f &#186;C');

    lbExt.element(panel.items.get(34)).address('LbMagnet.BSensor3.Babs').format('%7.4f Tesla');
    lbExt.element(panel.items.get(35)).address('LbMagnet.BSensor3.Bx').format('%7.4f Tesla');
    lbExt.element(panel.items.get(36)).address('LbMagnet.BSensor3.By').format('%7.4f Tesla');
    lbExt.element(panel.items.get(37)).address('LbMagnet.BSensor3.Bz').format('%7.4f Tesla');
    lbExt.element(panel.items.get(38)).address('LbMagnet.BSensor3.Temp').format('%7.2f &#186;C');

    // Install tooltips and callbacks if required
    if ( options.id )  {
      var elt = document.getElementById(id+'-body');
      if ( options.onclick )  {
	elt.onmouseout  = lbExt.setCursorDefault;
	elt.onmouseover = lbExt.setCursorLink;
	elt.onclick     = options.onclick;
      }
      if ( options.tooltip )  {
	elt.tooltip = new Ext.ToolTip({ target: elt.id, trackMouse: true, html: options.tooltip});
      }
    }
    /// Subscribe all required data item to receive data from the provider object
    lbExt.subscribe(panel, opts.provider);
    return panel;
  };
 }  /// Main file guard to avoid double inclusion

