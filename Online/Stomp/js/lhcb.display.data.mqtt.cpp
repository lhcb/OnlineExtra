//==========================================================================
// LHCb Web Status displays
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For details please see       Online/MQTT/README   ---- CHANGE FOLDER NAME, CL 27/07
// For the licensing terms see  Online/MQTT/LICENSE  ---- CHANGE FOLDER NAME, CL 27/07
//
// Author     : M.Frank
//
//==========================================================================

var _dataProvider = null;

/** @class DataProvider
 *
 *  @author  M.Frank
 *  @version 1.0
 */
var DataProvider = function(logger)  {
  this.calls = new Array();
  this.items = new Object();
  this.calls.length    = 0;
  this.logger          = logger;
  this.isConnected     = false;
  this.needConnection  = false;
  this.topic           = '/item';
  this.command         = '/topic/clientCommands';
  this.pageName        = 'dataPage';
  this.service         = null;
  this.lastMessageTime = 'never!';
  this.reloadInterval  = 2*24*3600*1000; // reload once every 2 days...
  this.checkInterval   = 30000;
  this.reconnectTimer  = null;
  this.printData       = false;
  _dataProvider        = this;

  if ( the_displayObject ) this.pageName=the_displayObject.type;

  /// Handler for automatic reconnection
  this.reconnectHandler = function() {
    var dp = _dataProvider;
    dp.logger.info('MQTT socket:'+
		   ' connected:'+dp.isConnected+
		   ' connection required:'+ dp.needConnection+
		   ' last message:'+dp.lastMessageTime.toString());
    if ( !dp.isConnected && dp.needConnection ) {
      dp.reconnect();  // start() will rearm the checking....
      return;
    }
    dp.reconnectTimer = setTimeout(dp.reconnectHandler,dp.checkInterval);
  };

  /// Data dispatch callback once mqtt receives data
  this.onMessageArrived = function(frame) {
    _dataProvider.lastMessageTime = new Date();
    var i, o, len, itm, data, v = frame.payloadString.split('#');
    if ( v.length >= 2 ) {
      itm  = v[1];
      data = v.slice(2);
      v = null;
      if ( data != 'DEAD' ) {
	o = _dataProvider.items[itm];
	len = o.length;
	if ( !o.prev_data ) {
	  o.prev_data = data;
	}
	else if ( o.prev_data+'' == data+'' ) {
	  logger.verbose('DataProvider: ignore: [' +frame.payloadString.length+' bytes] '+itm+'='+o.prev_data);
	  return;
	}
	o.prev_data = data;
	for(i=0; i<len; ++i) {
	  if ( o[i] ) {
	    o[i].set(data);
	  }
	  else {
	    logger.warning('DataProvider: MQTT socket: Debug: Dead element: '+itm+'['+i+'] out of '+len);
	  }
	}
	if ( _dataProvider.printData )  {
	  logger.info("DataProvider: Update: [" +frame.payloadString.length+' bytes] '+
		      itm+'='+_dataProvider.lastMessageTime+' prev:'+o.prev_data);
	}
      }
      i = o = v = len = itm = data = null;
      return;
    }
    else if ( frame.payloadString == this.parent.pageName+':reload' ) {
      this.parent.reset();
      window.location.replace(document.location);
    }
    else if ( frame.payloadString == 'allPages:reload' ) {
      this.parent.reset();
      window.location.replace(document.location);
    }
    else if ( frame.payloadString.substring(0,this.parent.pageName.length+5) == this.parent.pageName+':url:' ) {
      this.parent.reset();
      window.location.assign(frame.payloadString.substring(this.parent.pageName.length+5));
    }
    logger.error('DataProvider: MQTT socket: onmessage: retrieved data with invalid item number');
  };

  /// Callback when the connection is lost
  this.onConnectionLost = function(responseObject)    {
    _dataProvider.isConnected = false;
    if ( responseObject.errorCode !== 0 )  {
      _dataProvider.logger.warning("DataProvider: MQTT socket: transport closed (code: " + 
				   responseObject.errorCode + "," + responseObject.errorMessage + ")");
    }
  };

  /// Connect to mqtt channel
  /**
   *  @return  Reference to self
   */
  this.start = function() {
    var clientid = 'ID_'+parseInt(Math.floor((Math.random() * 100000000000000000000) + 1));
    try {
      var b, l, s = ''+document.location, d = new Date();
      s = s.replace(/type=/g,'');
      if ( (b=s.lastIndexOf('/')+1) < 1 ) b = 0;
      if ( (l=s.indexOf('&'))       < 0 ) l = s.length;
      // Added CL, 25/07/2017 - Max number of characters for 
      var clid = get_ip_address(); // IPv4, max 15characters
      clientid = 'ID_'+clid+'_'+parseInt(Math.floor((Math.random() * 10000) + 1));
      b = l = s = clid = null;
    }
    catch (e) {
    }
    this.host     = location.hostname;
    this.port     = Number(location.port);
    this.service  = new Messaging.Client(this.host, this.port, clientid);
    this.clientid = clientid;
    this.service.onConnectionLost = this.onConnectionLost;
    /// Data dispatch callback once mqtt receives data
    this.service.onMessageArrived = this.onMessageArrived;

    this.logger.info("DataProvider: MQTT socket: Created client....");
    clientid = null;

    this.reconnectTimer = setTimeout(this.reconnectHandler,this.checkInterval);
    try {
      this.connectOpts['hosts'] = [this.host,this.host,this.host,this.host,this.host,this.host,this.host];
      this.connectOpts['ports'] = [this.port,this.port,this.port,this.port,this.port,this.port,this.port];
      this.service.connect(this.connectOpts);
    }
    catch (e) {
      this.logger.error('DataProvider: MQTT socket: Exception during connect:'+e);
      //alert('Exception: MQTT connect:'+e);
    }
    this.needConnection = true;
    this.logger.info("DataProvider: Client is now connected to the server...."); 
    host = null;
    port = null;
    return this;
  };

  /// Disconnect from mqtt channel
  /**
   *  @return  Reference to self
   */
  this.reset = function()  {
    if ( this.isConnected )   {
      try  {
	var svc = this.service;
	this.isConnected = false;
	this.service = null;
	if ( svc )  {
	  svc.disconnect();
	  delete svc;
	}
	svc = null;
      }
      catch (e) {
      }
    }
    this.needConnection = false;
    return this;
  };

  /// Reconnect to data source
  /**
   *  @return  Reference to self
   */
  this.reconnect = function() {
    this.isConnected = true;
    this.reset();
    this.start();
    return this;
  };

  /// Pre-Subscribe to data items
  /**  @param item      mqtt topic to subscribe to. Must be an object with a "name" property.
   *
   *  @return  Reference to self
   */
  this.subscribeItem = function(item)  {
    return this.subscribe(item.name,item);
  };


  /// Pre-Subscribe to data items
  /** @param item      Name of mqtt topic to subscribe to
   *  @param callback  Object implementing "set" method when new data is received.
   *
   *  @return  Reference to self
   */
  this.subscribe = function(item,callback)  {
    var svc, msg, message, len = this.calls.length;

    this.calls.length = this.calls.length+1;
    this.calls[len] = item;

    if ( !this.items.hasOwnProperty(item) )
      this.items[item] = new Array(callback);
    else
      this.items[item].push(callback);
    this.logger.debug('DataProvider: Subscribed to data item:'+item+'   '+this.service);
    if ( this.isConnected && this.service )   {
      svc = this.calls[len];
      msg = 'SUBSCRIBE:/'+svc;
      if ( svc.substring(0,7)=='/topic/' ) svc = svc.substring(7);
      else if ( svc.substring(0,1) != '/' ) svc = '/'+svc;
      svc = svc.replace(/\./g,'/');
      svc = svc + this.destinationOpt;
      this.logger.info('DataProvider: Subscribe to service:'+svc);
      this.service.subscribe(svc,this.subscribeOpts);
      message = new Messaging.Message(msg);
      message.destinationName = this.topic;
      this.service.send(message);
      message = null;
    }
    message = svc = msg = len = null;
    return this;
  };

  /// Unubscribe to data items
  /**  @param item      Name of mqtt topic to subscribe to
   *
   *  @return  Reference to self
   */
  this.unsubscribe = function(item)  {
    if ( this.isConnected && this.service ) {
      this.service.unsubscribe(this.command,{});
      for(var i=0; i<this.calls.length;++i) {
	if ( this.calls[i] == item ) {
	 this.service.unsubscribe(item,{});
	 delete this.calls[i];
	 this.calls.length = this.calls.length-1;
	 return this;
	}
      }
    }
    else {
      this.unsubscribeAll();
    }
    this.service = null;
    return null;
  };

  /** Disconnect from all item topics
   *
   *  @return  Reference to self
   */
  this.unsubscribeAll = function()  {
    this.logger.info("DataProvider: Disconnect all pending data services ..");
    if ( this.isConnected && this.service ) {
      this.service.unsubscribe(this.command,{});
      for(var i=0; i<this.calls.length;++i) {
	var item = this.calls[i];
	this.service.unsubscribe(item,{});
	item = null;
      }
    }
    this.calls = new Array();
    this.calls.length = 0;
    this.items = new Object();
    return this;
  };

  /// Update all data items by requesting a "SUBSCRIBE:<item> call to the server
  /**
   *  @return  Reference to self
   */
  this.update = function() {
    if ( this.service )  {
      for (var i=0; i < this.calls.length; ++i)  {
	var msg = 'SUBSCRIBE:/'+this.calls[i];
	var message = new Messaging.Message(msg);
	message.destinationName = this.topic+this.destinationOpt;
	this.service.send(message);
	this.logger.verbose('DataProvider: Connect data item:'+msg);
	msg = message = null;
      }
    }
    return this;
  };

  /// Connect to item topics
  /**
   *  @return  Reference to self
   */
  this.onConnect = function(frame) {  
    this.logger.info("DataProvider: Connected ..");
    this.isConnected = true;
    this.logger.info("DataProvider: Connecting all pending data leaves to services ..");
    this.service.subscribe(this.command,{});
    for (var i=0; i < this.calls.length; ++i) {
      var svc = this.calls[i];
      var msg = 'SUBSCRIBE:/'+this.calls[i];
      if ( svc.substring(0,7)=='/topic/' ) svc = svc.substring(7);
      else if ( svc.substring(0,1) != '/' ) svc = '/'+svc;
      svc = svc.replace(/\./g,'/');
      svc = svc + this.destinationOpt;
      this.logger.info('DataProvider: Subscribe to service:'+svc);
      this.service.subscribe(svc,this.subscribeOpts);
      // this.service.subscribe(svc);
      var message = new Messaging.Message(msg);
      message.destinationName = this.topic;
      this.service.send(message);
      message = null;
      svc = msg = message = null;
    }
    return this;
  };

  /// Disconnect to item topics
  /**
   *  @return  Reference to self
   */
  this.disconnect = function()  {
    this.unsubscribeAll();
    if ( this.service )  {
      this.service.disconnect();
      this.service.reset();
    }
    return this;
  };

  /// Since long running documents have memory leaks, we reload the page regularly to avoid them.
  /**
   *  @return  Reference to self
   */
  this.reloadHandler = function() {
    //window.location.reload();
    _dataProvider.logger.info('MQTT socket: Request to reload: '+window.location);
    setTimeout(_dataProvider.reloadHandler,_dataProvider.reloadInterval);
  };

  // Setup subscription options for ActiveMQ
  this.destinationOpt = '?consumer.retroactive=true';
  this.subscribeOpts = {};
  this.subscribeOpts.qos = 0;
  this.subscribeOpts.timeout = 10;
  this.subscribeOpts.onFailure = function(error) {};

  this.connectOpts = {};
  this.connectOpts['useSSL'] = true;
  //this.connectOpts['mqttVersion'] = 3;
  this.connectOpts.onSuccess = function() {
    _dataProvider.onConnect();  
  };

  /// Failure handler
  this.connectOpts.onFailure = function(error) {
    var msg = 'DataProvider: MQTT socket: OnFailure: Error No. '+error.errorCode+'\n['+ error.errorMessage+']';
    _dataProvider.logger.error(msg);
    msg = null;
  };
  setTimeout(_dataProvider.reloadHandler,this.reloadInterval);
  return this;
};

var dataProviderReset = function() {
  if ( null != _dataProvider ) {
    _dataProvider.needConnection = false;
    _dataProvider.unsubscribeAll();
    _dataProvider.isConnected = true;
    _dataProvider.reset();
  }
};

if ( _debugLoading ) alert('Script lhcb.display.data.mqtt.cpp loaded successfully');
