//==========================================================================
// LHCb Web Status displays
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For details please see       Online/Stomp/README
// For the licensing terms see  Online/Stomp/LICENSE
//
// Author     : M.Frank
//
//==========================================================================

_loadFile('lhcb.display.ext','css');
_loadFile('lhcb.display.ext.fsm','css');
_loadScript('lhcb.display.zoom.cpp');
_loadScript('lhcb.display.extWidgets.cpp');
_loadScript('lhcb.display.extPanels.cpp');

/// Main entry point to create the 'LHCb Velo Motion' web page
/*
 *    \author  M.Frank
 *    \version 2.0
 */
function lhcb_velo_move(id, messages, font_size) {

  /// Once the body is ready, this function is called
  Ext.onReady(function()  {
      /// Create the application object
      var app = lbExt.createApp({title: 'VELO Motion Monitor',
	    messages: messages,
	    fontSize: font_size});
      /// Create view port and add elements
      try  {
	app.run({id: id,
	      layout: { type: 'table', columns: 1, tableAttrs: { width: '100%'}},
	      items: [ { items:    lbExt.standardHeaderItems({id: id+'-header', 
		      left:   { title: lbExt.lhcb_online_picture()+'&nbsp;<B>Velo Motion Monitor</B>',
			onclick: lhcb.constants.urls.lhcb.home_page.src,
			tooltip: 'Velo motion monitor:<br>&nbsp;<br>'+
			'General status information of<br>'+
			'the LHCb Velo motion system'},
		      right:  { title: lbExt.lhcb_logo('//cern.ch/lhcb-public/Objects/Detector/VELO.jpg'),
			onclick: '//cern.ch/lhcb-public/en/Detector/VELO-en.html',
			tooltip: 'Click to see the Velo home page.'},
		      provider: app.provider}) },
		{ items: [lbExt.veloPositionSummary({id: id+'-position',
			  tooltip:  'Raw velo position [units in mm]<br>&nbsp;<br>'+
			  'Click to goto the velo subdetector page.',
			  onclick:  function() { lbExt.gotoPage(lhcb.constants.urls.lhcb.subdetectors.velo.src); },
			  provider: app.provider}),
		    lbExt.veloPositionDisplay({id: id+'-motion',
			  tooltip:  'Velo position display [units in mm]<br>&nbsp;<br>'+
			  'If the beam is stable the two halfes move to the center<br>'+
			  'around the beam indicated by the red dot.',
			  height:   700,
			  provider: app.provider})]  }
		] });
      }
      catch(e)  {
	app.logger.error('[EXCEPTION] Ext:'+Ext.version+'\n\nException: '+e);
      }
    });
};

var velo_move_new_unload = function()  {
  dataProviderReset();
};

var velo_move_new_body = function()  {
  var msg  = the_displayObject['messages'];
  var siz  = the_displayObject['size'];
  return lhcb_velo_move('lhcb_velo_move_main', msg, siz);
};

console.info('Script lhcb.display.velo_move_new.cpp loaded successfully');
