#!/bin/bash
base=`basename $1`;
dir=`dirname $1`;
out_dir=$2;
if test -z "${out_dir}"; then 
    out_dir=../web;
fi;
OUT=./pack_files.data;
echo "load('/web/sites/comet/production/htdocs/WEB/Online/Stomp/packer/packer.js');">$OUT;
echo "pack_javascript('${dir}/${base}','${out_dir}/${base}');">>$OUT;
/usr/bin/java -Xms256M -Xmx256M -jar `dirname ${0}`/../web/env-js.jar <$OUT;
