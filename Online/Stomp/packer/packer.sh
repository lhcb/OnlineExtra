#!/bin/bash
pack_all_files()
{
    echo $1
    FILES=`ls $1`;
    OUT=./pack_files.data;
    echo "load('/web/sites/comet/production/htdocs/WEB/Online/Stomp/packer/packer.js');">$OUT;
    echo $FILES;
    for i in $FILES; do 
	base=`basename $i`;
	dir=`dirname $i`;
	echo "pack_javascript('${1}/${base}','${2}/${base}');">>$OUT;
    done;
    /usr/bin/java -Xms256M -Xmx256M -jar `dirname ${0}`/../web/env-js.jar <${OUT};
}
usage()
{
    echo "packer <input-directory>  <output-directory>";
}

if test -z "${1}"; then
    usage;
elif test -z "${2}"; then
    usage;
else
    pack_all_files ${1} ${2};
fi;
