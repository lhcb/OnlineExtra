

function loadDomains2() {  
  var request = new XmlRpcRequest("/options/RPC2", 'domains');
  request.setHeader("Content-type", "text/xml");
  request.setHeader("User-Agent",     "XMLRPC-Test");
  request.setHeader("Dim-DNS",        "ecs03");
  request.setHeader("Dim-Server",     "/RPCDomainInfo");
  request.setHeader("RPC-Port",       "2601");
  var resp = request.send();
  var sele = document.getElementById("select_domain");
  for(var i = sele.options.length - 1 ; i >= 0 ; i--)    {
    sele.remove(i);
  }
  var para = resp.parseXML();
  //alert('Para: '+para);
  for(var i = 0; i<para.length; i++)    {
    var option = document.createElement("option");
    option.text = para[i];
    sele.add(option);
  }
}
function loadNodes() {  
  var dns = document.getElementById("select_domain");
  var request = new XmlRpcRequest("/options/RPC2", 'nodes');
  request.addParam(dns.value);
  request.setHeader("Content-type", "text/xml");
  request.setHeader("User-Agent",     "XMLRPC-Test");
  request.setHeader("Dim-DNS",        "ecs03");
  request.setHeader("Dim-Server",     "/RPCDomainInfo");
  request.setHeader("RPC-Port",       "2601");
  var resp = request.send();
  var sele = document.getElementById("select_node");
  for(var i = sele.options.length - 1 ; i >= 0 ; i--)    {
    sele.remove(i);
  }
  var para = resp.parseXML();
  //alert('Para: '+typ+' '+para);
  for(var i = 0; i<para.length; i++)    {
    var option = document.createElement("option");
    option.text = para[i];
    sele.add(option);
  }
}
function loadTasks() {  
  var dns = document.getElementById("select_domain");
  var node = document.getElementById("select_node");
  var request = new XmlRpcRequest("/options/RPC2", 'tasks');
  request.addParam(dns.value);
  request.addParam(node.value);
  request.setHeader("Content-type", "text/xml");
  request.setHeader("User-Agent",     "XMLRPC-Test");
  request.setHeader("Dim-DNS",        "ecs03");
  request.setHeader("Dim-Server",     "/RPCDomainInfo");
  request.setHeader("RPC-Port",       "2601");
  var resp = request.send();
  var sele = document.getElementById("select_task");
  for(var i = sele.options.length - 1 ; i >= 0 ; i--)    {
    sele.remove(i);
  }
  var para = resp.parseXML();
  var typ = typeof para;
  //alert('Para: '+typ+' '+para);
  for(var i = 0; i<para.length; i++)    {
    var option = document.createElement("option");
    option.text = para[i];
    sele.add(option);
  }
}
function loadOptions() {  
  var dns = document.getElementById("select_domain");
  var task = document.getElementById("select_task");
  var request = new XmlRpcRequest("/options/RPC2", 'allProperties');
  request.addParam(dns.value);
  request.addParam(task.value);
  request.setHeader("Content-type", "text/xml");
  request.setHeader("User-Agent",     "XMLRPC-Test");
  request.setHeader("Dim-DNS",        dns.value);
  request.setHeader("Dim-Server",     task.value);
  request.setHeader("RPC-Port",       "2600");

  var resp = request.send();
  var sele = document.getElementById("select_option");
  for(var i = sele.options.length - 1 ; i >= 0 ; i--)    {
    sele.remove(i);
  }
  var para = resp.parseXML();
  var typ = typeof para;
  alert('Para: '+para);
  for(var i = 0; i<para.length; i++)    {
    var option = document.createElement("option");
    var o = para[i];
    option.text = o.client+'.'+o.name+'  =  '+o.prop_value;
    sele.add(option);
  }
}

function myLoop(x) {
  var i, y, xLen, txt;
  txt = "";
  x = x.childNodes;
  xLen = x.length;
  for (i = 0; i < xLen ;i++) {
    y = x[i];
    if (y.nodeType != 3) {
      if (y.childNodes[0] != undefined) {
        txt += myLoop(y);
      }
    } else {
    txt += y.nodeValue + "<br>";
    }
  }
  return txt;
};

function loadDomains() {  
  var request = new XmlRpcRequest("/options/RPC2", 'domains');  
  var text = request.build();
  var xhttp;
  if (window.XMLHttpRequest) {
    // code for modern browsers
    xhttp = new XMLHttpRequest();
  } else {
    // code for IE6, IE5
    xhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var demo = document.getElementById("demo");
      var sele = document.getElementById("sele");
      var str = xhttp.responseText;
      var res = str.replace(/</gi, "&lt;");
      res = str.replace(/>/gi, "&gt;\n");      
      alert('Result:'+str.length+' --> '+str);
      var parser;
      if (typeof DOMParser != "undefined") {
	parser = new DOMParser();
	var obj = parser.parseFromString(str, "text/xml");

	demo.innerHTML = myLoop(obj.documentElement);

	var s = str.split('\n');
	var pp = demo.innerHTML + '<br>';
	//for(var i=0; i<s[0].length; ++i)
	//  pp = pp + '<p>' + i + ': "'+s[0].charAt(i) + '" </p>';
	demo.innerHTML = pp;
	
	for(var i = sele.options.length - 1 ; i >= 0 ; i--)    {
	  sele.remove(i);
	}
	var resp = new XmlRpcResponse(obj);
	var para = resp.parseXML();
	var typ = typeof para;
	alert('Para: '+typ+' '+para);
	for(var i = 0; i<para.length; i++)    {
	  var option = document.createElement("option");
	  option.text = para[i];
	  sele.add(option);
	}
      }
      else  {
	alert('No DOM parser present!');
      }
    }
    else if (xhttp.readyState == 4) {
      alert('HTTP Error '+xhttp.status);
    }
  };
  xhttp.open("POST", "/options/RPC2", true);
  xhttp.setRequestHeader("Content-type", "text/xml");
  xhttp.setRequestHeader("User-Agent",     "XMLRPC-Test");
  xhttp.setRequestHeader("Dim-DNS",        "ecs03");
  xhttp.setRequestHeader("Dim-Server",     "/RPCDomainInfo");
  xhttp.setRequestHeader("RPC-Port",       "2601");
  xhttp.send(text);
} 

function loadDomains2() {
  var xhttp;
  if (window.XMLHttpRequest) {
    // code for modern browsers
    xhttp = new XMLHttpRequest();
  } else {
    // code for IE6, IE5
    xhttp = new ActiveXObject("Microsoft.XMLHTTP");
  }
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      var str = xhttp.responseText;
      var res = str.replace(/</gi, "&lt;");
      res = str.replace(/>/gi, "&gt;\n");      
      document.getElementById("demo").innerHTML = str;
      alert(str);
    }
  };
  var text = "<methodCall><methodName>domains</methodName><params /></methodCall>";

  xhttp.open("POST", "/options/RPC2", true);
  xhttp.setRequestHeader("Content-type",   "text/xml");
  //xhttp.setRequestHeader("Content-length", text.length);
  xhttp.setRequestHeader("User-Agent",     "XMLRPC-Test");
  xhttp.setRequestHeader("Dim-DNS",        "ecs03");
  xhttp.setRequestHeader("Dim-Server",     "/RPCDomainInfo");
  xhttp.setRequestHeader("RPC-Port",       "2601");
  xhttp.send(text);
}
