// Add year on footer
var year = new Date();
document.getElementById("lhcb_copyright").innerHTML = "&copy; Copyright <a href='http://home.cern.ch/' target='_blank'>CERN</a> " + year.getFullYear().toString() + ". All rights reserved. <a href='#' data-toggle='modal' data-target='#sources'>(+)</a>"; 

// Change appearance of navigation bar on scroll
$(window).scroll(function() {
    if ($(".navbar").offset().top > 50) {
      $(".fixed-top").addClass("nav-woPadding");
      //$("body").css("padding-top", "64");
    } else {
      $(".fixed-top").removeClass("nav-woPadding");
      //$("body").css("padding-top", "107");
    }
  });

// Media Query in JS
// Add listener for window size changes
if (matchMedia) {
  const breakPoint = window.matchMedia("(max-width: 575px)");
  checkMatch(breakPoint); // Call listener function at run time
  breakPoint.addListener(checkMatch); // Attach listener function on changes
 }


// Apply changes
function checkMatch(breakPoint)   {
  if (breakPoint.matches) {
    // window width is less than 576
    $("#nav_container").removeClass("container");
  } else {
    // window width is more or equal to 576px
    $("#nav_container").addClass("container");
  }

}

// Status area
// Card flip
$(".flippy").flip({
  trigger: 'manual'
      });

$(".flipper").click(function() {
    $(".flippy").flip(true);
  });

$(".flipperB").click(function() {
    $(".flippy").flip(false);
  });

// Card height
var frontHeight = 0;
var maxFrontHeight = 0;
var auxIteratorF = 0;
$.each($('.front').children(), function(){
    frontHeight += $(this).height();
    if (frontHeight > maxFrontHeight) {
      maxFrontHeight = frontHeight;
    }
    if (auxIteratorF < 1) {
      auxIteratorF += 1;
    }
    else {
      auxIteratorF = 0;
      frontHeight = 0;
    }
    //console.log(maxFrontHeight);
  });

var backHeight = 0;
var maxBackHeight = 0;
var auxIteratorB = 0;
$.each($('.back').children(), function(){
    backHeight += $(this).height();
    if (backHeight > maxBackHeight) {
      maxBackHeight = backHeight;
    }
    if (auxIteratorB < 1) {
      auxIteratorB += 1;
    }
    else {
      auxIteratorB = 0;
      backHeight = 0;
    }
    //console.log(maxBackHeight);
  });

var maxForBHeight = 0;
if (maxBackHeight > maxFrontHeight) {
  maxForBHeight = maxBackHeight;
 }
 else {
   maxForBHeight = maxFrontHeight;
 }
//console.log(maxForBHeight);

$('.flippy').height(maxForBHeight);

// iframe
// iframe height
document.getElementById('myiFrame').onload = function() {
  if ( this.contentWindow )  {
    //var off = this.contentWindow.document.body.offsetHeight - 100;
    //this.contentWindow.document.body.offsetHeight = off;
    console.log('LHCbOnlineDisplays.cpp> document.body.scrollHeight='+
		this.contentWindow.document.body.scrollHeight+
		' document.body.offsetHeight='+
		this.contentWindow.document.body.offsetHeight);
  }
};

// iframe source + toggle in status section
$(".selectDiv").on('click', function(event){
    //console.log(this.id);
    var requestedUrl = iframeSource(this.id);
    //console.log(requestedUrl);
    var elt = document.getElementById('myiFrame');
    elt.src = requestedUrl.src;
    elt.style.height    = requestedUrl.height;
    elt.style.width     = requestedUrl.width;
    elt.style.maxHeight = requestedUrl.height;
    elt.style.minHeight = requestedUrl.height;
    elt.page_type = this.id;
    $('#status > .toggling').toggle();
    window.document.title = 'LHCb Online Displays -- '+requestedUrl;
    var elt = document.getElementById('iframeContainer');
    elt.scrollIntoView(true);
    // Once in the view, we have to scoll down the container a few pixels to not see the container text
    window.scrollBy(0,10);
    elt = null;
  });

//Finds y value of given object
function __findPos(obj)  {
  var curtop = 0;
  if (obj.offsetParent) {
    do {
      curtop += obj.offsetTop;
    } while (obj = obj.offsetParent);
    return [curtop];
  }
}
var focus_element = function(element)   {
  var elt = document.getElementById(element);
  togglerLink();
  window.scroll(0,__findPos(elt));
};

var __move_to = function(event,element)   {
  var elt = document.getElementById(element);
  event.preventDefault();  //this will prevent the link trying to navigate to another page
  focus_element(element);
  //window.document.title = 'LHCb Online Displays';
};

function togglerLink() {
  var requestedUrl = iframeSource('default');
  var elt = document.getElementById('myiFrame');
  elt.src = requestedUrl.src;
  elt.style.height = requestedUrl.height;
  elt.style.width  = requestedUrl.width;
  elt.style.maxHeight = requestedUrl.height;
  elt.style.minHeight = requestedUrl.height;
  elt.page_type = 'default';
  if ($('#statusContainer').css('display') == 'none') {
    $('#status > .toggling').toggle();
  }
  window.document.title = 'LHCb Online Displays';
};

function iframeSource (pageID) {
  var requestedPage = {
    'LHCb_Daily'            : {src: '//lbgroups.cern.ch/online/DailyReport/today.html',                  height: '1000px', width: '100%'},
    'LHCb_RunDB'            : {src: '//lbrundb.cern.ch',                                                 height: '1000px', width: '100%'},
    'LHCb_ShiftDB'          : {src: '//lbshiftdb.cern.ch',                                               height: '1000px', width: '100%'},
    'LHCb_ProblemDB'        : {src: '//lbproblems.cern.ch',                                              height: '1000px', width: '100%'},
    'LHCb_WebCam'           : {src: '/webcam',                                                           height: '1000px', width: '100%'},
    'LHCb_OperTwiki'        : {src: '//lbtwiki.cern.ch/bin/view/Operation/WebHome',                      height: '1000px', width: '100%'},
    'LHCb_WeeklyMeetings'   : {src: '//indico.cern.ch/category/6734/overview?period=week',               height: '1000px', width: '100%'},
    'LHCb_RunNews'          : {src: '//groups.cern.ch/group/lhcb-run-news/default.aspx',                 height:  '800px', width: '100%'},
    'LHCb_OperPlots'        : {src: '//lhcb-operationsplots.web.cern.ch/lhcb-operationsplots/index.htm', height: '1000px', width: '100%'},
    'LHCb_ELog'             : {src: '//lblogbook.cern.ch/Shift/',                                        height: '1000px', width: '100%'},
    'LHCb_EventDisplay'     : {src: 'https://lbevent.cern.ch/EventDisplay/index.html',                   height: '1000px', width: '100%'},

    'LHCb_Page1_old'        : {src: '/Online/lhcb.comet.htm?type=page1',                                 height:  '750px', width: '100%'},
    'LHCb_Page1'            : {src: '/Online/lhcb.ext.htm?type=page1_new',                               height:  '900px', width: '100%'},
    'LHCb_RunStatus'        : {src: '/Online/lhcb.comet.htm?type=status&system=LHCb',                    height:  '800px', width: '100%'},
    'LHCb_SDRunStatus'      : {src: '/Online/lhcb.comet.htm?type=status',                                height:  '800px', width: '100%'},
    'LHCb_FESTRunStatus'    : {src: '/Online/lhcb.comet.htm?type=status&system=FEST',                    height:  '800px', width: '100%'},
    'LHCb_HVStatus'         : {src: '/Online/lhcb.comet.htm?type=detstatus',                             height: '1000px', width: '100%'},
    'LHCb_HTL2'             : {src: '/Online/lhcb.ext.htm?type=hlt2_new',                                height:  '950px', width: '100%'},
    'LHCb_BCM'              : {src: '/Online/lhcb.comet.htm?type=bcm&charts=1',                          height:  '700px', width: '100%'},
    'LHCb_Magnet'           : {src: '/Online/lhcb.ext.htm?type=magnet_new',                              height:  '700px', width: '100%'},
    'LHCb_PVSSAlarms'       : {src: '/Online/lhcb.comet.htm?type=alarms',                                height: '1000px', width: '100%'},
    'LHCb_OperationStatus'  : {src: '/Online/lhcb.comet.htm?type=oper',                                  height: '1000px', width: '100%'},
    'LHCb_DataQuality'      : {src: '/Online/lhcb.comet.htm?type=dataquality',                           height: '1300px', width: '100%'},
    'LHCb_DAQLumi'          : {src: '/Online/lhcb.comet.htm?type=lumi',                                  height:  '800px', width: '100%'},
    'LHCb_DAQTrigger'       : {src: '/Online/lhcb.comet.htm?type=trigger',                               height: '1000px', width: '100%'},
    'LHCb_DAQBigBrother'    : {src: '/Online/lhcb.comet.htm?type=bigbrother',                            height:  '900px', width: '100%'},
    'LHCb_VELOMotion'       : {src: '/Online/lhcb.ext.htm?type=velo_move_new',                           height:  '980px', width: '100%'},
    'LHCb_VELOMotion_old'   : {src: '/Online/lhcb.comet.htm?type=velo_move',                             height:  '800px', width: '100%'},
    'LHCb_VELOLHLV'         : {src: '/Online/lhcb.comet.htm?type=velo',                                  height:  '900px', width: '100%'},
    'LHCb_L0'               : {src: '/Online/lhcb.comet.htm?type=trgstatus',                             height: '1000px', width: '100%'},
    'LHCb_ST'               : {src: '/Online/lhcb.comet.htm?type=st',                                    height: '1000px', width: '100%'},
    'LHCb_OT'               : {src: '/Online/lhcb.comet.htm?type=ot',                                    height: '1000px', width: '100%'},
    'LHCb_RICH'             : {src: '/Online/lhcb.comet.htm?type=rich',                                  height:  '900px', width: '100%'},
    'LHCb_CALO'             : {src: '/Online/lhcb.comet.htm?type=calo',                                  height: '1300px', width: '100%'},
    'LHCb_MUON'             : {src: '/Online/lhcb.comet.htm?type=muon',                                  height:  '800px', width: '100%'},
    'LHCb_Cooling'          : {src: '/Online/lhcb.comet.htm?type=cooling',                               height:  '600px', width: '100%'},

    'LHCb_LHCStatus'        : {src: '/Online/lhcb.comet.htm?type=lhc',                                   height: '1000px', width: '100%'},
    'LHCb_Collimators'      : {src: '/Online/lhcb.ext.htm?type=collimators_new',                         height:  '700px', width: '100%'},

    'LHC_Page1'             : {src: '//op-webtools.web.cern.ch/vistar/vistars.php?usr=LHC1',             height:  '950px', width: '100%'},
    'LHC_Oper'              : {src: '//op-webtools.web.cern.ch/vistar/vistars.php?usr=LHC3',             height:  '950px', width: '100%'},
    'LHC_Dashboard'         : {src: '//op-webtools.web.cern.ch/vistar/vistars.php?usr=LHCDASHBOARD',     height:  '950px', width: '100%'},
    'LHC_Cryo'              : {src: '//op-webtools.web.cern.ch/vistar/vistars.php?usr=LHC2',             height:  '950px', width: '100%'},
    'LHC_Dump'              : {src: '//op-webtools.web.cern.ch/vistar/vistars.php?usr=LHCBDS',           height:  '800px', width: '100%'},
    'LHC_Coordination'      : {src: '//op-webtools.web.cern.ch/vistar/vistars.php?usr=LHCCOORD',         height:  '950px', width: '100%'},
    'LHC_Configuration'     : {src: '//op-webtools.web.cern.ch/vistar/vistars.php?usr=LHCCONFIG',        height: '1000px', width: '100%'},
    'LHC_RFTiming'          : {src: '//op-webtools.web.cern.ch/vistar/vistars.php?usr=LHCRFTiming',      height: '1000px', width: '100%'},
    'LHC_Collimators'       : {src: '//op-webtools.web.cern.ch/vistar/vistars.php?usr=LHCCOLLI',         height:  '950px', width: '100%'},
    'LHC_ColliBeam1'        : {src: '//op-webtools.web.cern.ch/vistar/vistars.php?usr=LHCCOLLB1',        height:  '950px', width: '100%'},
    'LHC_ColliBeam2'        : {src: '//op-webtools.web.cern.ch/vistar/vistars.php?usr=LHCCOLLB2',        height:  '950px', width: '100%'},

    'LHC_BSRT'              : {src: '//op-webtools.web.cern.ch/vistar/vistars.php?usr=LHCBSRT',          height:  '800px', width: '100%'},
    'LHC_ExpMagnets'        : {src: '//op-webtools.web.cern.ch/vistar/vistars.php?usr=LHCEXPMAG',        height:  '950px', width: '100%'},
    'CPS_Page'              : {src: '//op-webtools.web.cern.ch/vistar/vistars.php?usr=CPS',              height:  '800px', width: '100%'},
    'PSB_Page'              : {src: '//op-webtools.web.cern.ch/vistar/vistars.php?usr=PSB',              height:  '800px', width: '100%'},
    'PSB_BLMPage'           : {src: '//op-webtools.web.cern.ch/vistar/vistars.php?usr=PSBBLM',           height:  '950px', width: '100%'},
    'SPS_Page1'             : {src: '//op-webtools.web.cern.ch/vistar/vistars.php?usr=SPS1',             height:  '800px', width: '100%'},
    'SPS_BT'                : {src: '//op-webtools.web.cern.ch/vistar/vistars.php?usr=SPSBT',            height: '1000px', width: '100%'},
    'LinacII_Page'          : {src: '//op-webtools.web.cern.ch/vistar/vistars.php?usr=LIN',              height:  '800px', width: '100%'},
    'Collider_Logbook'      : {src: 'http://elogbook.cern.ch/eLogbook/eLogbook.jsp',                     height:  '800px', width: '100%'},
    'CMS_Page1'             : {src: '//op-webtools.web.cern.ch/vistar/vistars.php?usr=LHCCMS',           height:  '800px', width: '100%'},
    'default'               : {src: '/Online/html/Blank.htm',                                            height: '1000px', width: '100%'}
  };
  return (requestedPage[pageID] || requestedPage['default']);
};

function add_bookmark(title, url) {
  if( window.external && ('AddFavorite' in window.external) ) { // ie
    window.external.AddFavorite(url, title);
  }
  else if ( window.sidebar )  { // firefox
    alert('Dear Firefox user, \n\n'+
	  'Your browser does no longer allow to \n'+
	  'add bookmarks programatically.\n\n'+
	  'The browser will now switch to a url, \n'+
	  'which when bookmarked will reload this view.\n\n'+
	  'Please press then CTRL+D to Bookmark the page:\n'+
	  '  ** '+title+' **\n\n'+
	  'or use alternatively the following URL to restart \n'+
	  'the viewer in this configuration:\n\n\n'+
	  url+'\n\n\n'
	  );
    window.parent.document.location = url;
  }
  else if(window.opera && window.print) { // opera
    var elem = document.createElement('a');
    elem.setAttribute('href',url);
    elem.setAttribute('title',title);
    elem.setAttribute('rel','sidebar');
    elem.click();
  }
  else   {
    var key = (navigator.userAgent.toLowerCase().indexOf('mac') != - 1 
	       ? 'CMD' 
	       : 'CTRL');
    alert('Dear user, \n\n'+
	  'Your browser does no longer allow to \n'+
	  'add bookmarks programatically.\n\n'+
	  'The browser will now switch to a url, \n'+
	  'which when bookmarked will reload this view.\n\n'+
	  'Please press then '+key+'-D to Bookmark the page:\n'+
	  '  ** '+title+' **\n\n'+
	  'or use alternatively the following URL to restart \n'+
	  'the viewer in this configuration:\n\n\n'+
	  url+'\n\n\n'
	  ); 
    window.parent.document.location = url;
  }
};

function bookmark_page(event)  {
  var ident = $('#myiFrame').attr('page_type') || 'default';
  var pars  = document.location.toString().split('?');
  if ( ident != 'default' )  {
    var loc = pars[0] + '?type=' + ident;
    add_bookmark(document.title, loc);
  }
  else   {
    add_bookmark(document.title, document.location);
  }
};

// Basic click handlers of the page
$('#anchor_topnav').on(        'click', function(event)  {  __move_to(event,'anchor_topnav');       });
//$('#selector_topnav').on(      'click', function(event)  {  __move_to(event,'anchor_topnav');       });
$('#selector_detector').on(    'click', function(event)  {  __move_to(event,'anchor_detector');     });
$('#selector_subdetectors').on('click', function(event)  {  __move_to(event,'anchor_subdetectors'); });
$('#selector_collider').on(    'click', function(event)  {  __move_to(event,'anchor_collider');     });
$('#selector_contact').on(     'click', function(event)  {  __move_to(event,'anchor_contact');      });
$('#selector_links').on(       'click', function(event)  {  __move_to(event,'anchor_links');        });
$('#selector_help').on(        'click', function(event)  {  __move_to(event,'anchor_help');         });
$('#selector_bookmark').on(    'click', function(event)  {  bookmark_page(event);                   });

var webpage_parameters = {};
var pars = location.search;
if ( pars.length > 0 ) pars = pars.substr(1);
pars = pars.split('&');
webpage_parameters.params = pars;
for (var i=0; i<pars.length; ++i) {
  var v = pars[i].split('=');
  webpage_parameters[v[0]] = v[1];
 }
webpage_parameters.url_base = location.origin+location.pathname;
if ( webpage_parameters.type )  {
  var requestedUrl = iframeSource(webpage_parameters.type);
  document.getElementById('selector_detector').click();
  setTimeout(function() { document.getElementById(webpage_parameters.type).click();},25);
 }

