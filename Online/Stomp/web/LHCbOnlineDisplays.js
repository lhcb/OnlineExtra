// Add year on footer
var year = new Date();
document.getElementById("lhcb_copyright").innerHTML = "© Copyright CERN " + year.getFullYear().toString() + ". All rights reserved. <a href='#' data-toggle='modal' data-target='#sources'>(+)</a>"; 

// Change appearance of navigation bar on scoll
$(window).scroll(function() {
    if ($(".navbar").offset().top > 50) {
        $(".fixed-top").addClass("nav-woPadding");
        $("body").css("padding-top", "64");
    } else {
        $(".fixed-top").removeClass("nav-woPadding");
        $("body").css("padding-top", "107");
    }
});

// Media Query in JS
// Add listener for window size changes
if (matchMedia) {
  const breakPoint = window.matchMedia("(max-width: 575px)");
  checkMatch(breakPoint); // Call listener function at run time
  breakPoint.addListener(checkMatch); // Attach listener function on changes
}
// Apply changes
function checkMatch(breakPoint) {
  if (breakPoint.matches) {
    // window width is less than 576
    $("#nav_container").removeClass("container");
  } else {
    // window width is more or equal to 576px
    $("#nav_container").addClass("container");
  }

}

// Status area
// Card flip
$(".flippy").flip({
 	trigger: 'manual'
});

$(".flipper").click(function() {
	$(".flippy").flip(true);
});

$(".flipperB").click(function() {
	$(".flippy").flip(false);
});

// Card height
var frontHeight = 0;
var maxFrontHeight = 0;
var auxIteratorF = 0;
$.each($('.front').children(), function(){
    frontHeight += $(this).height();
    if (frontHeight > maxFrontHeight) {
    	maxFrontHeight = frontHeight;
    }
    if (auxIteratorF < 1) {
    	auxIteratorF += 1;
    }
    else {
    	auxIteratorF = 0;
    	frontHeight = 0;
    }
    //console.log(maxFrontHeight);
});

var backHeight = 0;
var maxBackHeight = 0;
var auxIteratorB = 0;
$.each($('.back').children(), function(){
    backHeight += $(this).height();
    if (backHeight > maxBackHeight) {
    	maxBackHeight = backHeight;
    }
    if (auxIteratorB < 1) {
    	auxIteratorB += 1;
    }
    else {
    	auxIteratorB = 0;
    	backHeight = 0;
    }
    //console.log(maxBackHeight);
});

var maxForBHeight = 0;
if (maxBackHeight > maxFrontHeight) {
    maxForBHeight = maxBackHeight;
}
else {
	maxForBHeight = maxFrontHeight;
}
//console.log(maxForBHeight);

$('.flippy').height(maxForBHeight);

// iframe
// iframe height
document.getElementById('myiFrame').onload = function() {
    console.log(this.contentWindow.document.body.scrollHeight);
    console.log(this.contentWindow.document.body.offsetHeight);
    this.style.height = '1000px';//this.contentWindow.document.body.scrollHeight + 'px';
}

// iframe source + toggle in status section
$(".selectDiv").on('click', function(){
	//console.log(this.id);
	var requestedUrl = iframeSource(this.id);
	//console.log(requestedUrl);
	$('#myiFrame').attr('src', requestedUrl);
	$('#status > .toggling').toggle();

});

function togglerLink() {
	if ($('#statusContainer').css('display') == 'none') {
		$('#status > .toggling').toggle();
	}
}

function iframeSource (pageID) {
  	var requestedPage = {
	    'LHCbPage1' : '/OnlineTest/lhcb.comet.htm?type=page1',
		'LHCbRunStatus' : '/OnlineTest/lhcb.comet.htm?type=status&system=LHCb',
		'subdetectorRunStatus' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=status',
		'FESTRunStatus' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=status&system=FEST',
		'HVRunStatus' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=detstatus',
		'HTL2' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=hlt2',
		'BCM' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=bcm&charts=1',
		'LHCbPVSSAlarms' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=alarms',
		'operationStatus' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=oper',
		'onlineDataQuality' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=dataquality',
		'LHCbELog' : '//lblogbook.cern.ch/Shift/',
		'DAQLumi' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=lumi',
		'DAQTrigger' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=trigger',
		'DAQBigBrother' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=bigbrother',
		'DAQEventDisplay' : 'https://lbevent.cern.ch/EventDisplay/index.html',
		'VELOMotion' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=velo_move',
		'VELOLHLV' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=velo',
		'L0' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=trgstatus',
		'ST' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=st',
		'OT' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=ot',
		'RICH' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=rich',
		'CALO' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=calo',
		'MUON' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=muon',
		'cooling' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=cooling',
		'LHCStatus' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=lhc',
		'operations' : 'https://op-webtools.web.cern.ch/vistar/vistars.php?usr=LHC1',
		'collimators' : 'https://lbcomet.cern.ch/Online/lhcb.comet.htm?type=collimators',
	    'default': ''
  	};
	return (requestedPage[pageID] || requestedPage['default']);
}