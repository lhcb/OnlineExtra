// Include files 

// local
#include "JsonConverter.h"

//-----------------------------------------------------------------------------
// Implementation file for class : JsonConverter
//
// 2014-12-03 : Ben Couturier
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( JsonConverter )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
JsonConverter::JsonConverter( const std::string& name,
                              ISvcLocator* pSvcLocator)
: DaVinciAlgorithm ( name , pSvcLocator )
{
  declareProperty("OutputDirectory", m_outputDir = "");
  declareProperty("EventDisplayParticles", m_partLoc = "" );
  declareProperty("ParticleType", m_partType = "");
}

//=============================================================================
// Destructor
//=============================================================================
JsonConverter::~JsonConverter() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode JsonConverter::initialize()
{
  const StatusCode sc = DaVinciAlgorithm::initialize();
  if ( sc.isFailure() ) return sc;

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  if ( m_outputDir.empty() ) { m_outputDir = "."; }

  m_trackExtrapolator = tool<ITrackExtrapolator>("TrackMasterExtrapolator", this);

  m_otHitCreator = tool<Tf::IOTHitCreator>("Tf::OTHitCreator/OTHitCreator") ;

  m_hcalDetector = getDet<DeCalorimeter> ("/dd/Structure/LHCb/DownstreamRegion/Hcal");
  m_ecalDetector = getDet<DeCalorimeter> ("/dd/Structure/LHCb/DownstreamRegion/Ecal");
  m_muonDetector = getDet<DeMuonDetector>("/dd/Structure/LHCb/DownstreamRegion/Muon");
  m_itDetector   = getDet<DeITDetector>  ("/dd/Structure/LHCb/AfterMagnetRegion/T/IT");
  m_ttDetector   = getDet<DeTTDetector>  ("/dd/Structure/LHCb/BeforeMagnetRegion/TT");
  m_veloDetector = getDet<DeVelo>        ("/dd/Structure/LHCb/BeforeMagnetRegion/Velo");

  return sc;
}

//=============================================================================
// finalize
//=============================================================================
StatusCode JsonConverter::finalize()
{
  // write the summary list
  std::ofstream summary;
  const std::string sumFileName = m_outputDir + "/events.json";
  summary.open( sumFileName.c_str(), std::ios::trunc );
  if ( summary.is_open() )
  {
    summary << "[";
    bool first = true;
    for ( const auto jsonF : m_allJsonFiles )
    {
      if ( !first ) { summary << ","; }
      first = false;
      summary << "\"" << jsonF << "\"";
    }
    summary << "]";
    summary.close();
  }

  return DaVinciAlgorithm::finalize();
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode JsonConverter::execute() 
{

  // Map containing the various data entries
  // HAs to use boost::any unfortunately...
  Stream jsonStream(Container::MAP);

  //std::map<std::string, boost::any> eventData;

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  if ( msgLevel(MSG::DEBUG) ) debug() << "=====> IN JSON CONVERTER" << std::endl;


  if ( msgLevel(MSG::DEBUG) ) debug() << "=====> Extracting ODIN " << endmsg;
  auto * odin = (LHCb::ODIN *)get<LHCb::ODIN>("/Event/DAQ/ODIN");
  if ( msgLevel(MSG::DEBUG) ) debug() << "ODIN RunNumber    : " << odin->runNumber() << endmsg;
  if ( msgLevel(MSG::DEBUG) ) debug() << "ODIN EventNumber  : " << odin->eventNumber() << endmsg;
  if ( msgLevel(MSG::DEBUG) ) debug() << "ODIN GPS Time     : " << odin->gpsTime() << endmsg;
  if ( msgLevel(MSG::DEBUG) ) debug() << "ODIN Event Type   : " << odin->eventType() << endmsg;

  jsonStream << std::make_pair("runNumber", odin->runNumber());
  jsonStream << std::make_pair("eventNumber", odin->eventNumber());
  jsonStream << std::make_pair("gpsTime", odin->gpsTime());

  std::time_t result = odin->gpsTime()/1000000;
  char mbstr[100];
  if (std::strftime(mbstr, sizeof(mbstr), "%a, %d %b %Y %H:%M:%S", std::localtime(&result))) {
    jsonStream << std::make_pair("time", std::string(mbstr));
  }

  // Do we have a particle location set. If so only load those particles, and the
  // detector hits associated to them
  m_loadAll = m_partLoc.empty();
  const auto loc = ( !m_partLoc.empty() ? m_partLoc : "/Event/Phys/StdAllNoPIDsPions/Particles" );
  const auto inputParts = getIfExists<LHCb::Particle::Range>(loc);

  // if no particles, skip this event..
  if ( inputParts.empty() ) return StatusCode::SUCCESS;

  m_lhcbIDs.clear();
  m_tracks.clear();
  if ( !m_loadAll )
  {
    for ( const auto part : inputParts ) { findLHCbIDs(part); }
    std::sort( m_lhcbIDs.begin(), m_lhcbIDs.end() );
    m_lhcbIDs.erase( std::unique(m_lhcbIDs.begin(),m_lhcbIDs.end()), m_lhcbIDs.end() );
    std::sort( m_tracks.begin(), m_tracks.end() );
    m_tracks.erase( std::unique(m_tracks.begin(),m_tracks.end()), m_tracks.end() );
  }

  if ( msgLevel(MSG::DEBUG) ) debug() << "=====> Extracting PVs " << endmsg;
  auto * vertices = getIfExists<LHCb::RecVertices>("/Event/Rec/Vertex/Primary");
  if (nullptr == vertices)   {
    if(msgLevel(MSG::INFO)) info()<<" Container Rec/Vertex/Primary doesn't exist"<<endmsg;
  } else {
    int vertexCount = 0;
    Stream vlistJson(Container::LIST);
    for_each (vertices->begin(),
              vertices->end(),
              [&vertexCount, &vlistJson] (const LHCb::RecVertex *r)
              {

                if ( !r ) return;
                //getting the covariance matrix
                const auto& cov = r->covMatrix();
                Gaudi::Math::GSL::EigenSystem esystem;
                Gaudi::Vector3 evalues;
                Gaudi::Matrix3x3 evectors;
                esystem.eigenVectors ( cov, evalues, evectors, false );

                Stream vertJson(Container::MAP);
                vertJson << std::make_pair("pv_x", r->position().x());
                vertJson << std::make_pair("pv_y", r->position().y());
                vertJson << std::make_pair("pv_z", r->position().z());

                vertJson << std::make_pair("eval_0", evalues(0));
                vertJson << std::make_pair("eval_1", evalues(1));
                vertJson << std::make_pair("eval_2", evalues(2));
                double mag0 = sqrt(evectors(0,0)*evectors(0,0)+evectors(1,0)*evectors(1,0)+evectors(2,0)*evectors(2,0));
                double mag1 = sqrt(evectors(0,1)*evectors(0,1)+evectors(1,1)*evectors(1,1)+evectors(2,1)*evectors(2,1));
                double mag2 = sqrt(evectors(0,2)*evectors(0,2)+evectors(1,2)*evectors(1,2)+evectors(2,2)*evectors(2,2));
                vertJson << std::make_pair("evec_0x", evectors(0,0)/mag0);
                vertJson << std::make_pair("evec_0y", evectors(1,0)/mag0);
                vertJson << std::make_pair("evec_0z", evectors(2,0)/mag0);
                vertJson << std::make_pair("evec_1x", evectors(0,1)/mag1);
                vertJson << std::make_pair("evec_1y", evectors(1,1)/mag1);
                vertJson << std::make_pair("evec_1z", evectors(2,1)/mag1);
                vertJson << std::make_pair("evec_2x", evectors(0,2)/mag2);
                vertJson << std::make_pair("evec_2y", evectors(1,2)/mag2);
                vertJson << std::make_pair("evec_2z", evectors(2,2)/mag2);

                vlistJson << vertJson;
                vertexCount++;
              });
    // Now adding the entries to the main map
    jsonStream << std::make_pair("PVS", vlistJson);
  }

  if ( msgLevel(MSG::DEBUG) ) debug() << "=====> Extracting MUON Hits " << endmsg;
  const auto * coords = getIfExists<LHCb::MuonCoords>("Raw/Muon/Coords");
  if( nullptr == coords ) {
    if(msgLevel(MSG::INFO)) info()<<" Container Raw/Muon/Coords doesn't exist"<<endmsg;
  } else
  {
    Stream muonJson(Container::LIST);
    if ( m_loadAll )
    {
      for_each(coords->begin(),
               coords->end(),
               [this, &muonJson] (LHCb::MuonCoord *c)
               {
                 if (nullptr == c) return;
                 const auto digitTiles = c->digitTile();
                 for ( auto dt : digitTiles )
                 {
                   const LHCb::LHCbID lhcbID(dt);
                   double x(0), dx(0), y(0), dy(0), z(0), dz(0);
                   m_muonDetector-> Tile2XYZ(dt, x, dx, y, dy, z, dz);
                   Stream hitJson(Container::LIST);
                   hitJson << int(x) << int(dx) << int(y) << int(dy)
                           << int(z) << int(dz);
                   muonJson << hitJson;
                 }
               });
    }
    else
    {
      for ( const auto id : m_lhcbIDs )
      {
        if ( id.isMuon() )
        {
          double x(0), dx(0), y(0), dy(0), z(0), dz(0);
          m_muonDetector-> Tile2XYZ(id.muonID(), x, dx, y, dy, z, dz);
          Stream hitJson(Container::LIST);
          hitJson << int(x) << int(dx) << int(y) << int(dy)
                  << int(z) << int(dz);
          muonJson << hitJson;
        }
      }
    }
    jsonStream << std::make_pair("MUON", muonJson);
  }

  if ( msgLevel(MSG::DEBUG) ) debug() << "=====> Extracting OT Hits " << endmsg;
  // Using the Hit creator as recommended y W.Hulsbergen

  if ( m_otHitCreator != nullptr )
  {
    const auto othits = m_otHitCreator->hits();
    Stream otJson(Container::LIST);
    for( const auto& othit : othits )
    {
      if ( !isActive(othit->rawhit().channel()) ) continue;
      Stream sothit(Container::LIST);
      auto beginPoint = othit->beginPoint();
      auto endPoint =  othit->position( othit->yEnd());
      sothit << beginPoint.x() << beginPoint.y() << beginPoint.z()
             << endPoint.x() << endPoint.y() << endPoint.z();
      otJson << sothit;
    }
    jsonStream << std::make_pair("OT", otJson);
  }

  if ( msgLevel(MSG::DEBUG) ) debug() << "=====> Extracting IT Hits " << endmsg;
  const LHCb::STClusters* itClusters = getIfExists<LHCb::STClusters>("/Event/Raw/IT/Clusters");
  if( nullptr == itClusters ) {
    if(msgLevel(MSG::INFO)) info()<<" Container Raw/IT/Clusters doesn't exist"<<endmsg;
  } else
  {

    Stream itJson(Container::LIST);
    for_each(itClusters->begin(),
             itClusters->end(),
             [this, &itJson] (const LHCb::STCluster *c)
             {
               if (nullptr == c) return;
               LHCb::STChannelID cid = c->channelID();
               if ( !isActive(cid) ) return;
               LHCb::LHCbID id(cid);
               auto t = m_itDetector->trajectory(id, 0.0);
               if ( t.get() )
               {
                 Stream ithit(Container::LIST);
                 ithit << t->beginPoint().x() << t->beginPoint().y() << t->beginPoint().z()
                       << t->endPoint().x() << t->endPoint().y() << t->endPoint().z();
                 itJson << ithit;
               }
             });
    jsonStream << std::make_pair("IT", itJson);
  }

  if ( msgLevel(MSG::DEBUG) ) debug() << "=====> Extracting TT Hits " << endmsg;
  const LHCb::STClusters* ttClusters = getIfExists<LHCb::STClusters>("/Event/Raw/TT/Clusters");
  if( nullptr == ttClusters ) {
    if(msgLevel(MSG::INFO)) info()<<" Container Raw/TT/Clusters doesn't exist"<<endmsg;
  } else
  {

    Stream ttJson(Container::LIST);
    for_each(ttClusters->begin(),
             ttClusters->end(),
             [this, &ttJson] (const LHCb::STCluster *c)
             {
               if (nullptr == c) return;
               auto cid = c->channelID();
               if ( !isActive(cid) ) return;
               LHCb::LHCbID id(cid);
               auto t = m_ttDetector->trajectory(id, 0.0);
               if ( t.get() )  
               {
                 Stream tthit(Container::LIST);
                 tthit << t->beginPoint().x() << t->beginPoint().y() << t->beginPoint().z()
                       << t->endPoint().x() << t->endPoint().y() << t->endPoint().z();
                 ttJson << tthit;
               }
             });
    jsonStream << std::make_pair("TT", ttJson);
  }

  // Get track state(s) at HCAL
  std::vector<LHCb::State> hcalStates;
  if ( !m_loadAll )
  {
    hcalStates.reserve( inputParts.size() );
    const double zHcal = 13692;
    for ( const auto part : inputParts )
    {
      hcalStates.emplace_back( LHCb::State() );
      auto & hcalState = hcalStates.back();
      m_trackExtrapolator->propagate( *part->proto()->track(), zHcal, hcalState );
    }
  }

  if ( msgLevel(MSG::DEBUG) ) debug() << "=====> Extracting HCAL Hits " << endmsg;
  const LHCb::CaloDigits* caloDigits = getIfExists<LHCb::CaloDigits>("/Event/Raw/Hcal/Digits");
  if( nullptr == caloDigits ) {
    if(msgLevel(MSG::INFO)) info()<<" Container Raw/Hcal/Digits doesn't exist"<<endmsg;
  } else
  {
    Stream hcalJson(Container::LIST);
    for_each(caloDigits->begin(),
             caloDigits->end(),
             [this, &hcalJson, &hcalStates] (const LHCb::CaloDigit *c)
             {
               if ( nullptr == c ) return;
               if (c->e() > 100.0)//150
               {
                 const auto cid = c->cellID();
                 double x = m_hcalDetector->cellX(cid);
                 double y = m_hcalDetector->cellY(cid);
                 double z = m_hcalDetector->cellZ(cid);
                 double s = m_hcalDetector->cellSize(cid);
                 //info() << "HCAL " << x << " " << y << " " << z << endmsg;
                 // compare to track states to see if its associated
                 bool sel = m_loadAll;
                 if ( !sel )
                 {
                   for ( const auto & state : hcalStates )
                   {
                     const auto diff = std::sqrt( std::pow( x - state.x(), 2 ) +
                                                  std::pow( y - state.y(), 2 ) );
                     if ( diff < 100 ) { sel = true; break; } // CRJ : guess at min distance...
                   }
                 }
                 if ( sel )
                 {
                   Stream hcalhit(Container::LIST);
                   hcalhit << int(c->e()) << int(x) << int(y) << int(z) << int(s);
                   hcalJson << hcalhit;
                 }
               }
             });
    jsonStream << std::make_pair("HCAL", hcalJson);
  }

  if ( msgLevel(MSG::DEBUG) ) debug() << "=====> Extracting ECAL Hits " << endmsg;
  const auto * ecalDigits = getIfExists<LHCb::CaloDigits>("/Event/Raw/Ecal/Digits");
  if( nullptr == ecalDigits ) {
    if(msgLevel(MSG::INFO)) info()<<" Container Raw/Ecal/Digits doesn't exist"<<endmsg;
  } else
  {
    Stream ecalJson(Container::LIST);
    for_each(ecalDigits->begin(),
             ecalDigits->end(),
             [this, &ecalJson] (const LHCb::CaloDigit *c)
             {
               if (c->e() > 50.0) //150.0
               {
                 const LHCb::CaloCellID cid = c->cellID();
                 if ( !isActive(cid) ) return;
                 double x = m_ecalDetector->cellX(cid);
                 double y = m_ecalDetector->cellY(cid);
                 double z = m_ecalDetector->cellZ(cid);
                 double s = m_ecalDetector->cellSize(cid);
                 Stream ecalhit(Container::LIST);
                 ecalhit << int(c->e()) << int(x) << int(y) << int(z)
                         << int(s);
                 ecalJson << ecalhit;
               }
             });
    jsonStream << std::make_pair("ECAL", ecalJson);
  }


  if ( msgLevel(MSG::DEBUG) ) debug() << "=====> Extracting VELO Hits " << endmsg;
  const LHCb::VeloClusters* veloClusters = getIfExists<LHCb::VeloClusters>("/Event/Raw/Velo/Clusters");
  if( nullptr == veloClusters ) {
    if(msgLevel(MSG::INFO)) info()<<" Container Raw/Velo/Clusters doesn't exist"<<endmsg;
  } else
  {

    Stream veloRJson(Container::LIST);
    Stream veloPhiJson(Container::LIST);
    for_each(veloClusters->begin(),
             veloClusters->end(),
             [this, &veloRJson, &veloPhiJson] (const LHCb::VeloCluster *c)
             {
               if (nullptr == c) return;
               auto cid = c->channelID();
               if ( !isActive(cid) ) return;
               const auto * sensor = m_veloDetector->sensor(cid);
               const auto values = c->stripValues();
               for (auto p: values)
               {
                 auto striplimits = sensor->globalStripLimits(p.first);
                 auto f = striplimits.first;
                 auto s = striplimits.second;
                 Stream hit(Container::LIST);
                 hit << int(f.x()) << int(f.y()) << int(f.z())
                     << int(s.x()) << int(s.y()) << int(s.z());

                 if ( c->isRType() || c->isPileUp() )
                 {
                   veloRJson << hit;
                 } 
                 else
                 {
                   veloPhiJson << hit;
                 }
               }
             });
    jsonStream << std::make_pair("VELOPHI", veloPhiJson);
    jsonStream << std::make_pair("VELOR", veloRJson);
  }

  if ( msgLevel(MSG::DEBUG) ) debug() << "=====> Extracting Velo tracks " << endmsg;
  LHCb::Tracks *vtracks = getIfExists<LHCb::Tracks>("/Event/Rec/Track/Best");
  Stream veloTracksJson(Container::LIST);
  int vtracksCount = 0;
  if( nullptr == vtracks ) {
    if(msgLevel(MSG::INFO)) info()<<" Container /Event/Rec/Track/Best doesn't exist"<<endmsg;
  } else
  {
    for_each (vtracks->begin(),
              vtracks->end(),
              [this, &vtracksCount, &veloTracksJson, &vertices] (LHCb::Track *t)
              {

                if ( isActive(t) && t->checkType(LHCb::Track::Types::Velo) )//only look at velo tracks
                {
                  //LHCb::RecVertex* bestpv = nullptr;
                  const auto&  pos = t->position();
                  const auto&  mom = t->momentum();
                  const auto momnormalized = mom/sqrt(mom.Mag2());
                  double zstart = 0.0;
                  double bestip = 1.0e+6;
                  Gaudi::XYZPoint closest(0.0, 0.0, 0.0);

                  for_each (vertices->begin(),
                            vertices->end(),
                            [&t, &pos, &bestip, &closest, &zstart, &momnormalized] (LHCb::RecVertex *pv)
                            {
                              const auto&  pvpos = pv->position();
                              Gaudi::XYZPoint pvminuspos(pvpos.x()-pos.x(), pvpos.y()-pos.y(), pvpos.z()-pos.z());
                              double currentip = sqrt(  (pvminuspos - pvminuspos.Dot(momnormalized)*momnormalized).Mag2() );
                              if (currentip < bestip)
                              {
                                closest = pos + pvminuspos.Dot(momnormalized)*momnormalized;
                                zstart = closest.z();
                                bestip = currentip;
                              }
                            });

                  double zfinal = (closest+momnormalized*500.0).z();
                  if (t->checkFlag(LHCb::Track::Flags::Backward)) //backwards track
                    zfinal = (closest-momnormalized*500.0).z();
                  Stream trackJson(Container::LIST);
                  const unsigned int npoints = 5;
                  LHCb::State s;
                  for(unsigned int i=0; i < npoints; i++)
                  {
                    double z = zstart + i*(zfinal-zstart)/npoints;
                    m_trackExtrapolator->propagate(*t, z, s);
                    Stream p(Container::LIST);
                    p << s.x() << s.y() << s.z();
                    trackJson << p;
                  }
                  veloTracksJson << trackJson;
                }
              });
    if ( msgLevel(MSG::DEBUG) ) debug() << "Vtracks count " << vtracksCount << endmsg;
  }
  jsonStream << std::make_pair("VTRACKS", veloTracksJson);


  if ( msgLevel(MSG::DEBUG) ) debug() << "=====> Extracting Particles " << endmsg;
  Stream allParticlesJson(Container::LIST);
  int partCount = 0;
  int partTypeCount = 0;
  if ( !inputParts.empty() )
  {
    for_each (inputParts.begin(),
              inputParts.end(),
              [this, &partCount, &allParticlesJson, &partTypeCount] (const LHCb::Particle *p)
              {
                Stream pJson(Container::MAP);
                partCount++;

                if (p == nullptr)
                  return;
                if (!p->proto()->track()->checkType(LHCb::Track::Types::Long))
                  return;

                // Check that is particle is of the requested type and reject those that arent
                if( m_partType != "" ) {
                  if( (getParticleName(p) != m_partType+"+") && (getParticleName(p) != m_partType+"-") )
                    return;
                }
                partTypeCount++;

                std::string partName =  getParticleName(p);
                pJson <<  std::make_pair("name", partName);
                pJson <<  std::make_pair("m", p->measuredMass());
                pJson <<  std::make_pair("E", p->momentum().E());
                pJson <<  std::make_pair("px", p->momentum().px());
                pJson <<  std::make_pair("py", p->momentum().py());
                pJson <<  std::make_pair("pz", p->momentum().pz());
                pJson <<  std::make_pair("q", p->charge());

                // XXX Missing Impact parameter info
                // Finding boundaries for the track
                auto bestVertex = findBestVertex(p);
                double zmin = 0;
                if (bestVertex != 0)
                {
                  zmin = bestVertex->position().z();
                }

                double zmax = HCAL_Z;
                if (partName == "mu+" || partName == "mu-")
                  zmax = MUON_Z;

                // Now propagating the track!
                Stream trackJson(Container::LIST);
                if (p->proto() != nullptr)
                {
                  processTrack(p->proto()->track(),
                               p->particleID(),
                               zmin,
                               zmax,
                               trackJson);
                  pJson <<  std::make_pair("track", trackJson);
                }

                // Adding this particle to the list
                allParticlesJson << pJson;

              });

    if ( msgLevel(MSG::DEBUG) ) debug() << "Part count " << partCount << endmsg;
  }
  jsonStream << std::make_pair("PARTICLES", allParticlesJson);


  //SVs
  /*
    const std::string PartsTESLocation("/Event/Phys/StdAllNoPIDsPions/Particles");
    LHCb::Particles *inputParts = getIfExists<LHCb::Particles>(PartsTESLocation);
    Stream allParticlesJson(Container::LIST);
    int partCount = 0;
    if( nullptr == inputParts ) {
    if(msgLevel(MSG::INFO)) info()<<" Container /Event/Phys/StdAllNoPIDsPions/Particles doesn't exist"<<endmsg;
    } else
    {
    for_each (inputParts->begin(),
    inputParts->end(),
    [this, &partCount, &allParticlesJson] (LHCb::Particle *p)
    {
  */



  // Now printing the result
  // if ( msgLevel(MSG::DEBUG) ) debug() << "=====================> JSON Result" << endmsg;
  // if ( msgLevel(MSG::DEBUG) ) debug() << jsonStream.str() << endmsg;
  // if ( msgLevel(MSG::DEBUG) ) debug() << "=====================> JSON Result" << endmsg;

  std::ofstream jsonOutput;
  char fname[PATH_MAX];
  ::snprintf(fname,sizeof(fname),"%s/%08ld_%010ld.json",
             m_outputDir.empty() ? "." : m_outputDir.c_str(),
             long(odin->runNumber()), long(odin->eventNumber()));
  // Check that particles of the requested types were found - if not, do not write the event
  if( partTypeCount > 0) {
    jsonOutput.open(fname);
    jsonOutput << jsonStream.str() << std::endl;
    jsonOutput.close();

    if(msgLevel(MSG::INFO)) info()<< "Writen JSON file: "  << fname << endmsg;
    m_allJsonFiles.push_back(fname);
    setFilterPassed(true);  // Mandatory. Set to true if event is accepted.
  }
  else{
    setFilterPassed(false);
  }

  return StatusCode::SUCCESS;
}


std::string JsonConverter::getParticleName(const LHCb::Particle *p)
{

  auto ProbNNe = LHCb::ProtoParticle::ProbNNe;  //# The ANN probability for the electron hypothesis
  auto ProbNNmu = LHCb::ProtoParticle::ProbNNmu; //# The ANN probability for the muon hypothesis
  //auto ProbNNpi = LHCb::ProtoParticle::ProbNNpi; //# The ANN probability for the pion hypothesis
  auto ProbNNk = LHCb::ProtoParticle::ProbNNk; //  # The ANN probability for the kaon hypothesis
  auto ProbNNp = LHCb::ProtoParticle::ProbNNp; //  # The ANN probability for the proton hypothesis
  //auto ProbNNghost = LHCb::ProtoParticle::ProbNNghost; //# The ANN probability for the ghost hypothesis

  // A few checks in case
  if (p == nullptr)
    return "NullParticle";

  auto pr = p->proto();
  if (pr == nullptr)
    return "NullProtoParticle";

  auto muPID = pr->muonPID();

  // Now actually doing the checks
  std::string pname = "";
  if (p->charge() > 0)
  {
    pname = "pi+";
    if  (pr->info(ProbNNk, -9999) > 0.3
         && pr->info(ProbNNk, -9999) > pr->info(ProbNNp, -9999)
         && pr->info(ProbNNk, -9999) > pr->info(ProbNNe, -9999)
         && pr->info(ProbNNk, -9999) > pr->info(ProbNNmu, -9999))
      pname = "K+";


    if  (pr->info(ProbNNp, -9999) > 0.3
         && pr->info(ProbNNp, -9999) > pr->info(ProbNNk, -9999)
         && pr->info(ProbNNp, -9999) > pr->info(ProbNNe, -9999)
         && pr->info(ProbNNp, -9999) > pr->info(ProbNNmu, -9999))
      pname = "p+";

    if  (pr->info(ProbNNe, -9999) > 0.3
         && pr->info(ProbNNe, -9999) > pr->info(ProbNNk, -9999)
         && pr->info(ProbNNe, -9999) > pr->info(ProbNNp, -9999)
         && pr->info(ProbNNe, -9999) > pr->info(ProbNNmu, -9999))
      pname = "e+";

    if  (pr->info(ProbNNmu, -9999) > 0.1 && muPID && muPID->IsMuon())
      pname = "mu+";

  }

  if (p->charge() < 0)
  {

    pname = "pi-";
    if  (pr->info(ProbNNk, -9999) > 0.3
         && pr->info(ProbNNk, -9999) > pr->info(ProbNNp, -9999)
         && pr->info(ProbNNk, -9999) > pr->info(ProbNNe, -9999)
         && pr->info(ProbNNk, -9999) > pr->info(ProbNNmu, -9999))
      pname = "K-";

    if  (pr->info(ProbNNp, -9999) > 0.3
         && pr->info(ProbNNp, -9999) > pr->info(ProbNNk, -9999)
         && pr->info(ProbNNp, -9999) > pr->info(ProbNNe, -9999)
         && pr->info(ProbNNp, -9999) > pr->info(ProbNNmu, -9999))
      pname = "p-";

    if  (pr->info(ProbNNe, -9999) > 0.3
         && pr->info(ProbNNe, -9999) > pr->info(ProbNNk, -9999)
         && pr->info(ProbNNe, -9999) > pr->info(ProbNNp, -9999)
         && pr->info(ProbNNe, -9999) > pr->info(ProbNNmu, -9999))
      pname = "e-";

    if  (pr->info(ProbNNmu, -9999) > 0.1 && muPID && muPID->IsMuon())
      pname = "mu-";
  }

  return pname;

}

const LHCb::VertexBase* JsonConverter::findBestVertex(const LHCb::Particle *p)
{
  if (nullptr == p)
  {
    if(msgLevel(MSG::DEBUG)) debug()<< " Null pointer passed to findBestVertex"<<endmsg;
    return nullptr;
  }

  return this->bestPV(p);
}

void JsonConverter::processTrack(const LHCb::Track* track,
                                 const LHCb::ParticleID pid,
                                 double zmin,
                                 double zmax,
                                 Stream& json)
{

  const int npoints = 10;
  LHCb::State s;
  double z;

  // A few checks just in case...
  if (nullptr == track || nullptr == m_trackExtrapolator)
    return;

  for(int i=0; i < npoints+1; i++)
  {
    z = zmin + i*(zmax-zmin)/npoints;
    m_trackExtrapolator->propagate(*track, z, s, pid);
    Stream p(Container::LIST);
    p << s.x() << s.y() << s.z();
    json << p;
  }
}

/// Recurse down to the stable daugthers
void JsonConverter::findLHCbIDs( const LHCb::Particle * part,
                                 const unsigned int recLevel )
{
  if ( recLevel < 9999 && part )
  {
    if ( part->isBasicParticle() && part->proto() )
    {
      // charged ?
      const auto tk = part->proto()->track();
      if ( tk )
      {
        m_tracks.push_back( tk );
        for ( const auto& id : tk->lhcbIDs() ) { m_lhcbIDs.push_back(id); }
        // muon track ?
        if ( part->proto()->muonPID() )
        {
          const auto mutk = part->proto()->muonPID()->muonTrack();
          if ( mutk )
          {
            //info() << "Found Muon track" << endmsg;
            for ( const auto& id : mutk->lhcbIDs() )
            {
              //info() << " Adding " << id << endmsg;
              m_lhcbIDs.push_back(id);
            }
          }
        }
      }
      // neutrals
      for ( const auto ch : part->proto()->calo() ) { addCalo(ch); }
    }
    else
    {
      for ( const auto d : part->daughters() )
      {
        findLHCbIDs( d, recLevel+1 );
      }
    }
  }
}

/// Add a Calo Hypo
void JsonConverter::addCalo( const LHCb::CaloHypo* ch )
{
  if ( ch )
  {
    for ( const auto clus : ch->clusters() )
    {
      m_lhcbIDs.push_back(clus->seed());
      for ( const auto en : clus->entries() )
      {
        //info() << "Adding " << en.digit()->cellID() << endmsg;
        m_lhcbIDs.push_back(en.digit()->cellID());
      }
    }
    for ( const auto dig : ch->digits() )
    {
      m_lhcbIDs.push_back(dig->cellID());
    }
    for ( const auto h : ch->hypos() ) { addCalo(h); }
  }
}

//=============================================================================
