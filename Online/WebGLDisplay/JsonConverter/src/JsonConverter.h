#ifndef JSONCONVERTER_H 
#define JSONCONVERTER_H 1

// STL
#include <string>
#include <ctime>
#include <iostream>
#include <fstream>
#include <sstream>
#include <climits>
#include <map>
#include <memory>

// Boost
#include <boost/any.hpp>

// from Gaudi
#include "GaudiKernel/AlgFactory.h"

// from DaVinci.
#include "Kernel/DaVinciAlgorithm.h"

// Event model
#include "Event/Particle.h"
#include "Event/ODIN.h"
#include "Event/RecVertex.h"
#include "Event/MuonCoord.h"
//#include "Event/OTTime.h"
#include "Event/STCluster.h"
#include "Event/VeloCluster.h"
#include "Event/CaloDigit.h"

// detectors
#include "MuonDet/DeMuonDetector.h"
#include "OTDet/DeOTDetector.h"
#include "STDet/DeITDetector.h"
#include "STDet/DeTTDetector.h"
#include "VeloDet/DeVelo.h"
#include "VeloDet/DeVeloSensor.h"
#include "CaloDet/DeCalorimeter.h"

// kernel
#include "LHCbMath/EigenSystem.h"
//#include "Kernel/OTChannelID.h"
#include "Kernel/LHCbID.h"

// local
#include "JSONStream.h"
#include "TfKernel/OTHit.h"
#include "TfKernel/IOTHitCreator.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

// Forward declaration
class ITrackExtrapolator;

using namespace JSON;

/** @class JsonConverter JsonConverter.h
 *
 *  Converts LHCb Events to JSON format.
 *  @author Ben Couturier
 *  @date   2014-12-03
 */
class JsonConverter : public DaVinciAlgorithm
{

public:

  /// Standard constructor
  JsonConverter( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~JsonConverter( ); ///< Destructor

  virtual StatusCode initialize();    ///< Algorithm initialization
  virtual StatusCode execute   ();    ///< Algorithm execution
  virtual StatusCode finalize();      ///< Algorithm execution

  /**
   * Find the best vertex for the particle
   */
  const LHCb::VertexBase* findBestVertex(const LHCb::Particle *p);

private:

  /**
   * Process a proto particle, dumping the track to JSON
   */
  void processTrack(const LHCb::Track* track,
                    const LHCb::ParticleID pid,
                    double zmin,
                    double zmax,
                    Stream& json);

  /**
   * Convertion of Christoph's method.
   * Have to understand why not use the standard one !
   */
  std::string getParticleName(const LHCb::Particle *p);

  /// Recurse down to the stable daugthers
  void findLHCbIDs( const LHCb::Particle * part,
                    const unsigned int recLevel = 0 );

  /// Add a Calo Hypo
  void addCalo( const LHCb::CaloHypo* ch );

  /// Check LHCbIDs
  inline bool isActive( const LHCb::LHCbID id ) const
  {
    return ( m_loadAll ||
             std::find( m_lhcbIDs.begin(), m_lhcbIDs.end(), id ) != m_lhcbIDs.end() );
  }

  /// check tracks
  inline bool isActive( const LHCb::Track * tk ) const
  {
    return ( m_loadAll ||
             std::find( m_tracks.begin(), m_tracks.end(), tk ) != m_tracks.end() );
  }

private:

  /// Property: output directory for JSON files
  std::string m_outputDir;

  ITrackExtrapolator * m_trackExtrapolator = nullptr;

  Tf::IOTHitCreator * m_otHitCreator = nullptr;

  // Some constants for the detectors
  const double HCAL_Z = 14000;
  const double MUON_Z = 19000;

  /// List of all json files written
  std::vector<std::string> m_allJsonFiles;

  /// TES location(s) for Particles to use
  std::string m_partLoc;

  // Particle types to output
  std::string m_partType;

  // load everything
  bool m_loadAll = true;

  /// List of LHCbIDs found
  std::vector<LHCb::LHCbID> m_lhcbIDs;

  /// List of tracks found
  std::vector<const LHCb::Track *> m_tracks;

  /// HCAL detector
  DeCalorimeter* m_hcalDetector = nullptr;

  /// ECAL detector
  DeCalorimeter* m_ecalDetector = nullptr;

  /// Muon detector
  DeMuonDetector* m_muonDetector = nullptr;

  /// IT detector
  DeITDetector* m_itDetector = nullptr;

  /// TT detector
  DeTTDetector* m_ttDetector = nullptr;

  /// Velo detector
  DeVelo* m_veloDetector = nullptr;

};

#endif // JSONCONVERTER_H
