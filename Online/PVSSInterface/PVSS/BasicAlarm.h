//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : PVSS
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_PVSS_BASICALARM_H
#define ONLINE_PVSS_BASICALARM_H

// Framework include files
#include "PVSS/CfgSetting.h"

// C++ include files
#include <memory>

/*
 *   PVSS namespace declaration
 */
namespace PVSS {

  class DataPoint;

  /** @class BasicAlarm   BasicAlarm.h  PVSS/BasicAlarm.h
    *
    *   Base class for all PVSS alarms.
    *
    *   @author  M.Frank
    *   @version 1.0
    */
  class BasicAlarm : public CfgSetting  {
  protected:
    /// Apply alarm settings to datapoint identified by datapoint name
    virtual void i_apply(Transaction& tr, const std::string& dp_name)  override;
    /// Apply alarm settings to datapoint identified by datapoint name
    virtual void i_remove(Transaction& tr, const std::string& dp_name) override;
  public:
    /// Default constructor
    BasicAlarm()  {}
    /// Copy constructor
    BasicAlarm(const BasicAlarm& copy) : CfgSetting(copy)  {}
    /// Initializing constructor
    BasicAlarm(const std::string& dp_name) : CfgSetting(dp_name) {}
    /// Standard destructor
    virtual ~BasicAlarm()  {}
    /// Assignment operator for insertion in STL containers
    BasicAlarm& operator=(const BasicAlarm& alm)  {
      CfgSetting::operator=(alm);  
      return *this;
    }
    /// Equality operator for insertion in STL containers
    bool operator==(const BasicAlarm& alm) const
    { return CfgSetting::operator==(alm);  }
    /// Operator less for insertion in STL associative classes
    bool operator<(const BasicAlarm& alm) const
    { return CfgSetting::operator<(alm);  }
  };
}      // End namespace PVSS
#endif // ONLINE_PVSS_BASICALARM_H
