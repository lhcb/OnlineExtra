################################################################################
# Package: PVSSInterface
################################################################################
gaudi_subdir(PVSSInterface v1r0)
gaudi_depends_on_subdirs(Online/PVSSManager)
			 
include(PVSS)
find_package(PythonLibs REQUIRED)

gaudi_add_library(PVSSInterface
                  src/*.cpp
                  PUBLIC_HEADERS PVSS
                  INCLUDE_DIRS PythonLibs
                  LINK_LIBRARIES OnlineBase PVSSManager PythonLibs ${CMAKE_DL_LIBS})

target_include_directories(PVSSInterface BEFORE PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/PVSS)
target_compile_options(PVSSInterface PRIVATE -Wno-unused-function)

#---Dictionaries------------------------------------------------------------
gaudi_add_dictionary(PVSS     dict/dictionary.h      dict/dictionary.xml     LINK_LIBRARIES PVSSInterface)
target_compile_options(PVSSDict PRIVATE -Wno-suggest-override)
gaudi_add_dictionary(PVSS_STL dict/dictionary_stl.h  dict/dictionary_stl.xml LINK_LIBRARIES PVSSInterface)
target_compile_options(PVSS_STLDict PRIVATE -Wno-suggest-override)
